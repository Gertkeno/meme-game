#include "gert_Collision.h"

#include <SDL2/SDL.h>

SDL_Rect Circle::ct_Rect( void )
{
    return { int( x-r ), int( y-r ), int( 2*r ), int( 2*r )};
}

bool collision::get_collide( const SDL_Rect &objA, const SDL_Rect &objB )
{
    if( objA.x > objB.x + objB.w )
        return false;
    if( objA.x + objA.w < objB.x )
        return false;
    if( objA.y > objB.y + objB.h )
        return false;
    if( objA.y + objA.h < objB.y )
        return false;
    return true;
}

bool collision::get_collide( const Circle &a, const Circle &b )
{
    int totalRadius = ( a.r + b.r );
    if( distance( a.x, b.x, a.y, b.y ) < totalRadius )
        return true;
    else
        return false;
}

bool collision::get_collide( const Circle& a, const SDL_Rect& box )
{
    float closeX, closeY;
    if( a.x < box.x )
        closeX = box.x; // sets to left side
    else if( a.x > box.x + box.w )
        closeX = box.x + box.w; // sets to right side
    else
        closeX = a.x; //Sets the closest x point to the circles X since it is inside the box

    if( a.y < box.y )
        closeY = box.y; //top
    else if( a.y > box.y + box.h )
        closeY = box.y + box.h; //bottom
    else
        closeY = a.y; //inside

    if( distance( a.x, closeX, a.y, closeY ) < a.r )
        return true;
    else
        return false;
}

void collision::get_normal_diffXY( const SDL_Point& a, const SDL_Point& b, float* diffX, float* diffY )
{
    float dx = a.x - b.x;
    float dy = a.y - b.y;
    if( dx != 0 || dy != 0 )
    {
        float hypotonus = sqrt( dx * dx + dy * dy );
        dx /= hypotonus;
        dy /= hypotonus;

        if( diffX != NULL ) *diffX = dx;
        if( diffY != NULL ) *diffY = dy;
    }
    else
    {
        if( diffX != NULL ) *diffX = 0;
        if( diffY != NULL ) *diffY = 0;
    }
}
