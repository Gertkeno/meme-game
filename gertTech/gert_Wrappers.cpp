#include "gert_Wrappers.h"

#include <SDL2/SDL_image.h>
#include <gert_Camera.h>

#include <gert_Collision.h>

#include <cmath>
#include <iostream>

//MULTI FRAME WRAPPING
MultiFrame_Wrap::MultiFrame_Wrap( void )
{
    _myTexture = nullptr;
    _frames = nullptr;
    _maxFrames = 0;
}

MultiFrame_Wrap::~MultiFrame_Wrap( void )
{
    close_texture();
}

void MultiFrame_Wrap::close_texture( void )
{
    if( _frames != nullptr )
    {
        delete[] _frames;
        _frames = nullptr;
    }
    if( _myTexture != nullptr )
    {
        SDL_DestroyTexture( _myTexture );
        _myTexture = nullptr;
    }
    _maxFrames = 0;
}

void MultiFrame_Wrap::load_texture( std::string fPath, Uint8 segWidthNum, Uint8 segHeightNum, Uint8 margin, Uint16 customFrameNum )
{
    close_texture();
    _myTexture = IMG_LoadTexture( gRenderer, fPath.c_str() );
    if( _myTexture == NULL )
    {
        std::cout << "Failed to load texture " << fPath << "\n> " << IMG_GetError() << std::endl;
    }

    else
    {
        int width, height;
        SDL_QueryTexture( _myTexture, NULL, NULL, &width, &height );

        if( segWidthNum != 0 && segHeightNum != 0 )
        {
            width /= segWidthNum;
            height /= segHeightNum;
            if( customFrameNum > segHeightNum * segWidthNum )
            {
                _maxFrames = customFrameNum;
            }
            else
            {
                _maxFrames = segHeightNum * segWidthNum;
            }
            width -= segWidthNum * margin;
            height -= segHeightNum * margin;
            _frames = new SDL_Rect[ _maxFrames ];

            for( int y = 0; y < segHeightNum; y++ )
            {
                for( int x = 0; x < segWidthNum; x++ )
                {
                    _frames[ x + ( y * segWidthNum ) ] = SDL_Rect{ x*width + margin*x, y*height + margin*y, width, height };
                }
            }
        }
        else
        {
            _frames = nullptr;
        }
    }
}

void MultiFrame_Wrap::load_texture( std::string fPath, std::string ssex )
{
    close_texture();

    _myTexture = IMG_LoadTexture( gRenderer, fPath.c_str() );
    if( _myTexture == NULL )
    {
        std::cout << "Failed to load texture " << fPath << "\n> " << IMG_GetError() << std::endl;
    }

    SDL_RWops* xmlsheet;
    {
        std::string xmlPath = fPath.substr( 0, fPath.find( "." ) + 1 );
        xmlPath = xmlPath + ssex;
        xmlsheet = SDL_RWFromFile( xmlPath.c_str(), "r" );
    }

    if( xmlsheet != NULL )
    {
        #define READ SDL_RWread( xmlsheet, foo, sizeof( char ), 1 )
        char* foo = new char( 'X' );

        int frameIteration = 0;
        char readingType = 'a';
        bool readval = false;
        while( READ )
        {
            if( !readval )
            {
                switch( *foo )
                {
                case 't':
                case 'x':
                case 'y':
                case 'w':
                case 'h':
                    readval = true;
                    readingType = *foo;
                    break;
                }
            }
            else// failsafe reads
            {
                switch( *foo )
                {
                case ' ':
                case '_':
                case '"':
                    readval = false;
                    readingType = 'a';
                    break;
                }
            }

            if( readval && *foo == '=' )
            {
                READ;
                bool starting = true;
                int totalnum = 0;
                while( starting || *foo != '"' )
                {
                    READ;
                    if( *foo != '"' )
                    {
                        totalnum = totalnum * 10 + ( *foo - '0' );
                    }
                    else
                    {
                        starting = false;
                        readval = false;
                    }
                }

                switch( readingType )
                {
                case 't':
                    _maxFrames = totalnum;
                    _frames = new SDL_Rect[ _maxFrames ];
                    break;
                case 'x':
                    _frames[ frameIteration ].x = totalnum;
                    break;
                case 'y':
                    _frames[ frameIteration ].y = totalnum;
                    break;
                case 'w':
                    _frames[ frameIteration ].w = totalnum;
                    break;
                case 'h':
                    _frames[ frameIteration ].h = totalnum;
                    frameIteration += 1;
                    break;
                }
            }
        }

        delete foo;
        SDL_RWclose( xmlsheet );
    }
    else
    {
        std::cout << "Couldn't load sprite sheet data\n";
    }
}

bool MultiFrame_Wrap::render( SDL_Rect area, SDL_Color color, int frame, double angle, bool useCamera, SDL_RendererFlip flip, SDL_Point* p )
{
    if( useCamera )
    {
        area.x -= gCamera.x;
        area.y -= gCamera.y;

        area.x *= gCamera.z;
        area.y *= gCamera.z;
        area.w *= gCamera.z;
        area.h *= gCamera.z;
    }

    SDL_SetTextureColorMod( _myTexture, color.r, color.g, color.b );
    SDL_SetTextureAlphaMod( _myTexture, color.a );
    if( p != NULL )
    {
        *p = { int( p->x * gCamera.z ), int( p->y * gCamera.z ) };
    }

    if( _frames == nullptr )
    {
        SDL_RenderCopyEx( gRenderer, _myTexture, NULL, &area, angle, p, flip );
    }
    else
    {
        SDL_RenderCopyEx( gRenderer, _myTexture, &_frames[ frame ], &area, angle, p, flip );
    }
    return true;
}

void MultiFrame_Wrap::enter_custom_frame( int index, SDL_Rect tile )
{
    if( index >= 0 )
    {
        _frames[ index ] = tile;
    }
}

void MultiFrame_Wrap::set_blend( SDL_BlendMode blend )
{
    SDL_SetTextureBlendMode( _myTexture, blend );
}

SDL_Rect MultiFrame_Wrap::get_frame_data( int frame )
{
    if( frame > -1 && frame < _maxFrames && _frames != nullptr )
    {
        return _frames[ frame ];
    }
    return { 0, 0, 0, 0 };
}
