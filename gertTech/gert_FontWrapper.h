#ifndef GERT_FONTWRAPPER_H
#define GERT_FONTWRAPPER_H

typedef struct _TTF_Font TTF_Font;
class SDL_Texture;
struct SDL_Rect;
struct SDL_Color;
class SDL_Window;

class Camera;
extern Camera gCamera;
extern SDL_Window* gWindow;

#ifndef gRenderer
    #define gRenderer SDL_GetRenderer( gWindow )
#endif // gRenderer

#include <string>

class Font_Wrapper
{
private:
    TTF_Font *_myFont;
    SDL_Texture **_lastTexture;
    std::string *_lastString;
    int* _textSize;
    int t_height;
    uint8_t _activeStr;
public:
    Font_Wrapper( void );
    Font_Wrapper( std::string fontPath, int fontSize );
    bool render( SDL_Rect area, std::string text, SDL_Color color, bool useCamera = true, bool strech = false );
    int string_width( std::string );
    int string_height( void ) { return t_height; }

    ~Font_Wrapper( void );
};

#endif // GERT_FONTWRAPPER_H
