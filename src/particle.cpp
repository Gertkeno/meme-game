#include "particle.h"
#include <SDL2/SDL_shape.h>
//#include <TexHolder.h>
#include <gert_Wrappers.h>

#define FADE_PERCENT 0.2
#define RISE_SPEED 30

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

Particle::Particle()
{
    //ctor
    _pos = new SDL_Rect( { 0, 0, 0, 0 } );
    myTexture = 0;
    effect = flags(0);
    _lifeSpan = 0;
    _lifeTime = 0;
}

Particle::~Particle()
{
    //dtor
    delete _pos;
}

void Particle::update( void )
{
    _lifeTime += gFrameTime;
    if( _lifeTime < _lifeSpan )
    {
        if( ( effect & RISING ) == RISING )
        {
            _pos->y -= RISE_SPEED * gFrameTime;
        }
        else if( ( effect & SINKING ) == SINKING )
        {
            _pos->y += RISE_SPEED * gFrameTime;
        }
    }
}

void Particle::draw( void )
{
    if( get_active() )
    {
        SDL_Color c = color::WHITE;
        if( ( effect & FADE_IN ) == FADE_IN && _lifeTime < _lifeSpan * FADE_PERCENT )
        {
            c = { 255, 255, 255, Uint8( _lifeTime / (_lifeSpan * FADE_PERCENT ) * 254 ) };
        }
        if( ( effect & FADE_OUT ) == FADE_OUT && _lifeTime > _lifeSpan - _lifeSpan * FADE_PERCENT )
        {
            c = { 255, 255, 255, Uint8( ( ( _lifeSpan - _lifeTime )/_lifeSpan )/FADE_PERCENT * 254 ) };
        }
        if( ( effect & RED_MOD ) == RED_MOD )
        {
            c.r = 0;
        }
        if( ( effect & BLUE_MOD ) == BLUE_MOD )
        {
            c.b = 0;
        }
        if( ( effect & GREEN_MOD ) == GREEN_MOD )
        {
            c.g = 0;
        }
        float rAngle = angle;
        if( ( effect & SPIN ) == SPIN )
        {
            rAngle = _lifeTime/_lifeSpan*360 + angle;
            if( rAngle > 360 )
            {
                rAngle -= 360;
            }
        }

        int frame;
        if( subTex < 0 )
        {
            frame = ( _lifeTime/_lifeSpan )*gTextures[ myTexture ].get_maxFrames();
        }
        else
        {
            frame = subTex;
        }

        SDL_Rect cplace = *_pos;
        if( ( effect & SHRINK ) == SHRINK )
        {
            float pc = (_lifeSpan - _lifeTime)/_lifeSpan;
            cplace.w = _pos->w * pc;
            cplace.h = _pos->h * pc;
            cplace.x += (_pos->w - cplace.w)/2;
            cplace.y += (_pos->h - cplace.h)/2;
        }

        bool useCam = true;
        if( ( effect & FIXED ) == FIXED )
        {
            useCam = false;
        }
        gTextures[ myTexture ].render( cplace, c, frame, rAngle, useCam );
    }
}

void Particle::start( const SDL_Rect& pos, float life )
{
    *_pos = pos;
    _lifeSpan = life;
    _lifeTime = 0;
    angle = 0;
    effect = flags(0);
    subTex = -1;
}

void Particle::force_kill( void )
{
    _lifeSpan = 0;
    _lifeTime = 0;
}
