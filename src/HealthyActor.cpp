#include "HealthyActor.h"

HealthyActor::HealthyActor()
{
    //ctor
    _healthPoints = 0;
    _enemy = true;
}

HealthyActor::~HealthyActor()
{
    //dtor
}

void HealthyActor::clean_up( void )
{
    _healthPoints = 0;
}

void HealthyActor::push_me( float xd, float yd, HurtCircle* from )
{
    _xBuffer += xd * gFrameTime;
    _yBuffer += yd * gFrameTime;
}
