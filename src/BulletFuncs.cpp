#include <BulletFuncs.h>
#include <Bullet.h>

#include <GameManager.h>
#include <SDL2/SDL_shape.h>
#include <HealthyActor.h>
#include <particle.h>
#include <HitCircle.h>
#include <gert_Collision.h>
#include <TexHolder.h>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

namespace bulletFunction
{
    void basicShot( Bullet* p )
    {
        HitCircle* foo;
        IF_HIT_CIRC( foo )
        {
            Circle ctArea = { (float)p->get_syncPoint().x, (float)p->get_syncPoint().y, p->radius };
            foo->start( ctArea, p->damage, p->owner->get_enemy() );
        }
    }

    #define RECURSIVESHOT_TOTAL 6
    void recursiveShot( Bullet* p )
    {
        Bullet* foo[ RECURSIVESHOT_TOTAL ];
        for( int i = 0; i < RECURSIVESHOT_TOTAL; i++ )
        {
            IF_BULLET_GET( foo[ i ] )
            {
                foo[ i ]->start( p->get_syncPoint(), p->owner, ( float(i)/RECURSIVESHOT_TOTAL )*360.0, 900, 1 );
                foo[ i ]->damage = p->damage;
                foo[ i ]->myTexture = p->myTexture;
                foo[ i ]->damage = p->damage;
                foo[ i ]->owner = p->owner;
                foo[ i ]->radius = p->radius;
            }
        }
    }

    void layDoTs( Bullet* p )
    {
        HitCircle* foo;
        IF_HIT_CIRC( foo )
        {
            Circle ctArea = { (float)p->get_syncPoint().x, (float)p->get_syncPoint().y, p->radius };
            foo->start( ctArea, p->damage, p->owner->get_enemy() );
            foo->totalLifeTime = 2.8;
            Particle* pdisplay;
            IF_PARTICLE_GET( pdisplay )
            {
                pdisplay->start( ctArea.ct_Rect(), foo->totalLifeTime + 0.2 );
                pdisplay->myTexture = assets::texture::ORB_SHOT;
                pdisplay->effect = Particle::flags( Particle::BLUE_MOD | Particle::FADE_OUT | Particle::FADE_IN );
            }
        }
    }

    void comeBack( Bullet* p )
    {
        if( p->get_abilityCount() %2 == 0 )
        {
            p->alter_direction( p->get_angle()+180 );
        }
        else
        {
            p->alter_direction( p->get_angle()-180 );
        }
    }
}
