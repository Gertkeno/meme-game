#include "PushCircle.h"
#include <gert_Collision.h>
#include <gert_CameraMath.h>
#include <GameManager.h>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

/*#ifndef DT_PI
#define DT_PI( d ) ( (d)*M_PI/180 )
#endif // DT_PI

#ifndef M_PI
#define M_PI    3.14159265358979323846264338327950288
#endif*/

PushCircle::PushCircle()
{
    //ctor
    _xForce = 0;
    _yForce = 0;
}

PushCircle::~PushCircle()
{
    //dtor
}

void PushCircle::start( Circle area, float angle, float totalForce, bool enemyFlag, float damage )
{
    float lastTime = totalLifeTime;
    HitCircle::start( area, damage, enemyFlag );
    totalLifeTime = lastTime;

    _xForce = cos( DT_PI( angle ) ) * totalForce;
    _yForce = sin( DT_PI( angle ) ) * totalForce;

    if( damage != 0 )
    {
        HitCircle* foo;
        IF_HIT_CIRC( foo )
        {
            foo->start( area, damage, enemyFlag );
            foo->totalLifeTime = lastTime;
        }
    }
}
