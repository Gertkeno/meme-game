#include "boss/TabeMonster.h"

#include <gert_SoundWrapper.h>
#include <gert_CameraMath.h>
#include <gert_Collision.h>
#include <boss/TabesMeat.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <PushCircle.h>
#include <TexHolder.h>
#include <particle.h>
#include <Bullet.h>

#ifdef _GERT_DEBUG
#include <iostream>
#include <gert_FontWrapper.h>
#endif // _GERT_DEBUG

#define TM_BODY_RADIUS 1001
#define TM_INVICIBLE_TIME 0.4
#define TM_BASE_HEALTH 130
#define TM_MAX_MEAT 15
#define TM_MEAT_REFRESH 1.2 + ( _healthPoints/TM_BASE_HEALTH )*5

#define TM_SUCK_MIN 750
#define TM_SUCK_MAX 450
#define TM_ATTACK_DELAY -0.9

TabeMonster::TabeMonster()
{
    //ctor
    _lastAttack = TOTAL;
    _meats = new TabesMeat[ TM_MAX_MEAT ];
    _bodyHC = nullptr;
    _iTime = 0;
}

TabeMonster::~TabeMonster()
{
    //dtor
    delete[] _meats;
}

void TabeMonster::update( void )
{
    _timeSinceAttack += gFrameTime;
    _iTime += gFrameTime;
    _lastMeat += gFrameTime;

    float ctTime = _attack_timer()/_total_procs();
    switch( _lastAttack )
    {
    case MEAT_SPIN:
        if( _timeSinceAttack >= 0 )
        {
            if( !_firstProc )
            {
                _storedClose = 0;
                for( uint8_t i = 0; i < TM_MAX_MEAT; i++ )
                {
                    if( _meats[ i ].get_healthPoints() <= 0 )
                    {
                        _storedClose = i;
                        _meats[ i ].start( { (int)_xBuffer, (int)_yBuffer } );
                        break;
                    }
                }
                _angleOffset = angles::get_angle( get_syncPoint(), gGameManager->get_player_point() );
                _firstProc = true;
            }

            float currentAngle = _timeSinceAttack/(_attack_timer()/3)*360 + _angleOffset;
            if( currentAngle > 360 )
            {
                currentAngle -= 360;
            }
            float currentDist = _timeSinceAttack/_attack_timer()*(TM_BODY_RADIUS*2) + TM_BODY_RADIUS;

            SDL_Point lastly;
            lastly.x = _xBuffer + cos( DT_PI( currentAngle ) ) * currentDist;
            lastly.y = _yBuffer + sin( DT_PI( currentAngle ) ) * currentDist;
            _meats[ _storedClose ].set_syncPoint( lastly );
            _meats[ _storedClose ].set_hitbox( 6 );
        }
        break;
    case RAM_MEAT:
        if( !_firstProc && _timeSinceAttack >= 0 )
        {
            _storedClose = _get_closest_meat( gGameManager->get_player_point() );

            if( !_meats[ _storedClose ].get_dead() )
            {
                float distance = collision::distance( get_syncPoint(), _meats[ _storedClose ].get_syncPoint() ) - TM_BODY_RADIUS;
                float diffX, diffY;
                collision::get_normal_diffXY( get_syncPoint(), _meats[ _storedClose ].get_syncPoint(), &diffX, &diffY );
                _xDelta = -diffX * distance / (_attack_timer()/_total_procs());
                _yDelta = -diffY * distance / (_attack_timer()/_total_procs());
                _firstProc = true;
            }
        }
        else if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            if( _attackProcs == 0 )
            {
                _xDelta = 0;
                _yDelta = 0;

                if( !_meats[ _storedClose ].get_dead() )
                {
                    float rangle = angles::get_angle( get_syncPoint(), _meats[ _storedClose ].get_syncPoint() );
                    uint8_t tb = 5;
                    for( uint8_t i = 0; i < tb; i++ )
                    {
                        Bullet* foo;
                        IF_BULLET_GET( foo )
                        {
                            foo->start( _meats[ _storedClose ].get_syncPoint(), this, rangle + rand()%40 - 20, 3900, 1.7 );
                            foo->myTexture = assets::texture::TABE_MONSTER;
                            foo->subTex = _meats[ _storedClose ].subtex;
                            foo->damage = 13;
                        }
                    }
                    _meats[ _storedClose ].force_kill();
                }
            }
            else if( _attackProcs == 1 )
            {

            }
            _attackProcs += 1;
        }
        break;
    case MIX_UP_SHOT:
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            if( _attackProcs == 0 )
            {
                _angleOffset = angles::get_angle( gGameManager->get_player_point(), get_syncPoint() );
            }
            uint8_t mbullets = 4;
            for( uint8_t i = 0; i < mbullets; i++ )
            {
                float bangle = float(i)/float(mbullets)*360 + _angleOffset;
                if( _attackProcs % 2 == 0 )
                {
                    bangle += 45;
                }
                while( bangle > 360 )
                {
                    bangle -= 360;
                }
                Bullet* foo;
                IF_BULLET_GET( foo )
                {
                    foo->start( get_syncPoint(), this, bangle, 3700, 1.8 );
                    foo->damage = 8;
                    foo->radius = 110;
                }
            }
            _attackProcs += 1;
        }
        break;
    case SUCKING_IN:
        if( !_firstProc && _timeSinceAttack > 0 )
        {
            gSound[ assets::sfx::TABE_SUCC ].play();
            _firstProc = true;
        }
        if( _timeSinceAttack > 0 )
        {
            for( uint8_t i = 0; i < TM_MAX_MEAT; i++ )
            {
                _meats[ i ].move_to( get_syncPoint(), 500 );
                if( !_meats[ i ].get_dead() && collision::get_collide( *_bodyHC->area, _meats[ i ].get_area() ) )
                {
                    _meats[ i ].force_kill();
                    _healthPoints += 2;
                }
                else
                {
                    _meats[ i ].set_hitbox( 4 );
                }
            }
            PushCircle* foo;
            IF_PUSH_CIRC( foo )
            {
                float dist = collision::distance( get_syncPoint(), gGameManager->get_player_point() );
                float addedForce = ((TM_BODY_RADIUS*3 - dist)/(TM_BODY_RADIUS*3))*TM_SUCK_MAX;
                if( addedForce < 0 )
                {
                    addedForce = 0;
                }
                Circle area = { _xBuffer, _yBuffer, dist };
                foo->start( area, angles::get_angle( gGameManager->get_player_point(), get_syncPoint() ), TM_SUCK_MIN + addedForce, _enemy );
            }

            HitCircle* mouthZone;
            IF_HIT_CIRC( mouthZone )
            {
                mouthZone->start( { _xBuffer, _yBuffer, TM_BODY_RADIUS*2/3 }, 11, _enemy );
            }
        }//_timeSinceAttack > 0
        break;
    case TOTAL:
        break;
    }

    if( _timeSinceAttack > _attack_timer() )
    {
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }
        _lastAttack = attacks( buffer );
        _attackProcs = 0;
        _timeSinceAttack = TM_ATTACK_DELAY;
        _firstProc = false;
    }

    if( _lastMeat > TM_MEAT_REFRESH )
    {
        for( uint8_t i = 0; i < TM_MAX_MEAT; i++ )
        {
            if( _meats[ i ].get_healthPoints() <= 0 )
            {
                int dist = 6500;
                SDL_Point pos = { rand()%dist - dist/2 + (int)_xBuffer, rand()%dist - dist/2 + (int)_yBuffer };
                _meats[ i ].start( pos );
                break;
            }
        }
        _lastMeat = 0;
    }

    /*MOVEMENT*/
    _xBuffer += _xDelta * gFrameTime;
    _yBuffer += _yDelta * gFrameTime;
    for( uint8_t i = 0; i < TM_MAX_MEAT; i++ )
    {
        _meats[ i ].update();
    }

    /*HURTCIRCLE*/
    if( _bodyHC != nullptr )
    {
        *_bodyHC->area = { _xBuffer, _yBuffer, TM_BODY_RADIUS };
    }
}

void TabeMonster::draw( void )
{
    SDL_Color bodyColor = color::WHITE;
    if( _iTime < TM_INVICIBLE_TIME )
    {
        bodyColor.g = _iTime/TM_INVICIBLE_TIME*254;
        bodyColor.b = bodyColor.g;
    }

    SDL_Rect area = { int(_xBuffer - TM_BODY_RADIUS), int(_yBuffer - TM_BODY_RADIUS), TM_BODY_RADIUS*2, TM_BODY_RADIUS*2 };
    gTextures[ assets::texture::TABE_MONSTER ].render( area, bodyColor );

    if( SUCKING_IN == _lastAttack )
    {

    }

    for( uint8_t i = 0; i < TM_MAX_MEAT; i++ )
    {
        _meats[ i ].draw();
        #ifdef _GERT_DEBUG
        char foo[] = { char( i + '0' ), '\0' };
        gFont->render( _meats[ i ].get_area().ct_Rect(), foo, color::BLUE, true, true );
        #endif // _GERT_DEBUG
    }
}

void TabeMonster::start( const SDL_Point& pos )
{
    Actor::start( pos );

    _lastAttack = TOTAL;
    _timeSinceAttack = 0;
    _firstProc = false;
    _lastMeat = 0;
    _attackProcs = 0;
    _iTime = TM_INVICIBLE_TIME;

    _xDelta = _yDelta = 0;

    IF_HURT_CIRC( _bodyHC )
    {
        _healthPoints = TM_BASE_HEALTH;
    }
}

void TabeMonster::get_hurt( double damage, HurtCircle* from )
{
    if( from == _bodyHC && _iTime > TM_INVICIBLE_TIME )
    {
        _healthPoints -= damage;
        _iTime = 0;
    }
}

void TabeMonster::clean_up( void )
{
    for( uint8_t i = 0; i < TM_MAX_MEAT; i++ )
    {
        _meats[ i ].force_kill();
    }
    if( _bodyHC != nullptr )
    {
        _bodyHC->target = nullptr;
        _bodyHC = nullptr;
    }
}

void TabeMonster::cut_scene_update( void )
{
    if( get_dead() )
    {
        _iTime += gFrameTime;
        if( _iTime > TM_INVICIBLE_TIME/2 )
        {
            Particle* boom;
            IF_PARTICLE_GET( boom )
            {
                SDL_Rect splace = { int(_xBuffer - TM_BODY_RADIUS) + rand()%int(TM_BODY_RADIUS*2 - 300), int(_yBuffer - TM_BODY_RADIUS) + rand()%int(TM_BODY_RADIUS*2 - 300), 300, 300 };
                boom->start( splace, 0.8 );
                if( rand()%3 == 0 )
                    boom->myTexture = assets::texture::FLAME;
                else
                    boom->myTexture = assets::texture::EXPLOSION_0;
                boom->effect = Particle::flags( Particle::FADE_OUT | Particle::RISING );
                boom->angle = rand()%360;
            }
            _iTime = 0;
        }
    }
}

float TabeMonster::_attack_timer( attacks t )
{
    if( t == TOTAL )
    {
        t = _lastAttack;
    }
    switch( t )
    {
    case MEAT_SPIN:
        return 4;
    case RAM_MEAT:
        return 2;
    case MIX_UP_SHOT:
        return 1.6;
    case SUCKING_IN:
        return 2.5;
    case TOTAL:
        return 5;
    }
    return 0;
}

uint8_t TabeMonster::_total_procs( attacks t )
{
    if( TOTAL == t )
    {
        t = _lastAttack;
    }
    switch( t )
    {
    case MEAT_SPIN:
        return 1;
    case RAM_MEAT:
        return 2;
    case MIX_UP_SHOT:
        if( _healthPoints > TM_BASE_HEALTH/2 )
        {
            return 5;
        }
        else
        {
            return 10;
        }
    case SUCKING_IN:
        return 1;
    case TOTAL:
        return 1;
    }
    return 1;
}

uint8_t TabeMonster::_get_closest_meat( const SDL_Point& pos )
{
    uint8_t indexOfClose = 0;
    for( uint8_t i = 0; i < TM_MAX_MEAT; i++ )
    {
        if( _meats[ indexOfClose ].get_dead() && !_meats[ i ].get_dead() )
        {
            indexOfClose = i;
        }
        if( !_meats[ i ].get_dead() && collision::distance( pos, _meats[ i ].get_syncPoint() ) < collision::distance( pos, _meats[ indexOfClose ].get_syncPoint() ) )
        {
            indexOfClose = i;
        }
    }
    return indexOfClose;
}
