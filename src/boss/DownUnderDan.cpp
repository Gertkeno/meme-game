#include "boss/DownUnderDan.h"

#include <gert_SoundWrapper.h>
#include <gert_CameraMath.h>
#include <SDL2/SDL_shape.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <PushCircle.h>
#include <TexHolder.h>
#include <particle.h>
#include <Bullet.h>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define DU_BODY_RADIUS 880
#define DU_INVICIBLE_TIME 0.5
#define DU_BASE_HEALTH 80
#define DU_SPIN_TIME 0.3
#define DU_ATTACK_DELAY -0.45

#define DU_ANT_RADIUS 234
#define DU_ANT_DIST 5430
#define DU_ANT_HEAL ( ( ( DU_BASE_HEALTH - _healthPoints )/DU_BASE_HEALTH )*( DU_BASE_HEALTH*0.3 ) )

#define DU_MAX_SHOT_RNG 1400
#define DU_BULLETS_SHOT 3
#define DU_SHOT_SPREAD 10

#define DU_JUMP_DIST ( DU_BODY_RADIUS + 300 )

DownUnderDan::DownUnderDan()
{
    //ctor
    _antHC = nullptr;
    _bodyHC = nullptr;
    _iTime = 0;
    _shotPoints = new SDL_Point[ _total_procs( SHOOTING_SPREE )/2 ];
}

DownUnderDan::~DownUnderDan()
{
    //dtor
    delete[] _shotPoints;
}

void DownUnderDan::update( void )
{
    _spinning += gFrameTime;
    _iTime += gFrameTime;
    _timeSinceAttack += gFrameTime;

    /*ATTACKS MOVES ATTACK*/
    float ctTime = _attack_timer()/_total_procs();
    switch( _lastAttack )
    {
    case BOOMERANG:
        if( !_firstProc && _timeSinceAttack > 0 )
        {
            int tbn = 12;
            for( uint16_t i = 0; i < tbn; i++ )
            {
                Bullet* foo;
                IF_BULLET_GET( foo )
                {
                    foo->start( get_syncPoint(), this, float(i)/tbn*360, 3322, 1.8 );
                    foo->ability = bulletFunction::comeBack;
                    foo->myTexture = assets::texture::DOWN_UNDER;
                    foo->subTex = 3;
                    foo->totalAbilityCount = 2;
                    foo->damage = 4;
                    foo->radius = 84;
                }
            }
            _firstProc = true;
        }
        break;
    case JUMP_TO:
        if( !_firstProc && _timeSinceAttack > 0 )
        {
            /*play yeehaw*/
            gSound[ assets::sfx::DAN_SHORT_UH ].play();
            _firstProc = true;
        }
        else if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            if( _attackProcs == 0 )
            {
                float toPangle = angles::get_angle( get_syncPoint(), gGameManager->get_player_point() );
                float dist = collision::distance( get_syncPoint(), gGameManager->get_player_point() ) + DU_JUMP_DIST;
                _xDelta = cos( DT_PI( toPangle ) ) * ( dist/ (_attack_timer()*(_total_procs()-1)/_total_procs()) );
                _yDelta = sin( DT_PI( toPangle ) ) * ( dist/ (_attack_timer()*(_total_procs()-1)/_total_procs()) );
            }
            else if( _attackProcs == _total_procs() - 1 )
            {
                _xDelta = 0;
                _yDelta = 0;
            }
            _attackProcs += 1;
        }
        break;
    case SHOOTING_SPREE:
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            if( _attackProcs < _total_procs()/2 )
            {
                SDL_Point p = gGameManager->get_player_point();
                _shotPoints[ _attackProcs ] = { p.x - DU_MAX_SHOT_RNG/2 + rand()%DU_MAX_SHOT_RNG, p.y - DU_MAX_SHOT_RNG/2 + rand()%DU_MAX_SHOT_RNG };
                Particle* foo;
                IF_PARTICLE_GET( foo )
                {
                    int spreadSize = DU_SHOT_SPREAD * 4;
                    SDL_Rect splace = { _shotPoints[ _attackProcs ].x - spreadSize, _shotPoints[ _attackProcs ].y - spreadSize, spreadSize*2, spreadSize*2 };
                    foo->start( splace, _attack_timer()/2 );
                    foo->myTexture = assets::texture::DOWN_UNDER;
                    foo->subTex = 2;
                    foo->effect = Particle::FADE_IN;
                }
            }
            else
            {
                for( int i = 0; i < DU_BULLETS_SHOT; i++ )
                {
                    float toPAng = angles::get_angle( get_syncPoint(), _shotPoints[ _attackProcs - _total_procs()/2 ] );
                    toPAng = toPAng - DU_SHOT_SPREAD/2 + ( float( i )/DU_BULLETS_SHOT )*DU_SHOT_SPREAD;
                    if( toPAng < 0 )
                    {
                        toPAng += 360;
                    }
                    else if( toPAng > 360 )
                    {
                        toPAng -= 360;
                    }
                    Bullet* foo;
                    IF_BULLET_GET( foo )
                    {
                        foo->start( get_syncPoint(), this, toPAng, 5432, 2.2 );
                        foo->damage = 9;
                        foo->radius = 70;
                    }
                }
            }
            _attackProcs += 1;
        }
        break;
    case HONEY_ANT:
        if( _timeSinceAttack > 0 )
        {
            if( !_firstProc )
            {
                IF_HURT_CIRC( _antHC )
                {
                    float pangle = angles::get_angle( get_syncPoint(), gGameManager->get_player_point() );
                    pangle = pangle - 90 + (rand()%(180*3))/3;
                    if( pangle < 0 ) pangle += 360;
                    else if( pangle > 360 ) pangle -= 360;

                    _xAnt = cos( DT_PI( pangle ) ) * DU_ANT_DIST + _xBuffer;
                    _yAnt = sin( DT_PI( pangle ) ) * DU_ANT_DIST + _yBuffer;
                    _antHit = false;
                    _oddProc = false;
                    _firstProc = true;
                }
            }
            else
            {
                if( !_antHit )
                {
                    float dist = ( collision::distance( get_syncPoint(), SDL_Point( { (int)_xAnt, (int)_yAnt } ) ) - DU_BODY_RADIUS/2 )*( _timeSinceAttack/_attack_timer() );
                    float xAntDiff, yAntDiff;
                    collision::get_normal_diffXY( get_syncPoint(), SDL_Point( { (int)_xAnt, (int)_yAnt } ), &xAntDiff, &yAntDiff );
                    _xAnt += xAntDiff * dist * gFrameTime;
                    _yAnt += yAntDiff * dist * gFrameTime;
                }
                else if( !_oddProc )
                {
                    _timeSinceAttack = _attack_timer() + DU_ATTACK_DELAY;
                    _oddProc = true;
                }
            }
        }
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            if( !_antHit )
            {
                _healthPoints += DU_ANT_HEAL;
            }
            if( _antHC != nullptr )
            {
                _antHC->target = nullptr;
                _antHC = nullptr;
            }
        }
        break;
    case TOTAL:
        break;
    }
    /*MOVEMENT */
    _xBuffer += _xDelta * gFrameTime;
    _yBuffer += _yDelta * gFrameTime;

    /*Attack swapping*/
    if( _timeSinceAttack > _attack_timer() )
    {
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }
        _lastAttack = attacks( buffer );
        _attackProcs = 0;
        _timeSinceAttack = DU_ATTACK_DELAY;
        _firstProc = false;
        _spinning = 0;
    }

    /*HurtBox*/
    if( _bodyHC != nullptr )
    {
        *_bodyHC->area = { _xBuffer, _yBuffer, DU_BODY_RADIUS };
    }
    if( _antHC != nullptr )
    {
        *_antHC->area = { _xAnt, _yAnt, DU_ANT_RADIUS };
    }

    if( _spinning < DU_SPIN_TIME && _bodyHC != nullptr )
    {
        PushCircle* foo;
        IF_PUSH_CIRC( foo )
        {
            float nangle = angles::get_angle( get_syncPoint(), gGameManager->get_player_point() );
            foo->start( *_bodyHC->area, nangle, 800, _enemy, 6 );
        }
    }
}

void DownUnderDan::draw( void )
{
    /*HURT RED FLASH*/
    SDL_Color c = color::WHITE;
    if( _iTime < DU_INVICIBLE_TIME )
    {
        c.b = _iTime/DU_INVICIBLE_TIME*254;
        c.g = c.b;
    }
    SDL_Rect splace = { int( _xBuffer - DU_BODY_RADIUS ), int( _yBuffer - DU_BODY_RADIUS ), DU_BODY_RADIUS*2, DU_BODY_RADIUS*2 };
    /*SPINNING ANGLE*/
    float spinAng = 0;
    if( _spinning < DU_SPIN_TIME )
    {
        spinAng = _spinning/DU_SPIN_TIME*360;
    }
    gTextures[ assets::texture::DOWN_UNDER ].render( splace, c, 0, spinAng );

    if( _lastAttack == HONEY_ANT && _firstProc && !get_dead() )
    {
        float antAng = angles::get_angle( SDL_Point( { (int)_xAnt, (int)_yAnt } ), get_syncPoint() );
        SDL_Rect antSpt = { int( _xAnt - DU_ANT_RADIUS ), int( _yAnt - DU_ANT_RADIUS ), DU_ANT_RADIUS*2, DU_ANT_RADIUS*2 };
        gTextures[ assets::texture::DOWN_UNDER ].render( antSpt, color::WHITE, 1, antAng );
        int rsize = 180;
        antAng += 180;
        if( antAng > 360 ) antAng -= 360;
        float xc = cos( DT_PI( antAng ) ) * DU_BODY_RADIUS + _xBuffer;
        float yc = sin( DT_PI( antAng ) ) * DU_BODY_RADIUS + _yBuffer;
        SDL_Rect antPoint = { int( xc - rsize ), int( yc - rsize ), rsize*2, rsize*2 };
        antAng += 90;
        gTextures[ assets::texture::DOWN_UNDER ].render( antPoint, color::YELLOW, 3, antAng );
    }
}

void DownUnderDan::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _xDelta = 0;
    _yDelta = 0;
    _lastAttack = TOTAL;
    _attackProcs = 0;
    _timeSinceAttack = 0;
    _iTime = DU_INVICIBLE_TIME;
    _firstProc = false;
    _oddProc = false;
    _spinning = DU_SPIN_TIME;

    IF_HURT_CIRC( _bodyHC )
    {
        _healthPoints = DU_BASE_HEALTH;
    }
}

void DownUnderDan::get_hurt( double damage, HurtCircle* from )
{
    if( from == _antHC )
    {
        _antHit = true;
    }
    else if( _iTime > DU_INVICIBLE_TIME )
    {
        if( from == _bodyHC )
        {
            _iTime = 0;
            _healthPoints -= damage;
        }
    }
}

void DownUnderDan::cut_scene_update( void )
{
    if( get_dead() )
    {
        _iTime += gFrameTime;
        if( _iTime > DU_INVICIBLE_TIME/2 )
        {
            Particle* boom;
            IF_PARTICLE_GET( boom )
            {
                SDL_Rect splace = { int(_xBuffer - DU_BODY_RADIUS) + rand()%int(DU_BODY_RADIUS*2 - 300), int(_yBuffer - DU_BODY_RADIUS) + rand()%int(DU_BODY_RADIUS*2 - 300), 300, 300 };
                boom->start( splace, 0.8 );
                if( rand()%3 == 0 )
                    boom->myTexture = assets::texture::FLAME;
                else
                    boom->myTexture = assets::texture::EXPLOSION_0;
                boom->effect = Particle::flags( Particle::FADE_OUT | Particle::RISING );
                boom->angle = rand()%360;
            }
            _iTime = 0;
        }
    }
}

float DownUnderDan::_attack_timer( attacks t )
{
    if( TOTAL == t )
    {
        t = _lastAttack;
    }
    switch( t )
    {
    case BOOMERANG:
        return 1.6;
    case JUMP_TO:
        return 1.6;
    case SHOOTING_SPREE:
        return 2.4;
    case HONEY_ANT:
        return 4.6;
    case TOTAL:
        return 4;
    }
    return 1;
}

uint8_t DownUnderDan::_total_procs( attacks t )
{
    if( TOTAL == t )
    {
        t = _lastAttack;
    }
    switch( t )
    {
    case BOOMERANG:
        return 1;
    case JUMP_TO:
        return 4;
    case SHOOTING_SPREE:
        return 18;
    case HONEY_ANT:
        return 1;
    case TOTAL:
        return 1;
    }
    return 1;
}

void DownUnderDan::clean_up( void )
{
    if( _bodyHC != nullptr )
    {
        _bodyHC->target = nullptr;
        _bodyHC = nullptr;
    }
    if( _antHC != nullptr )
    {
        _antHC->target = nullptr;
        _antHC = nullptr;
    }
    _xBuffer = 0;
    _yBuffer = 0;
}
