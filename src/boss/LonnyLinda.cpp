#include "boss/LonnyLinda.h"
#include <SDL2/SDL_shape.h>

#include <gert_CameraMath.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <PushCircle.h>
#include <TexHolder.h>
#include <particle.h>
#include <Bullet.h>
#include <random>

#define LL_INVICIBLE_TIME 0.9
#define LL_BASE_HEALTH 125
#define LL_MAX_POINTS 4
#define LL_RADIUS 750
#define LL_MOVEMENT_SPEED ( (LL_BASE_HEALTH - _healthPoints) / LL_BASE_HEALTH * 300 + 250 )

#define LL_ATTACK_DELAY -0.314

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

LonnyLinda::LonnyLinda()
{
    //ctor
    _myHurtcs = new HurtCircle*[ LL_MAX_POINTS ];
    _myPoints = new SDL_Point[ LL_MAX_POINTS ];
    _pointITime = new float[ LL_MAX_POINTS ];
    for( unsigned int i = 0; i < LL_MAX_POINTS; i++ )
    {
        _myHurtcs[ i ] = nullptr;
        _myPoints[ i ] = { 0, 0 };
        _pointITime[ i ] = 0;
    }
    _healthPoints = 0;
}

LonnyLinda::~LonnyLinda()
{
    //dtor
    delete[] _myHurtcs;
    delete[] _myPoints;
    delete[] _pointITime;
}

void LonnyLinda::update( void )
{
    for( unsigned int i = 0; i < LL_MAX_POINTS; i++ )
    {
        _pointITime[ i ] += gFrameTime;
    }
    _attackTime += gFrameTime;

    float ctTime = _attack_timer( _lastAttack )/_total_procs( _lastAttack );
    switch( _lastAttack )
    {
    case CHANGE_ANGLE:
        if( _attackTime > ctTime )
        {
            _angle = _angle - 55 + (rand()%(110*3))/3;
        }
        break;
    case COAT_FRONT:
        if( !_firstProc && _attackTime >= 0 )
        {
            Bullet* foo;
            IF_BULLET_GET( foo )
            {
                foo->start( _myPoints[ 0 ], this, _angle, 1300, 3 );
                foo->damage = 11;
                foo->ability = bulletFunction::layDoTs;
                foo->totalAbilityCount = 8;
                foo->radius = 240;
                _firstProc = true;
            }
        }
        if( _attackTime > ctTime )
        {
            Bullet* foo;
            IF_BULLET_GET( foo )
            {
                float backAngle = angles::get_angle( _myPoints[ LL_MAX_POINTS - 1 ], _myPoints[ LL_MAX_POINTS - 2 ] ) - 180;
                if( backAngle < 0 ) backAngle += 360;

                foo->start( _myPoints[ LL_MAX_POINTS-1 ], this, backAngle, 1300, 3 );
                foo->damage = 11;
                foo->ability = bulletFunction::layDoTs;
                foo->totalAbilityCount = 8;
                foo->radius = 240;
            }
        }
        break;
    case BARRAGE:
        if( _attackTime - _attackProcs*ctTime > ctTime )
        {
            for( uint16_t i = 0; i < LL_MAX_POINTS; i++ )
            {
                Bullet* foo;
                IF_BULLET_GET( foo )
                {
                    foo->start( _myPoints[ i ], this, angles::get_angle( _myPoints[ 0 ], gGameManager->get_player_point() ), 2900, 2.8 );
                    foo->damage = 4;
                    foo->myTexture = assets::texture::LASOR_SHOT;
                }
            }
            _attackProcs += 1;
        }
        break;
    case HUUUU:
        if( _attackTime - _attackProcs*ctTime > ctTime )
        {
            int shootnum = 12;
            for( int bn = 0; bn < shootnum; bn++ )
            {
                Bullet* foo;
                IF_BULLET_GET( foo )
                {
                    foo->start( _myPoints[ _attackProcs ], this, float(bn)/float(shootnum)*360, 1400, 1.9 );
                    foo->damage = 8;
                    foo->myTexture = assets::texture::ORB_SHOT;
                }
            }
            _attackProcs += 1;
        }
        break;
    case TOTAL:
        break;
    }
    if( _attackTime > _attack_timer( _lastAttack ) )
    {
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }
        _lastAttack = attacks( buffer );
        _attackTime = LL_ATTACK_DELAY;
        _attackProcs = 0;
        _firstProc = false;
    }

    /*MOVEMENT STUFF*/
    _xBuffer += cos( _angle*M_PI/180 ) * LL_MOVEMENT_SPEED * gFrameTime;
    _yBuffer += sin( _angle*M_PI/180 ) * LL_MOVEMENT_SPEED * gFrameTime;
    /*POINT TO POINT PULLING THE CONVERSION */
    {
        _myPoints[ 0 ] = { (int)_xBuffer, (int)_yBuffer };
        for( uint16_t i = 0; i < LL_MAX_POINTS - 1; i++ )
        {
            float pointDist = collision::distance( _myPoints[ i ], _myPoints[ i+1 ] );
            if( pointDist > LL_RADIUS*2 )
            {
                float diffX, diffY;
                collision::get_normal_diffXY( _myPoints[ i ], _myPoints[ i+1 ], &diffX, &diffY );
                _myPoints[ i+1 ].x += diffX * ( pointDist - LL_RADIUS*2 );
                _myPoints[ i+1 ].y += diffY * ( pointDist - LL_RADIUS*2 );
            }
        }
    }

    /*HURT/HIT CIRCLE UPDATES*/
    for( uint16_t i = 0; i < LL_MAX_POINTS; i++ )
    {
        if( _myHurtcs[ i ] != nullptr )
        {
            *_myHurtcs[ i ]->area = { (float)_myPoints[ i ].x, (float)_myPoints[ i ].y, LL_RADIUS };
        }
    }
}

void LonnyLinda::draw( void )
{
    for( uint16_t i = 0; i < LL_MAX_POINTS; i++ )
    {
        float pangle;
        if( i > 0 )
            pangle = angles::get_angle( _myPoints[ i ], _myPoints[ i-1 ] );
        else
            pangle = _angle;

        int frame;
        if( i < LL_MAX_POINTS-1 && i != 0 )
            frame = 1;
        else if( i == 0 )
            frame = 0;
        else
            frame = 2;

        SDL_Color c = color::WHITE;
        if( _pointITime[ i ] < LL_INVICIBLE_TIME )
        {
            c.g = _pointITime[ i ]/LL_INVICIBLE_TIME*254;
            c.b = c.g;
        }

        SDL_Rect drawSpot = { _myPoints[ i ].x - LL_RADIUS, _myPoints[ i ].y - LL_RADIUS, LL_RADIUS*2, LL_RADIUS*2 };
        gTextures[ assets::texture::LONNY_LINDA ].render( drawSpot, c, frame, pangle );
    }
}

void LonnyLinda::get_hurt( double damage, HurtCircle* from )
{
    for( uint16_t i = 0; i < LL_MAX_POINTS; i++ )
    {
        if( from == _myHurtcs[ i ] && _pointITime[ i ] > LL_INVICIBLE_TIME )
        {
            float dmod = 1;
            if( i != 0 )
            {
                dmod = 0.8 / i;
            }
            _healthPoints -= damage * dmod;
            _pointITime[ i ] = 0;
        }
    }
}

void LonnyLinda::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _lastAttack = TOTAL;
    _angle = 0;

    bool nofail = true;
    for( uint16_t i = 0; i < LL_MAX_POINTS; i++ )
    {
        _myHurtcs[ i ] = gGameManager->get_hurt_circle( this );
        if( _myHurtcs[ i ] == nullptr )
        {
            nofail = false;
        }
        _myPoints[ i ] = { pos.x - i*LL_RADIUS, pos.y };
        _pointITime[ i ] = LL_INVICIBLE_TIME;
    }

    if( nofail )
    {
        _healthPoints = LL_BASE_HEALTH;
    }
    _firstProc = false;
}

void LonnyLinda::clean_up( void )
{
    for( unsigned int i = 0; i < LL_MAX_POINTS; i++ )
    {
        if( _myHurtcs[ i ] != nullptr )
        {
            _myHurtcs[ i ]->target = nullptr;
            _myHurtcs[ i ] = nullptr;
        }
    }
}

void LonnyLinda::cut_scene_update( void )
{
    if( get_dead() )
    {
        for( unsigned int i = 0; i < LL_MAX_POINTS; i++ )
        {
            _pointITime[ i ] += gFrameTime;
            if( _pointITime[ i ] > LL_INVICIBLE_TIME/2 )
            {
                SDL_Rect prect = { rand()%LL_RADIUS*2 + _myPoints[ i ].x - LL_RADIUS, rand()%LL_RADIUS*2 + _myPoints[ i ].y - LL_RADIUS, 300, 300 };
                Particle* foo;
                IF_PARTICLE_GET( foo )
                {
                    foo->start( prect, 1 );
                    foo->myTexture = assets::texture::EXPLOSION_0;
                    foo->angle = rand()%360;
                    foo->effect = Particle::FADE_OUT;
                }
                _pointITime[ i ] = 0;
            }
        }
    }
}

void LonnyLinda::push_me( float xd, float yd, HurtCircle* from )
{
    if( from == _myHurtcs[ 0 ] )
    {
        HealthyActor::push_me( xd, yd, from );
    }
}

float LonnyLinda::_attack_timer( attacks t )
{
    switch( t )
    {
    case CHANGE_ANGLE:
        return 1;
    case BARRAGE:
        return 2.8;
    case HUUUU:
        if( _healthPoints > LL_BASE_HEALTH/2 )
        {
            return 3;
        }
        else
        {
            return 2;
        }
    case COAT_FRONT:
        return 2;
    case TOTAL:
        return 5;
    }
    return 0;
}

uint8_t LonnyLinda::_total_procs( attacks t )
{
    switch( t )
    {
    case BARRAGE:
        if( _healthPoints > LL_BASE_HEALTH/2 )
        {
            return 8;
        }
        else
        {
            return 12;
        }
    case CHANGE_ANGLE:
        return 1;
    case HUUUU:
        return LL_MAX_POINTS;
    case COAT_FRONT:
        return 1;
    case TOTAL:
        return 1;
    }
    return 1;
}
