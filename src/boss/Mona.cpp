#include "Mona.h"

#include <gert_SoundWrapper.h>
#include <gert_CameraMath.h>
#include <SDL2/SDL_shape.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <PushCircle.h>
#include <TexHolder.h>
#include <particle.h>
#include <Bullet.h>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define MO_BASE_HEALTH 110
#define MO_INVICIBLE_TIME 0.6

/**ATTACK CONSTANTS*/
#define MO_ATTACK_DELAY -0.6

#define MO_TETHER_RADIUS 200
#define MO_TETHER_DIST 6000
#define MO_SHOOTY_ANGLE 10
#define MO_WALL_POINTS 7
#define MO_RUSSER_RADIUS 450

/**IMAGE CONSTANTS*/
#define MO_SCALE (1900)
#define MO_WIDTH ( 0.6189710610932476 * MO_SCALE )
#define MO_HEIGHT ( 0.38102893890675243 * MO_SCALE )

#define MO_HIRT_XOFFSET ( 0.18961038961038962 * MO_SCALE )


Mona::Mona()
{
    //ctor
    _grapplePoint = new SDL_Point;
    _wallpoint = new SDL_Point[ MO_WALL_POINTS ];
}

Mona::~Mona()
{
    //dtor
    delete _grapplePoint;
    delete[] _wallpoint;
}

void Mona::update( void )
{
    _timeSinceAttack += gFrameTime;
    _iTime += gFrameTime;

    float ctTime = _attack_timer()/_total_procs();
    if( _timeSinceAttack > 0 )
    {
        switch( _lastAttack )
        {
        case RUSSER:
            {
                uint8_t furthestWall = 0;
                for( uint8_t i = 0; i < MO_WALL_POINTS; i++ )
                {
                    if( collision::distance( get_syncPoint(), _wallpoint[ i ] ) > collision::distance( get_syncPoint(), _wallpoint[ furthestWall ] ) )
                    {
                        furthestWall = i;
                    }
                }
                if( collision::distance( get_syncPoint(), _wallpoint[ furthestWall ] ) > MO_HEIGHT )
                {
                    float nangle = _timeSinceAttack/_attack_timer() * 360 + angles::get_angle( get_syncPoint(), _wallpoint[ furthestWall ] );
                    if( nangle > 360 ) nangle -= 360;

                    float dist = collision::distance( get_syncPoint(), _wallpoint[ furthestWall ] );
                    _grapplePoint->x = cos( DT_PI( nangle ) ) * dist;
                    _grapplePoint->y = sin( DT_PI( nangle ) ) * dist;

                    HitCircle* foo;
                    IF_HIT_CIRC( foo )
                    {
                        foo->start( { float(_grapplePoint->x), float(_grapplePoint->y), MO_RUSSER_RADIUS }, 5, _enemy );
                    }
                }
            }
            break;
        case WALL:
            if( !_firstProc )
            {
                *_grapplePoint = gGameManager->get_player_point();
                float diffx, diffy;
                collision::get_normal_diffXY( get_syncPoint(), *_grapplePoint, &diffx, &diffy );
                _grapplePoint->x -= diffx*400;
                _grapplePoint->y -= diffy*400;
                _firstProc = true;
            }
            if( _timeSinceAttack > ctTime )
            {
                _wallpoint[ _lastWall++ ] = *_grapplePoint;
                if( _lastWall > MO_WALL_POINTS )
                {
                    _lastWall = 0;
                }
            }
            break;
        case SHOOTY:
            if( !_firstProc )
            {
                *_grapplePoint = gGameManager->get_player_point();
                _firstProc = true;
            }
            if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
            {
                uint8_t total = 1 + _attackProcs*2;
                for( uint8_t i = 0; i < total; i++ )
                {
                    Bullet* foo;
                    IF_BULLET_GET( foo )
                    {
                        float nanlge = angles::get_angle( get_syncPoint(), *_grapplePoint ) - (MO_SHOOTY_ANGLE/2)*_attackProcs + (float(i)/float(total))*(MO_SHOOTY_ANGLE*_attackProcs);
                        foo->start( get_syncPoint(), this, nanlge, 2500, 3 );
                        foo->damage = 7;
                    }
                }
                _attackProcs += 1;
            }
            break;
        case TETHER:
            if( !_firstProc )
            {
                *_grapplePoint = gGameManager->get_player_point();
                float diffX, diffY;
                collision::get_normal_diffXY( get_syncPoint(), *_grapplePoint, &diffX, &diffY );

                *_grapplePoint = { int(MO_TETHER_DIST * -diffX), int(MO_TETHER_DIST * -diffY) };
                gSound[ assets::sfx::OVER_HERE ].play();
                _firstProc = true;
            }
            else if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
            {
                _attackProcs += 1;
            }

            {
                float dist = 0;
                if( _attackProcs == 0 )
                {
                    float percentDone = _timeSinceAttack/(_attack_timer()/2);
                    dist = collision::distance( get_syncPoint(), *_grapplePoint ) * percentDone;
                }
                else
                {
                    float percentDone = (_timeSinceAttack-_attack_timer()/2)/(_attack_timer()/2);
                    dist = collision::distance( get_syncPoint(), *_grapplePoint );
                    dist = dist - dist * percentDone;
                }

                if( dist > MO_HEIGHT )
                {
                    float diffX, diffY;
                    collision::get_normal_diffXY( get_syncPoint(), *_grapplePoint, &diffX, &diffY );
                    PushCircle* foo;
                    IF_PUSH_CIRC( foo )
                    {
                        Circle area = { dist * -diffX, dist * -diffY, MO_TETHER_RADIUS };
                        foo->start( area, angles::get_angle( gGameManager->get_player_point(), SDL_Point({ int(area.x), int(area.y) }) ), MO_TETHER_DIST/(_attack_timer()/2) + 900, _enemy, 6 );
                    }
                }
            }
        case TOTAL:
            break;
        }
    }

    /*NEW ATTACK*/
    if( _timeSinceAttack > _attack_timer() )
    {
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }
        _lastAttack = attacks( buffer );
        _attackProcs = 0;
        _timeSinceAttack = MO_ATTACK_DELAY;
        _firstProc = false;
    }

    /*MOVEMENT*/
    for( uint8_t i = 0; i < MO_WALL_POINTS; i++ )
    {
        if( collision::distance( _wallpoint[ i ], get_syncPoint() ) > MO_HEIGHT && collision::distance( get_syncPoint(), gGameManager->get_player_point() ) < collision::distance( get_syncPoint(), _wallpoint[ i ] ) )
        {
            float xproj, yproj;
            collision::get_normal_diffXY( get_syncPoint(), _wallpoint[ i ], &xproj, &yproj );

            PushCircle* foo;
            IF_PUSH_CIRC( foo )
            {
                float dist = collision::distance( get_syncPoint(), gGameManager->get_player_point() );
                Circle area;
                area.x = -xproj*dist;
                area.y = -yproj*dist;
                area.r = MO_TETHER_RADIUS*0.8;

                foo->start( area, angles::get_angle( area, gGameManager->get_player_point() ), 901, _enemy );
            }
        }
    }

    /*HURTCIRC*/
    if( _bodyHC != nullptr )
    {
        *_bodyHC->area = { _xBuffer, _yBuffer, MO_HEIGHT };
    }
}

void Mona::draw( void )
{
    if( _lastAttack == TETHER && _timeSinceAttack > 0 )
    {
        float dist = 0;
        if( _attackProcs == 0 )
        {
            float percentDone = _timeSinceAttack/(_attack_timer()/2);
            dist = collision::distance( get_syncPoint(), *_grapplePoint ) * percentDone;
        }
        else
        {
            float percentDone = (_timeSinceAttack-_attack_timer()/2)/(_attack_timer()/2);
            dist = collision::distance( get_syncPoint(), *_grapplePoint );
            dist = dist - dist * percentDone;
        }

        if( dist > MO_TETHER_RADIUS )
        {
            float sangle = angles::get_angle( get_syncPoint(), *_grapplePoint );
            float diffX, diffY;
            collision::get_normal_diffXY( get_syncPoint(), *_grapplePoint, &diffX, &diffY );

            uint16_t total = dist/(MO_TETHER_RADIUS*2) + 2;
            Circle splaces[ total ];
            for( uint16_t i = 0; i < total; i++ )
            {
                splaces[ i ] = { ( -diffX * dist ) + diffX * ( MO_TETHER_RADIUS*2 * i ), ( -diffY * dist ) + diffY * ( MO_TETHER_RADIUS*2 * i ), MO_TETHER_RADIUS };
                uint8_t frame = 2;
                if( i == 0 )
                {
                    frame = 3;
                }
                gTextures[ assets::texture::MONA ].render( splaces[ i ].ct_Rect(), color::WHITE, frame, sangle );
            }
        }
    }//_lastAttack == TETHER

    ///Wall drawing
    for( uint8_t i = 0; i < MO_WALL_POINTS; i++ )
    {
        if( _lastAttack == WALL && _lastWall == i && _timeSinceAttack > 0 )
        {
            uint32_t dist = collision::distance( *_grapplePoint, get_syncPoint() );
            SDL_Rect splace = { int(_xBuffer), 0, (int)dist, int( _timeSinceAttack/_attack_timer() * MO_TETHER_RADIUS*2 ) };
            SDL_Point rp = { 0, splace.h/2 };
            splace.y = _yBuffer - splace.h/2;

            SDL_Color c = color::WHITE;
            c.a = _timeSinceAttack/_attack_timer()*254;
            gTextures[ assets::texture::MONA ].render( splace, c, 2, angles::get_angle( get_syncPoint(), *_grapplePoint ), true, SDL_FLIP_NONE, &rp );
        }
        if( collision::distance( _wallpoint[ i ], get_syncPoint() ) > MO_HEIGHT )
        {
            uint32_t dist = collision::distance( _wallpoint[ i ], get_syncPoint() );
            SDL_Rect splace = { int(_xBuffer), int(_yBuffer - MO_TETHER_RADIUS), (int)dist, MO_TETHER_RADIUS*2 };
            SDL_Point rp = { 0, splace.h/2 };
            gTextures[ assets::texture::MONA ].render( splace, color::WHITE, 2, angles::get_angle( get_syncPoint(), _wallpoint[ i ] ), true, SDL_FLIP_NONE, &rp );
        }
    }

    ///Russer drawing
    if( _lastAttack == RUSSER && _timeSinceAttack > 0 )
    {
        uint8_t furthestWall = 0;
        for( uint8_t i = 0; i < MO_WALL_POINTS; i++ )
        {
            if( collision::distance( _wallpoint[ i ], get_syncPoint() ) > collision::distance( _wallpoint[ furthestWall ], get_syncPoint() ) )
            {
                furthestWall = i;
            }
        }
        if( collision::distance( _wallpoint[ furthestWall ], get_syncPoint() ) > MO_HEIGHT )
        {
            SDL_Rect splace { _grapplePoint->x - MO_RUSSER_RADIUS, _grapplePoint->y - MO_RUSSER_RADIUS, MO_RUSSER_RADIUS*2, MO_RUSSER_RADIUS*2 };
            float nangle = _timeSinceAttack/_attack_timer()*360;
            gTextures[ assets::texture::MONA ].render( splace, color::WHITE, 4, nangle );
        }
    }

    ///Mona drawing
    SDL_Color c = color::WHITE;
    if( _iTime < MO_INVICIBLE_TIME )
    {
        c.g = _iTime/MO_INVICIBLE_TIME*(254 - 90) + 90;
        c.b = c.g;
    }

    SDL_Rect splace = { int(_xBuffer - MO_WIDTH), int(_yBuffer - MO_HEIGHT), int(MO_WIDTH*2), int(MO_HEIGHT*2) };
    gTextures[ assets::texture::MONA ].render( splace, c, 1 );
}

void Mona::start( const SDL_Point& pos )
{
    Actor::start( pos );

    _iTime = MO_INVICIBLE_TIME;
    _timeSinceAttack = 0;
    _lastAttack = TOTAL;
    _firstProc = false;
    *_grapplePoint = pos;
    _lastWall = 0;
    for( uint8_t i = 0; i < MO_WALL_POINTS; i++ )
    {
        _wallpoint[ i ] = pos;
    }

    IF_HURT_CIRC( _bodyHC )
    {
        _healthPoints = MO_BASE_HEALTH;
    }
}

void Mona::get_hurt( double damage, HurtCircle* from )
{
    if( _iTime > MO_INVICIBLE_TIME && _bodyHC == from )
    {
        _healthPoints -= damage;
        _iTime = 0;
    }
}

void Mona::clean_up( void )
{
    if( _bodyHC != nullptr )
    {
        _bodyHC->target = nullptr;
        _bodyHC = nullptr;
    }
}

void Mona::cut_scene_update( void )
{
    if( get_dead() )
    {
        _iTime += gFrameTime;
        if( _iTime > MO_INVICIBLE_TIME/2 )
        {
            Particle* boom;
            IF_PARTICLE_GET( boom )
            {
                SDL_Rect splace = { int(_xBuffer - MO_WIDTH/2) + rand()%int(MO_WIDTH), int(_yBuffer - MO_HEIGHT/2) + rand()%int(MO_HEIGHT), 300, 300 };
                boom->start( splace, 0.8 );
                if( rand()%3 == 0 )
                    boom->myTexture = assets::texture::FLAME;
                else
                    boom->myTexture = assets::texture::EXPLOSION_0;
                boom->effect = Particle::flags( Particle::FADE_OUT | Particle::RISING );
                boom->angle = rand()%360;
            }
            _iTime = 0;
        }
    }
}

float Mona::_attack_timer( attacks t )
{
    if( t == TOTAL )
    {
        t = _lastAttack;
    }

    switch( t )
    {
    case RUSSER:
        return 3.5;
    case WALL:
        return 2;
    case SHOOTY:
        return 2;
    case TETHER:
        return 3;
    case TOTAL:
        return 5;
    }

    return 0;
}

uint8_t Mona::_total_procs( attacks t )
{
    if( t == TOTAL )
    {
        t = _lastAttack;
    }

    switch( t )
    {
    case RUSSER:
        return 1;
    case WALL:
        return 1;
    case SHOOTY:
        return 7;
    case TETHER:
        return 2;
    case TOTAL:
        return 1;
    }

    return 1;
}
