#include "ChaoHibby.h"

#include <gert_CameraMath.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <PushCircle.h>
#include <TexHolder.h>
#include <particle.h>
#include <Bullet.h>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define CH_SPEED 70
#define CH_INVICIBLE_TIME 0.4
#define CH_ATTACK_DELAY -0.6
#define CH_BASE_HEALTH 109
#define CH_SCALE ( _healthPoints/CH_BASE_HEALTH * (1400) + (900) )
#define CH_BALL_RADIUS 200

#define CH_EVIL_WIDTH ( 0.4821092278719397 * CH_SCALE )
#define CH_EVIL_HEIGHT ( ( 1 - CH_EVIL_WIDTH/CH_SCALE ) * CH_SCALE )

#define CH_HERO_WIDTH ( 0.4115138592750533 * CH_SCALE )
#define CH_HERO_HEIGHT ( ( 1- CH_HERO_WIDTH/CH_SCALE ) * CH_SCALE )

/*HURT CIRCLE CONSTANTS BASED ON IMAGE RATIO*/
#define CH_HH1R ( 0.16204690831556504 * CH_SCALE )
#define CH_HH1X ( 0.18550106609808104 * CH_SCALE )
#define CH_HH1Y ( 0.22174840085287847 * CH_SCALE )
#define CH_HH2R ( 0.10660980810234541 * CH_SCALE )
#define CH_HH2X ( 0.24093816631130063 * CH_SCALE )
#define CH_HH2Y ( 0.47547974413646055 * CH_SCALE )

#define CH_EH1R ( 0.19397363465160075 * CH_SCALE )
#define CH_EH1X ( 0.20527306967984935 * CH_SCALE )
#define CH_EH1Y ( 0.22033898305084745 * CH_SCALE )
#define CH_EH2R ( 0.09981167608286252 * CH_SCALE )
#define CH_EH2X ( 0.295668549905838 * CH_SCALE )  ///these two just divided nicely
#define CH_EH2Y ( 0.4086629001883239 * CH_SCALE )

/*ATTACK CONSTANTS*/
#define CH_PURIFY_DIST 1200
#define CH_BALL_MOVE_OFFSET 800
#define CH_BASTION_ANGLE_OFFSET 3
#define CH_BASTION_WARM 0.7


ChaoHibby::ChaoHibby()
{
    //ctor
    _bodyHC = new HurtCircle*[ 2 ];
    _ballPoint = new SDL_Point;
    _ballTo = new SDL_Point;
    _iTime = 0;
}

ChaoHibby::~ChaoHibby()
{
    //dtor
    delete[] _bodyHC;
    delete _ballPoint;
    delete _ballTo;
}

void ChaoHibby::update( void )
{
    _timeSinceAttack += gFrameTime;
    _iTime += gFrameTime;

    if( _timeSinceAttack > 0 )
    {
        float ctTime = _attack_timer()/_total_procs();
        switch( _lastAttack )
        {
        case BASTION:
            if( _evil )
            {
                if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
                {
                    float bspeed = 2400;
                    float blife = 1.4;
                    float bdamage = 5;
                    Bullet* ballB;
                    IF_BULLET_GET( ballB )
                    {
                        float bangle = angles::get_angle( *_ballPoint, gGameManager->get_player_point() );
                        bangle += CH_BASTION_ANGLE_OFFSET * _attackProcs;
                        if( bangle > 360 )
                        {
                            bangle -= 360;
                        }
                        ballB->start( *_ballPoint, this, bangle, bspeed, blife );
                        ballB->damage = bdamage;
                    }

                    Bullet* meB;
                    IF_BULLET_GET( meB )
                    {
                        float bangle = angles::get_angle( get_syncPoint(), gGameManager->get_player_point() );
                        bangle += CH_BASTION_ANGLE_OFFSET * _attackProcs;
                        if( bangle > 360 )
                        {
                            bangle -= 360;
                        }
                        meB->start( get_syncPoint(), this, bangle, bspeed, blife );
                        meB->damage = bdamage;
                    }
                    _attackProcs += 1;
                }
            }//dark
            else
            {
                if( _timeSinceAttack > CH_BASTION_WARM )
                {
                    float distFromBall = collision::distance( get_syncPoint(), *_ballPoint );
                    SDL_Point cplayer = gGameManager->get_player_point();
                    if( (collision::distance( get_syncPoint(), cplayer ) < distFromBall && collision::distance( *_ballPoint, cplayer ) < distFromBall) )
                    {
                        HitCircle* foo;
                        IF_HIT_CIRC( foo )
                        {
                            float diffx, diffy;
                            collision::get_normal_diffXY( get_syncPoint(), *_ballPoint, &diffx, &diffy );
                            float placementDist = collision::distance( get_syncPoint(), gGameManager->get_player_point() );
                            Circle area;
                            area.x = -diffx * placementDist + _xBuffer;
                            area.y = -diffy * placementDist + _yBuffer;
                            area.r = CH_BALL_RADIUS;
                            foo->start( area, 8, _enemy );

                            PushCircle* puller;
                            IF_PUSH_CIRC( puller )
                            {
                                area.r = collision::distance( area, cplayer );
                                puller->start( area, angles::get_angle( cplayer, area ), 700, _enemy );
                            }
                        }

                    }//(collision::distance( get_syncPoint(), cplayer ) < distFromBall && collision::distance( *_ballPoint, cplayer ) < distFromBall)
                }//_timeSinceAttack > CH_BASTION_WARM
            }//hero
            break;
        case BALL_MOVE:
            {
                *_ballTo = gGameManager->get_player_point();
                if( _evil )
                {
                    _ballTo->y += CH_BALL_MOVE_OFFSET;
                }
                else
                {
                    _ballTo->y -= CH_BALL_MOVE_OFFSET;
                }
                float xBallDiff, yBallDiff;
                collision::get_normal_diffXY( *_ballPoint, *_ballTo, &xBallDiff, &yBallDiff );
                float dist = collision::distance( *_ballPoint, *_ballTo ) * ( _timeSinceAttack/_attack_timer() ) * 4;

                HitCircle* foo;
                IF_HIT_CIRC( foo )
                {
                    foo->start( { float(_ballPoint->x), float(_ballPoint->y), CH_BALL_RADIUS }, 6, _enemy );
                }

                _ballPoint->x -= xBallDiff * dist * gFrameTime;
                _ballPoint->y -= yBallDiff * dist * gFrameTime;
            }
            break;
        case PURIFY:
            if( !_firstProc )
            {
                if( !_evil )
                {
                    _firstProc = true;
                }
                else
                {
                    int tb = 5;
                    for( uint8_t i = 0; i < tb; i++ )
                    {
                        float fangle = float(i)/float(tb)*360;
                        Bullet* foo;
                        IF_BULLET_GET( foo )
                        {
                            foo->start( get_syncPoint(), this, fangle, 1987, 1.8 );
                            foo->damage = 3;
                        }
                    }
                    _firstProc = true;
                }
            }
            if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
            {
                if( _evil )
                {
                    int tb = 5;
                    for( uint8_t i = 0; i < tb; i++ )
                    {
                        float fangle = float(i)/float(tb)*360;
                        Circle splace;
                        splace.x = cos( DT_PI( fangle ) ) * CH_PURIFY_DIST * ( _attackProcs + 1 ) + _xBuffer;
                        splace.y = sin( DT_PI( fangle ) ) * CH_PURIFY_DIST * ( _attackProcs + 1 ) + _yBuffer;
                        splace.r = 330;

                        float ptime = 2.6;
                        HitCircle* hflame;
                        IF_HIT_CIRC( hflame )
                        {
                            hflame->start( splace, 9, _enemy );
                            hflame->totalLifeTime = ptime;
                        }

                        Particle* pflame;
                        IF_PARTICLE_GET( pflame )
                        {
                            splace.r = 400;
                            pflame->start( splace.ct_Rect(), ptime );
                            pflame->myTexture = assets::texture::FLAME;
                            pflame->effect = Particle::FADE_OUT;
                        }
                    }
                }
                else
                {
                    float pangle = angles::get_angle( *_ballPoint, gGameManager->get_player_point() );

                    Bullet* foo;
                    IF_BULLET_GET( foo )
                    {
                        foo->start( *_ballPoint, this, pangle, 2100, 3 );
                        foo->ability = bulletFunction::recursiveShot;
                        foo->myTexture = assets::texture::ORB_SHOT;
                        foo->totalAbilityCount = 4;
                        foo->damage = 6;
                        foo->radius = 100;
                    }
                }
                _attackProcs += 1;
            }//_timeSinceAttack - _attackProcs*ctTime > ctTime
            break;
        case EVOLVE:
            if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
            {
                if( _attackProcs == 0 )
                {
                    uint8_t tb = 60;
                    for( uint8_t i = 0; i < tb; i++ )
                    {
                        Bullet* foo;
                        IF_BULLET_GET( foo )
                        {
                            foo->start( get_syncPoint(), this, float( i )/float( tb )*360, 2000, 0.7 );
                            foo->damage = 5;
                            foo->radius = 110;
                        }
                    }
                    _evil = !_evil;
                }
                _attackProcs += 1;
            }
            break;
        case TOTAL:
            break;
        }
    }

    /*ATTACK RESET*/
    if( _timeSinceAttack > _attack_timer() )
    {
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }
        _lastAttack = attacks( buffer );
        _attackProcs = 0;
        _timeSinceAttack = CH_ATTACK_DELAY;
        _firstProc = false;
    }

    /*MOVEMENT*/
    if( _lastAttack != EVOLVE )
    {
        float diffx, diffy;
        collision::get_normal_diffXY( get_syncPoint(), gGameManager->get_player_point(), &diffx, &diffy );
        _xBuffer += diffx * CH_SPEED * gFrameTime;
        _yBuffer += diffy * CH_SPEED * gFrameTime;
    }

    /*BODY*/
    if( _bodyHC[ 0 ] != nullptr && _bodyHC[ 1 ] != nullptr )
    {
        if( _evil )
        {
            *_bodyHC[ 0 ]->area = { float(CH_EH1X*2 + _xBuffer - CH_EVIL_WIDTH), float(CH_EH1Y*2 + _yBuffer - CH_EVIL_HEIGHT), float(CH_EH1R * 2) };
            *_bodyHC[ 1 ]->area = { float(CH_EH2X*2 + _xBuffer - CH_EVIL_WIDTH), float(CH_EH2Y*2 + _yBuffer - CH_EVIL_HEIGHT), float(CH_EH2R * 2) };
        }
        else
        {
            *_bodyHC[ 0 ]->area = { float(CH_HH1X*2 + _xBuffer - CH_HERO_WIDTH), float(CH_HH1Y*2 + _yBuffer - CH_HERO_HEIGHT), float(CH_HH1R * 2) };
            *_bodyHC[ 1 ]->area = { float(CH_HH2X*2 + _xBuffer - CH_HERO_WIDTH), float(CH_HH2Y*2 + _yBuffer - CH_HERO_HEIGHT), float(CH_HH2R * 2) };
        }
    }
}

void ChaoHibby::draw( void )
{
    ///bastion rendering
    if( !_evil && _lastAttack == BASTION && _timeSinceAttack > 0 )
    {
        SDL_Rect pLaser = { (int)_xBuffer, int(_yBuffer - CH_BALL_RADIUS), int(collision::distance( get_syncPoint(), *_ballPoint )), CH_BALL_RADIUS*2 };
        SDL_Color c = color::CYAN;
        if( _timeSinceAttack < CH_BASTION_WARM )
        {
            pLaser.h /= 4;
            pLaser.y = _yBuffer - pLaser.h/2;
            c.a = 100;
        }
        else
        {
            if( int(_timeSinceAttack*100)%30 > 15 )
            {
                c.a = 120;
            }
        }
        SDL_Point rp = { 0, pLaser.h/2 };

        gTextures[ assets::texture::MAKIN_BACON_BOT ].render( pLaser, c, 3, angles::get_angle( get_syncPoint(), *_ballPoint ), true, SDL_FLIP_VERTICAL, &rp );
    }

    ///Body rendering
    {
        SDL_Color c = color::WHITE;
        if( _iTime < CH_INVICIBLE_TIME )
        {
            c.g = _iTime/CH_INVICIBLE_TIME * 254;
            c.b = c.g;
        }

        SDL_Rect splace;
        if( _evil )
        {
            splace = { int( _xBuffer - CH_EVIL_WIDTH ), int( _yBuffer - CH_EVIL_HEIGHT ), int(CH_EVIL_WIDTH*2), int(CH_EVIL_HEIGHT*2) };
        }
        else
        {
            splace = { int( _xBuffer - CH_HERO_WIDTH ), int( _yBuffer - CH_HERO_HEIGHT ), int(CH_HERO_WIDTH*2), int(CH_HERO_HEIGHT*2) };
        }
        uint8_t frame = 1;
        if( _evil )
        {
            frame = 2;
        }

        gTextures[ assets::texture::CHAO_HIBBY ].render( splace, c, frame );

        if( _lastAttack == EVOLVE && _timeSinceAttack > 0 )
        {
            SDL_Color ec = color::WHITE;
            ec.a = _timeSinceAttack/(_attack_timer()/2)*254;
            if( _attackProcs == 1 )
            {
                ec.a = ( _attack_timer()/2 - (_timeSinceAttack - _attack_timer()/2) )/(_attack_timer()/2) * 254;
            }

            gTextures[ assets::texture::CHAO_HIBBY ].render( splace, ec, 3 );
        }
    }

    ///ball warning/track rendering
    if( ( _lastAttack == BALL_MOVE ) && _timeSinceAttack > 0 )
    {
        SDL_Rect place = { _ballPoint->x, _ballPoint->y - CH_BALL_RADIUS, int( collision::distance( *_ballPoint, *_ballTo ) ), CH_BALL_RADIUS*2 };
        SDL_Point rp = { 0, place.h/2 };
        SDL_Color c = color::YELLOW;
        if( _evil )
        {
            c = color::RED;
        }
        c.a = ( _attack_timer() - _timeSinceAttack )/_attack_timer()*254;

        gTextures[ assets::texture::TRACK ].render( place, c, 0, angles::get_angle( *_ballPoint, *_ballTo ), true, SDL_FLIP_NONE, &rp );
    }

    ///halo/point rendering
    {
        uint8_t frame = 4;
        if( _evil )
        {
            frame = 5;
        }
        SDL_Rect place = { _ballPoint->x - CH_BALL_RADIUS, _ballPoint->y - CH_BALL_RADIUS, CH_BALL_RADIUS*2, CH_BALL_RADIUS*2 };
        SDL_Color c = color::WHITE;
        gTextures[ assets::texture::CHAO_HIBBY ].render( place, c, frame );
    }
}

void ChaoHibby::start( const SDL_Point& pos )
{
    HealthyActor::start( pos );

    _lastAttack = TOTAL;
    _attackProcs = 0;
    _timeSinceAttack = 0;
    _iTime = CH_INVICIBLE_TIME;
    _evil = false;
    _firstProc = false;

    IF_HURT_CIRC( _bodyHC[ 0 ] )
    {
        IF_HURT_CIRC( _bodyHC[ 1 ] )
        {
            _healthPoints = CH_BASE_HEALTH;
        }
    }
    *_ballPoint = pos;
}

void ChaoHibby::get_hurt( double damage, HurtCircle* from )
{
    if( _iTime > CH_INVICIBLE_TIME && ( _bodyHC[ 0 ] == from || _bodyHC[ 1 ] == from ) )
    {
        if( _lastAttack != EVOLVE )
        {
            _healthPoints -= damage;
        }
        else
        {
            _healthPoints -= damage*0.3;
        }
        _iTime = 0;
    }
}

void ChaoHibby::clean_up( void )
{
    if( _bodyHC[ 0 ] != nullptr )
    {
        _bodyHC[ 0 ]->target = nullptr;
        _bodyHC[ 0 ] = nullptr;
    }
    if( _bodyHC[ 1 ] != nullptr )
    {
        _bodyHC[ 1 ]->target = nullptr;
        _bodyHC[ 1 ] = nullptr;
    }
}

void ChaoHibby::cut_scene_update( void )
{
    if( get_dead() )
    {
        _iTime += gFrameTime;
        if( _iTime > CH_INVICIBLE_TIME/2 )
        {
            Particle* boom;
            IF_PARTICLE_GET( boom )
            {
                SDL_Rect splace = { int(_xBuffer - CH_EVIL_WIDTH/2) + rand()%int(CH_EVIL_WIDTH), int(_yBuffer - CH_EVIL_HEIGHT/2) + rand()%int(CH_EVIL_HEIGHT), 300, 300 };
                boom->start( splace, 0.8 );
                if( rand()%3 == 0 )
                    boom->myTexture = assets::texture::FLAME;
                else
                    boom->myTexture = assets::texture::EXPLOSION_0;
                boom->effect = Particle::flags( Particle::FADE_OUT | Particle::RISING );
                boom->angle = rand()%360;
            }
            _iTime = 0;
        }
    }
}

float ChaoHibby::_attack_timer( attacks t )
{
    if( TOTAL == t )
    {
        t = _lastAttack;
    }
    switch( t )
    {
    case BASTION:
        return 3;
    case BALL_MOVE:
        return 1;
    case PURIFY:
        return 2;
    case EVOLVE:
        return 2;
    case TOTAL:
        return 3;
    }
    return 0;
}

uint8_t ChaoHibby::_total_procs( attacks t )
{
    if( TOTAL == t )
    {
        t = _lastAttack;
    }
    switch( t )
    {
    case BASTION:
        return 8;
    case BALL_MOVE:
        return 1;
    case PURIFY:
        return 3;
    case EVOLVE:
        return 2;
    case TOTAL:
        return 1;
    }
    return 1;
}
