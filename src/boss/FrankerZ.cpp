#include "boss/FrankerZ.h"

#include <gert_CameraMath.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <HitCircle.h>
#include <TexHolder.h>
#include <particle.h>
#include <Bullet.h>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define FZ_BASE_HEALTH 110
#define FZ_RADIUS 900
#define FZ_INVICIBLE_TIME ( 0.5 )
#define FZ_MOVEMENT_SPEED 120.0

#define FZ_BOMB_TIMER 1.8
#define FZ_MAX_BOMBS 7
#define FZ_BOMB_RADIUS_MIN 800
#define FZ_BOMB_RADIUS_MAX ( FZ_BOMB_RADIUS_MIN + 500 )
#define FZ_BOMB_SIZE 310

FrankerZ::FrankerZ()
{
    //ctor
    _lastAttack = TOTAL;
    _timeSinceLastAttack = 0;
    _bodyHC = nullptr;
    _iTime = FZ_INVICIBLE_TIME;
    _attackProcs = 0;
    _enemy = true;
    _xMove = 0;
    _yMove = 0;
    _bombPoint = new SDL_Point[ FZ_MAX_BOMBS ];
    for( unsigned int i = 0; i < FZ_MAX_BOMBS; i++ )
    {
        _bombPoint[ i ] = { 0, 0 };
    }
}

FrankerZ::~FrankerZ()
{
    //dtor
    delete[] _bombPoint;
}

void FrankerZ::update( void )
{
    _iTime += gFrameTime;
    _timeSinceLastAttack += gFrameTime;

    float ctTime = _attack_timer( _lastAttack )/_total_attack_procs( _lastAttack );
    switch( _lastAttack )
    {
    case SPLITS:
        if( _timeSinceLastAttack - _attackProcs * ctTime > ctTime )
        {
            Bullet* foo;
            IF_BULLET_GET( foo )
            {
                foo->start( get_syncPoint(), this, 360.0/( _total_attack_procs( _lastAttack ) ) * _attackProcs, 1800, 4 );
                foo->ability = bulletFunction::recursiveShot;
                foo->myTexture = assets::texture::FRANKER_Z;
                foo->totalAbilityCount = 3;
                foo->damage = 6;
            }

            _attackProcs += 1;
        }
        break;
    case SOFT_LOCK:
        if( _timeSinceLastAttack - _attackProcs*ctTime > ctTime )
        {
            Bullet* foo;
            IF_BULLET_GET( foo )
            {
                float sped = 2600;
                if( _healthPoints < FZ_BASE_HEALTH/2 )
                {
                    sped = 3200;
                }
                foo->start( get_syncPoint(), this, angles::get_angle( get_syncPoint(), gGameManager->get_player_point() ), sped, 2 );
                foo->myTexture = assets::texture::FRANKER_Z;
                foo->damage = 7;
            }
            _attackProcs += 1;
        }
        break;
    case CHANGE_ROUTE:
        if( _timeSinceLastAttack > _attack_timer( _lastAttack ) )
        {
            _xMove = rand()%3 - 1;
            _yMove = rand()%3 - 1;
        }
        break;
    case BOMB:
        if( _healthPoints < FZ_BASE_HEALTH/2 )
        {
            if( !_firstProc )
            {
                for( int i = 0; i < FZ_MAX_BOMBS; i++ )
                {
                    float tempAngle = float(i)/FZ_MAX_BOMBS * 360;
                    float xCoord = cos( tempAngle * M_PI/180 ) * ( FZ_BOMB_RADIUS_MIN + rand()%(FZ_BOMB_RADIUS_MAX - FZ_BOMB_RADIUS_MIN) );
                    float yCoord = sin( tempAngle * M_PI/180 ) * ( FZ_BOMB_RADIUS_MIN + rand()%(FZ_BOMB_RADIUS_MAX - FZ_BOMB_RADIUS_MIN) );

                    _bombPoint[ i ] = { int( xCoord + gGameManager->get_player_point().x ), int( yCoord + gGameManager->get_player_point().y ) };
                    Particle* foo;
                    IF_PARTICLE_GET( foo )
                    {
                        foo->start( { _bombPoint[ i ].x - FZ_BOMB_SIZE/2, _bombPoint[ i ].y - FZ_BOMB_SIZE/2, FZ_BOMB_SIZE, FZ_BOMB_SIZE }, FZ_BOMB_TIMER );
                        foo->effect = Particle::FADE_IN;
                        foo->myTexture = assets::texture::OOT_BOMB;
                    }
                }
                _firstProc = true;
            }

            if( _timeSinceLastAttack > _attack_timer( _lastAttack ) )
            {
                for( int i = 0; i < FZ_MAX_BOMBS; i++ )
                {
                    HitCircle* hc;
                    Particle* pexplode;
                    IF_HIT_CIRC( hc )
                    {
                        hc->start( { float( _bombPoint[ i ].x ), float( _bombPoint[ i ].y ), FZ_BOMB_SIZE*1.3 }, 16, _enemy );
                    }
                    IF_PARTICLE_GET( pexplode )
                    {
                        pexplode->start( { _bombPoint[ i ].x - FZ_BOMB_SIZE, _bombPoint[ i ].y - FZ_BOMB_SIZE, FZ_BOMB_SIZE*2, FZ_BOMB_SIZE*2 }, 0.6 );
                        pexplode->myTexture = assets::texture::EXPLOSION_0;
                        pexplode->angle = rand()%360;
                        pexplode->effect = Particle::flags( Particle::FADE_OUT | Particle::BLUE_MOD );
                    }
                }
            }
        }
        break;
    default:
        break;
    }

    if( _timeSinceLastAttack > _attack_timer( _lastAttack ) )
    {
        _timeSinceLastAttack = 0;
        _attackProcs = 0;
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }
        _lastAttack = attacks( buffer );
        _firstProc = false;
    }

    _xBuffer += FZ_MOVEMENT_SPEED * gFrameTime * int(_xMove);
    _yBuffer += FZ_MOVEMENT_SPEED * gFrameTime * int(_yMove);

    if( _bodyHC != nullptr )
    {
        *_bodyHC->area = { _xBuffer, _yBuffer, FZ_RADIUS*0.9 };
    }
}

void FrankerZ::draw( void )
{
    SDL_Color c = color::WHITE;
    if( _iTime < FZ_INVICIBLE_TIME )
    {
        c.g = _iTime/FZ_INVICIBLE_TIME*254;
        c.b = c.g;
    }
    gTextures[ assets::texture::FRANKER_Z ].render( { (int)_xBuffer - FZ_RADIUS, (int)_yBuffer - FZ_RADIUS, FZ_RADIUS*2, FZ_RADIUS*2 }, c );
}

void FrankerZ::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _lastAttack = TOTAL;
    _timeSinceLastAttack = 0;
    _attackProcs = 0;

    if( _bodyHC == nullptr )
    {
        IF_HURT_CIRC( _bodyHC )
        {
            _healthPoints = FZ_BASE_HEALTH;
            _iTime = FZ_INVICIBLE_TIME;
        }
    }
    _xMove = 0;
    _yMove = 0;
}

float FrankerZ::_attack_timer( attacks t )
{
    switch( t )
    {
    case SPLITS:
        return 3;
    case SOFT_LOCK:
        return 2.4;
    case CHANGE_ROUTE:
        return 0.3;
    case BOMB:
        if( _healthPoints < FZ_BASE_HEALTH/2 )
        {
            return FZ_BOMB_TIMER;
        }
        else
        {
            return 1;
        }
    case TOTAL:
        return 6;
    }
    return 0;
}

unsigned char FrankerZ::_total_attack_procs( attacks t )
{
    switch( t )
    {
    case SPLITS:
        return 8;
    case SOFT_LOCK:
        return 24;
    case CHANGE_ROUTE:
        return 1;
    case BOMB:
        return 1;
    case TOTAL:
        return 0;
    }
    return 0;
}

void FrankerZ::get_hurt( double damage, HurtCircle* from )
{
    if( _iTime > FZ_INVICIBLE_TIME )
    {
        _healthPoints -= damage;
        _iTime = 0;
    }
}

void FrankerZ::clean_up( void )
{
    if( _bodyHC != nullptr )
    {
        _bodyHC->target = nullptr;
        _bodyHC = nullptr;
    }
    Actor::start( { 0, 0 } );
}

void FrankerZ::cut_scene_update( void )
{
    if( get_dead() )
    {
        _iTime += gFrameTime;
        if( _iTime > FZ_INVICIBLE_TIME/2 )
        {
            Particle* foo;
            IF_PARTICLE_GET( foo )
            {
                int splodeSize = 300;
                foo->start( { rand()%FZ_RADIUS*2 + (int)_xBuffer - FZ_RADIUS, rand()%FZ_RADIUS*2 + (int)_yBuffer - FZ_RADIUS, splodeSize, splodeSize }, 1 );
                foo->myTexture = assets::texture::EXPLOSION_0;
                foo->angle = rand()%360;
                foo->effect = Particle::FADE_OUT;
            }
            _iTime = 0;
        }
    }
}
