#include "boss/TabesMeat.h"

#include <random>
#include <TexHolder.h>
#include <gert_Wrappers.h>
#include <HurtCircle.h>
#include <PushCircle.h>
#include <particle.h>
#include <GameManager.h>
#include <gert_Collision.h>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define TMM_BASE_HEALTH 3
#define TMM_INVICIBLE_TIME 0.35
#define TMM_START_TIME 1.2
#define TMM_RADIUS 300

TabesMeat::TabesMeat()
{
    //ctor
    _body = nullptr;
    subtex = 0;
    _angle = 0;
    _iTime = 0;
    _starting = false;
}

TabesMeat::~TabesMeat()
{
    //dtor
}

void TabesMeat::update( void )
{
    _iTime += gFrameTime;

    if( _starting && _iTime > TMM_START_TIME )
    {
        HitCircle* foo;
        IF_HIT_CIRC( foo )
        {
            foo->start( get_area(), 7, _enemy );
        }

        Particle* boom;
        IF_PARTICLE_GET( boom )
        {
            boom->start( get_area().ct_Rect(), 0.3 );
            boom->myTexture = assets::texture::EXPLOSION_0;
            boom->effect = Particle::FADE_OUT;
        }
        _starting = false;
    }

    /*updatemove*/
    if( _body != nullptr )
    {
        *_body->area = { _xBuffer, _yBuffer, TMM_RADIUS };
    }
}

void TabesMeat::draw( void )
{
    if( _body != nullptr && !HealthyActor::get_dead() )
    {
        SDL_Color c = color::WHITE;
        if( _iTime < TMM_INVICIBLE_TIME )
        {
            c.g = uint8_t( _iTime/TMM_INVICIBLE_TIME*254 );
        }
        SDL_Rect area = _body->area->ct_Rect();
        if( _starting )
        {
            float multi = 1 + (TMM_START_TIME - _iTime)/TMM_START_TIME;
            area.x *= multi;
            area.y *= multi;
            area.w *= multi;
            area.h *= multi;
        }
        gTextures[ assets::texture::TABE_MONSTER ].render( area, c, subtex, _angle );
    }
}

void TabesMeat::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _starting = true;
    _iTime = TMM_INVICIBLE_TIME;

    IF_HURT_CIRC( _body )
    {
        _healthPoints = TMM_BASE_HEALTH;
    }

    _angle = (rand()%(360*3))/3;
    subtex = rand()%5 + 1;
}

void TabesMeat::get_hurt( double damage, HurtCircle* from )
{
    if( _iTime > TMM_INVICIBLE_TIME && !_starting )
    {
        _healthPoints -= damage;
        _iTime = 0;

        if( _healthPoints <= 0 && _body != nullptr )
        {
            Particle* foo;
            IF_PARTICLE_GET( foo )
            {
                foo->start( get_area().ct_Rect(), 0.3 );
                foo->myTexture = assets::texture::TABE_MONSTER;
                foo->subTex = subtex;
                foo->effect = Particle::FADE_OUT;
                foo->angle = _angle;
            }
            _body->target = nullptr;
            _body = nullptr;
        }
    }
}

Circle TabesMeat::get_area( void )
{
    if( _body != nullptr )
    {
        return *_body->area;
    }
    return { 0, 0, 0 };
}

void TabesMeat::move_to( const SDL_Point& pos, float speed )
{
    if( !_starting )
    {
        float diffX, diffY;
        collision::get_normal_diffXY( pos, get_syncPoint(), &diffX, &diffY );

        _xBuffer += diffX * speed * gFrameTime;
        _yBuffer += diffY * speed * gFrameTime;
    }
}

void TabesMeat::set_hitbox( float dam )
{
    if( !_starting )
    {
        HitCircle* foo;
        IF_HIT_CIRC( foo )
        {
            foo->start( get_area(), dam, _enemy );
        }
    }
}

bool TabesMeat::get_dead( void )
{
    if( HealthyActor::get_dead() )
    {
        return true;
    }
    else if( _starting )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void TabesMeat::set_syncPoint( const SDL_Point& pos )
{
    _xBuffer = pos.x;
    _yBuffer = pos.y;
}

void TabesMeat::force_kill( void )
{
    if( _body != nullptr )
    {
        Particle* foo;
        IF_PARTICLE_GET( foo )
        {
            foo->start( get_area().ct_Rect(), 0.3 );
            foo->myTexture = assets::texture::TABE_MONSTER;
            foo->subTex = subtex;
            foo->effect = Particle::FADE_OUT;
            foo->angle = _angle;
        }
        _body->target = nullptr;
        _body = nullptr;
    }
    _healthPoints = 0;
}
