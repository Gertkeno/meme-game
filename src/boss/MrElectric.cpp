#include "boss/MrElectric.h"

#include <gert_CameraMath.h>
#include <SDL2/SDL_shape.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <TexHolder.h>
#include <HitCircle.h>
#include <Bullet.h>
#include <random>
#include <cmath>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define ME_MAX_APPENDAGES 6
#define ME_INVICIBLE_TIME 0.5
#define ME_BASE_HEALTH 105
#define ME_ATTACK_DELAY -0.666 ///LOL SADIN

#define ME_APPENDAGE_RADIUS ( ME_BODY_RADIUS * 4 )
#define ME_BODY_RADIUS 790
#define ME_HAND_RADIUS 240
#define ME_ELECTRIC_RADIUS 110
#define ME_BOLT_RADIUS 990

#define ME_ELECTRIC_FRAMES 6
#define ME_ELECTRIC_OFFSET 3

MrElectric::MrElectric()
{
    //ctor
    _appendage = new SDL_Point[ ME_MAX_APPENDAGES ];
    _appTo = new SDL_Point[ ME_MAX_APPENDAGES ];
    _bodyHurtC = nullptr;
    _armHurC = new HurtCircle*[ ME_MAX_APPENDAGES ];
    for( int i = 0; i < ME_MAX_APPENDAGES;i ++ )
    {
        _armHurC[ i ] = nullptr;
    }
    _handDelta = new float[ ME_MAX_APPENDAGES * 2 ];
}

MrElectric::~MrElectric()
{
    //dtor
    delete[] _appendage;
    delete[] _handDelta;
}

void MrElectric::update( void )
{
    /*TIMERS*/
    _timeSinceAttack += gFrameTime;
    _iTime += gFrameTime;
    /*attack stuff*/

    bool stopArms = false;
    float ctTime = _attack_timer( _lastAttack )/_total_procs( _lastAttack );
    switch( _lastAttack )
    {
    case ROT_ARMS:
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            for( uint16_t i = 0; i < ME_MAX_APPENDAGES; i++ )
            {
                uint16_t nextArm = i + 1;
                if( nextArm >= ME_MAX_APPENDAGES )
                {
                    nextArm = 0;
                }
                float dist = collision::distance( _appendage[ i ], _appendage[ nextArm ] ) / ( _attack_timer( _lastAttack )/_total_procs( _lastAttack ) );
                float diffX, diffY;
                collision::get_normal_diffXY( _appendage[ i ], _appendage[ nextArm ], &diffX, &diffY );

                _handDelta[ i*2 + 0 ] = -diffX * dist;
                _handDelta[ i*2 + 1 ] = -diffY * dist;
            }
            _attackProcs += 1;
        }
        if( _timeSinceAttack > _attack_timer( _lastAttack ) )
        {
            stopArms = true;
        }
        break;
    case RESET_ARMS:
        if( !_firstProc  && _timeSinceAttack > 0 )
        {
            for( unsigned int i = 0; i < ME_MAX_APPENDAGES; i++ )
            {
                float tangle = float(i)/ME_MAX_APPENDAGES*360;
                SDL_Point desireP = { int( cos( DT_PI( tangle ) ) * ME_APPENDAGE_RADIUS + _xBuffer ), int( sin( DT_PI( tangle ) ) * ME_APPENDAGE_RADIUS + _yBuffer ) };
                _appTo[ i ] = desireP;

                float dist = collision::distance( _appendage[ i ], desireP ) / _attack_timer( _lastAttack );

                float diffX, diffY;
                collision::get_normal_diffXY( _appendage[ i ], desireP, &diffX, &diffY );

                _handDelta[ i*2 + 0 ] = -diffX * dist;
                _handDelta[ i*2 + 1 ] = -diffY * dist;
            }
            _firstProc = true;
        }
        if( _timeSinceAttack > ctTime )
        {
            stopArms = true;
        }
        break;
    case GRAB_PLAYER:
        if( !_firstProc && _timeSinceAttack > 0 )
        {
            SDL_Point bigP = gGameManager->get_player_point();
            int closeIndex[ ME_MAX_APPENDAGES ];
            float dist[ ME_MAX_APPENDAGES ];
            for( unsigned int i = 0; i < ME_MAX_APPENDAGES; i++ )
            {
                _appTo[ i ] = _appendage[ i ];
                dist[ i ] = collision::distance( bigP, _appendage[ i ] );
                closeIndex[ i ] = i;
            }
            /*FUCKING SORT ALGORITHM probably not needed*/
            for( unsigned int a = 0; a < ME_MAX_APPENDAGES; a++ )
            {
                unsigned int amin = a;
                for( unsigned int b = a+1; b < ME_MAX_APPENDAGES; b++ )
                {
                    if( dist[ closeIndex[ b ] ] < dist[ closeIndex[ amin ] ] )
                    {
                        amin = b;
                    }
                }
                if( amin != a )
                {
                    int buffer = closeIndex[ amin ];
                    closeIndex[ amin ] = closeIndex[ a ];
                    closeIndex[ a ] = buffer;
                }
            }

            for( unsigned int i = 0; i < 2; i++ )
            {
                _appTo[ closeIndex[ i ] ] = bigP;
                float diffX, diffY;
                collision::get_normal_diffXY( _appendage[ closeIndex[ i ] ], bigP, &diffX, &diffY );
                float dist = collision::distance( bigP, _appendage[ closeIndex[ i ] ] ) / _attack_timer( _lastAttack );

                _handDelta[ closeIndex[ i ]*2 + 0 ] = -diffX * dist;
                _handDelta[ closeIndex[ i ]*2 + 1 ] = -diffY * dist;
                _firstProc = true;
            }
        }
        else if( _timeSinceAttack > ctTime )
        {
            stopArms = true;
        }
        break;
    case MOVE_TO:
        if( !_firstProc && _timeSinceAttack > 0 )
        {
            int select = rand()%ME_MAX_APPENDAGES;
            float diffX, diffY;
            collision::get_normal_diffXY( _appendage[ select ], get_syncPoint(), &diffX, &diffY );
            float dist = collision::distance( _appendage[ select ], get_syncPoint() );

            _xDelta = diffX * dist / _attack_timer( _lastAttack );
            _yDelta = diffY * dist / _attack_timer( _lastAttack );

            _firstProc = true;
        }
        else if( _timeSinceAttack > ctTime )
        {
            _xDelta = 0;
            _yDelta = 0;
        }
        break;
    case DIRECT_SHOT:
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            if( _attackProcs > 0 )
            {
                for( uint16_t i = 0; i < ME_MAX_APPENDAGES; i++ )
                {
                    Bullet* foo;
                    IF_BULLET_GET( foo )
                    {
                        foo->start( _appendage[ i ], this, angles::get_angle( _appendage[ i ], gGameManager->get_player_point() ), 2345, 1.2 );
                        foo->damage = 5;
                    }
                }
            }
            _attackProcs += 1;
        }
        break;
    case TOTAL:
        break;
    }

    if( stopArms )
    {
        for( unsigned int i = 0; i <  ME_MAX_APPENDAGES; i++ )
        {
            _handDelta[ i*2 + 0 ] = 0;
            _handDelta[ i*2 + 1 ] = 0;
        }
    }

    /*MOVEMENT*/
    _xBuffer += _xDelta * gFrameTime;
    _yBuffer += _yDelta * gFrameTime;

    for( unsigned int i = 0; i < ME_MAX_APPENDAGES; i++ )
    {
        _appendage[ i ].x += _handDelta[ i*2 + 0 ] * gFrameTime;
        _appendage[ i ].y += _handDelta[ i*2 + 1 ] * gFrameTime;

    }

    /*ELECTRICS HITBOXES*/
    for( uint16_t i = 0; i < ME_MAX_APPENDAGES; i++ )
    {
        HitCircle* foo;
        IF_HIT_CIRC( foo )
        {
            foo->start( { float( _appendage[ i ].x ), float( _appendage[ i ].y ), ME_HAND_RADIUS }, 7, _enemy );
        }
    }

    /*NEW ATTACKS*/
    if( _timeSinceAttack > _attack_timer( _lastAttack ) )
    {
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }

        _lastAttack = attacks( buffer );
        _attackProcs = 0;
        _timeSinceAttack = ME_ATTACK_DELAY;
        _firstProc = false;
        for( uint8_t i = 0; i < ME_MAX_APPENDAGES; i++ )
        {
            _appTo[ i ] = _appendage[ i ];
        }
    }

    if( _bodyHurtC != nullptr )
    {
        *_bodyHurtC->area = { _xBuffer, _yBuffer, ME_BODY_RADIUS };
        for( uint16_t i = 0; i < ME_MAX_APPENDAGES; i++ )
        {
            if( _armHurC[ i ] != nullptr )
            {
                *_armHurC[ i ]->area = { (float)_appendage[ i ].x, (float)_appendage[ i ].y, ME_HAND_RADIUS };
            }
        }
    }
}

void MrElectric::draw( void )
{
    /*HANDS FIRST*/
    if( ( _lastAttack == GRAB_PLAYER || _lastAttack == RESET_ARMS ) && _timeSinceAttack > 0 )
    {
        for( uint8_t i = 0; i < ME_MAX_APPENDAGES; i++ )
        {
            SDL_Color c = color::CYAN;// { 102, 0, 204, 255 };
            c.a = (_attack_timer( _lastAttack ) - _timeSinceAttack)/_attack_timer( _lastAttack )*254;

            SDL_Rect splace = { _appendage[ i ].x, _appendage[ i ].y - ME_HAND_RADIUS, int(collision::distance( _appendage[ i ], _appTo[ i ] )), ME_HAND_RADIUS*2 };
            SDL_Point rotP = { 0, splace.h/2 };
            gTextures[ assets::texture::TRACK ].render( splace, c, 0, angles::get_angle( _appendage[ i ], _appTo[ i ] ), true, SDL_FLIP_NONE, &rotP );
        }
    }
    for( unsigned int i = 0; i < ME_MAX_APPENDAGES; i++ )
    {
        SDL_Color c = color::WHITE;
        if( _iTime < ME_INVICIBLE_TIME )
        {
            c.g = _iTime/ME_INVICIBLE_TIME*254;
            c.b = c.g;
        }
        int elecFrame = _timeSinceAttack/_attack_timer( _lastAttack ) * ME_ELECTRIC_FRAMES + ME_ELECTRIC_OFFSET;
        if( _timeSinceAttack < 0 )
        {
            elecFrame = 3;
        }
        elecFrame += i;
        if( elecFrame >= ME_ELECTRIC_FRAMES + ME_ELECTRIC_OFFSET )
        {
            elecFrame -= ME_ELECTRIC_FRAMES;
        }

        SDL_Rect electString = { int( _xBuffer ), int( _yBuffer - ME_ELECTRIC_RADIUS ), int( collision::distance( get_syncPoint(), _appendage[ i ] ) ), ME_ELECTRIC_RADIUS*2 };
        SDL_Point p = { 0, electString.h/2 };
        float electAngle = angles::get_angle( get_syncPoint(), _appendage[ i ] );
        gTextures[ assets::texture::MR_ELECTRIC ].render( electString, color::WHITE, elecFrame, electAngle, true, SDL_FLIP_NONE, &p );

        SDL_Rect handy = { _appendage[ i ].x - ME_HAND_RADIUS, _appendage[ i ].y - ME_HAND_RADIUS, ME_HAND_RADIUS*2, ME_HAND_RADIUS*2 };
        int frame = 1;
        if( i%2 == 0 )
        {
            frame = 2;
        }
        gTextures[ assets::texture::MR_ELECTRIC ].render( handy, c, frame, electAngle );
    }

    if( _lastAttack == DIRECT_SHOT )
    {
        SDL_Color c = color::WHITE;
        if( _attackProcs == 0 )
        {
            c.a = 100;
        }
        for( unsigned int i = 0; i < ME_MAX_APPENDAGES; i++ )
        {
            float fangle = angles::get_angle( _appendage[ i ], gGameManager->get_player_point() );
            SDL_Rect electString = { _appendage[ i ].x, _appendage[ i ].y - ME_ELECTRIC_RADIUS, ME_BOLT_RADIUS, ME_ELECTRIC_RADIUS*2 };
            SDL_Point p = { 0, electString.h/2 };
            int frame = _timeSinceAttack/_attack_timer( _lastAttack ) * ME_ELECTRIC_FRAMES + ME_ELECTRIC_OFFSET;
            if( _timeSinceAttack < 0 )
            {
                frame = ME_ELECTRIC_OFFSET;
            }

            gTextures[ assets::texture::MR_ELECTRIC ].render( electString, c, frame, fangle, true, SDL_FLIP_NONE, &p );
        }
    }

    /*BODY/HEAD THING*/
    SDL_Color bc = color::WHITE;
    if( _iTime < ME_INVICIBLE_TIME )
    {
        bc = { 255, uint8_t( _iTime/ME_INVICIBLE_TIME*254 ), uint8_t( _iTime/ME_INVICIBLE_TIME*254 ), 255 };
    }
    SDL_Rect bodaciousBod = { int( _xBuffer - ME_BODY_RADIUS ), int( _yBuffer - ME_BODY_RADIUS ), ME_BODY_RADIUS*2, ME_BODY_RADIUS*2 };
    gTextures[ assets::texture::MR_ELECTRIC ].render( bodaciousBod, bc, 0, 0 );

    /*DEAD CUT SCENE*/
    if( get_dead() )
    {
        SDL_Rect elecSTring = { (int)_xBuffer, (int)_yBuffer, int( ME_BODY_RADIUS*1.6 ), ME_ELECTRIC_RADIUS };
        SDL_Point p = { 0, elecSTring.h/2 };
        gTextures[ assets::texture::MR_ELECTRIC ].render( elecSTring, color::WHITE, rand()%ME_ELECTRIC_FRAMES + ME_ELECTRIC_OFFSET, rand()%360, true, SDL_FLIP_NONE, &p );
    }
}

void MrElectric::start( const SDL_Point& pos )
{
    Actor::start( pos );
    for( unsigned int i = 0; i < ME_MAX_APPENDAGES; i++ )
    {
        _appendage[ i ] = pos;
    }
    for( unsigned int i = 0; i < ME_MAX_APPENDAGES*2; i++ )
    {
        _handDelta[ i ] = 0;
    }

    _xDelta = 0;
    _yDelta = 0;

    _firstProc = false;
    _lastAttack = RESET_ARMS;
    _attackProcs = 0;
    _timeSinceAttack = 0;
    _iTime = ME_INVICIBLE_TIME;

    IF_HURT_CIRC( _bodyHurtC )
    {
        _healthPoints = ME_BASE_HEALTH;
    }
    for( uint16_t i = 0; i < ME_MAX_APPENDAGES; i++ )
    {
        IF_HURT_CIRC( _armHurC[ i ] )
        {

        }
    }
}

void MrElectric::get_hurt( double damage, HurtCircle* from )
{
    if( _iTime > ME_INVICIBLE_TIME )
    {
        _iTime = 0;
        if( from == _bodyHurtC )
        {
            _healthPoints -= damage;
        }
        else
        {
            _iTime = ME_INVICIBLE_TIME/2;
            _healthPoints -= damage*0.4;
        }
    }
}

void MrElectric::cut_scene_update( void )
{

}

float MrElectric::_attack_timer( attacks t )
{
    switch( t )
    {
    case RESET_ARMS:
        return 2;
    case GRAB_PLAYER:
        return 1.2;
    case MOVE_TO:
        return 2.6;
    case DIRECT_SHOT:
        return 1;
    case ROT_ARMS:
        return 3.8;
    case TOTAL:
        return 6;
    }
    return 0;
}

uint8_t MrElectric::_total_procs( attacks t )
{
    switch( t )
    {
    case RESET_ARMS:
        return 1;
    case GRAB_PLAYER:
        return 1;
    case MOVE_TO:
        return 1;
    case DIRECT_SHOT:
        return 9;
    case ROT_ARMS:
        return ME_MAX_APPENDAGES;
    case TOTAL:
        return 1;
    }
    return 1;
}

void MrElectric::clean_up( void )
{
    if( _bodyHurtC != nullptr )
    {
        _bodyHurtC->target = nullptr;
        _bodyHurtC = nullptr;
    }
    for( uint16_t i = 0; i < ME_MAX_APPENDAGES; i++ )
    {
        if( _armHurC[ i ] != nullptr )
        {
            _armHurC[ i ]->target = nullptr;
            _armHurC[ i ] = nullptr;
        }
    }

    /*_xBuffer = 0;
    _yBuffer = 0;
    for( uint16_t i = 0; i < ME_MAX_APPENDAGES; i++ )
    {
        _appendage[ i ] = { 0, 0 };
    }*/
}

void MrElectric::push_me( float xd, float yd, HurtCircle* from )
{
    if( from == _bodyHurtC )
    {
        HealthyActor::push_me( xd, yd, from );
    }
}
