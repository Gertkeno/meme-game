#include "BaconBot.h"

#include <gert_SoundWrapper.h>
#include <gert_CameraMath.h>
#include <SDL2/SDL_shape.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <TexHolder.h>
#include <particle.h>
#include <random>

#include <HurtCircle.h>
#include <PushCircle.h>
#include <Bullet.h>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define MBB_BASE_HEALTH 55
#define MBB_INVICIBLE_TIME 0.7
#define MBB_HEIGHT ( 0.5607094134 * MBB_SCALE )
#define MBB_WIDTH ( ( 1 - MBB_HEIGHT/MBB_SCALE ) * MBB_SCALE )
#define MBB_PARTICLEH ( 0.07113543091655267 * MBB_SCALE )
#define MBB_SCALE 1100

#define MBB_ATTACK_DELAY -0.33

#define MBB_ORBIT_DIST 1800
#define MBB_ORBIT_PREP 0.35

#define MBB_LAZOR_RADIUS 400

#define MBB_PULSE_DIST (2800 * 2)

BaconBot::BaconBot()
{
    //ctor
    _timeSinceAttack = 0;
    _lastAttack = TOTAL;

    _pointTarget = new SDL_Point;
}

BaconBot::~BaconBot()
{
    //dtor
    delete _pointTarget;
}

void BaconBot::update( void )
{
    _timeSinceAttack += gFrameTime;
    _iTime += gFrameTime;

    float ctTime = _attack_timer()/_total_procs();
    switch( _lastAttack )
    {
    case SHOOTY:
        if( _timeSinceAttack > 0 )
        {
            *_pointTarget = gGameManager->get_player_point();
        }
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            uint8_t tb = 6;
            for( uint8_t i = 0; i < tb; i++ )
            {
                Bullet* foo;
                IF_BULLET_GET( foo )
                {
                    float bangle = angles::get_angle( get_syncPoint(), gGameManager->get_player_point() );
                    float angleadd = -50 + i*100/tb;
                    foo->start( get_syncPoint(), this, bangle+angleadd, 2000, 2.6 );
                    foo->damage = 6;
                }
            }
            _attackProcs += 1;
        }
        break;
    case PULSE:
        if( _timeSinceAttack > 0 )
        {
            float tRadius = 0;
            if( _attackProcs == 0 )
            {
                tRadius = _timeSinceAttack/(_attack_timer())*MBB_PULSE_DIST;
            }
            else
            {
                tRadius = (_attack_timer() - _timeSinceAttack)/(_attack_timer())*MBB_PULSE_DIST;
            }
            HitCircle* foo;
            IF_HIT_CIRC( foo )
            {
                foo->start( { _xBuffer, _yBuffer, tRadius }, 8, _enemy );
            }
        }
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            _attackProcs += 1;
        }
        break;
    case BIG_LAZOR:
        if( _attackProcs == 0 )
        {
            *_pointTarget = gGameManager->get_player_point();
        }
        else if( _attackProcs >= 2 )
        {
            float xNorm, yNorm;
            collision::get_normal_diffXY( *_pointTarget, get_syncPoint(), &xNorm, &yNorm );

            float needed = collision::distance( get_syncPoint(), gGameManager->get_player_point() );
            //for( uint8_t i = 0; i < needed; i++ )
            {
                HitCircle* killa;
                IF_HIT_CIRC( killa )
                {
                    Circle ara = { xNorm * needed + _xBuffer, yNorm * needed + _yBuffer, MBB_LAZOR_RADIUS*0.8333 };
                    killa->start( ara, 11, _enemy );
                }
            }
        }
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            _attackProcs += 1;
        }
        break;
    case SPIN_CHAMBER:
        if( _timeSinceAttack > 0 )
        {
            if( !_firstProc )
            {
                gSound[ assets::sfx::TELEPORT ].play();
                *_pointTarget = gGameManager->get_player_point();
                _weirdAngle = rand()%360;
                _firstProc = true;
                _xBuffer = cos( DT_PI( _weirdAngle ) ) * MBB_ORBIT_DIST + _pointTarget->x;
                _yBuffer = sin( DT_PI( _weirdAngle ) ) * MBB_ORBIT_DIST + _pointTarget->y;
            }

            if( _timeSinceAttack > MBB_ORBIT_PREP )
            {
                float percent = (_timeSinceAttack - MBB_ORBIT_PREP)/(_attack_timer()-MBB_ORBIT_PREP) * 360*2 + _weirdAngle;
                if( percent > 360 )
                {
                    percent -= 360;
                }
                _xBuffer = _pointTarget->x + cos( DT_PI( percent ) ) * MBB_ORBIT_DIST;
                _yBuffer = _pointTarget->y + sin( DT_PI( percent ) ) * MBB_ORBIT_DIST;

                /*PUSHING AND KILLING*/
                PushCircle* foo;
                IF_PUSH_CIRC( foo )
                {
                    Circle area = { _xBuffer, _yBuffer, MBB_HEIGHT };
                    foo->start( area, angles::get_angle( get_syncPoint(), *_pointTarget ), 1200, _enemy, 7 );
                }
            }
            else
            {

            }
        }
        if( _timeSinceAttack - _attackProcs*ctTime > ctTime )
        {
            /*do bullets*/
            Bullet* foo;
            IF_BULLET_GET( foo )
            {
                foo->start( get_syncPoint(), this, angles::get_angle( get_syncPoint(), gGameManager->get_player_point() ), 2500, 1.1 );
                foo->radius = 80;
                foo->damage = 6;
            }
            _attackProcs += 1;
        }
        break;
    case TOTAL:
        break;
    }

        /*PARTICLE EFFECTS WEO WEO*/
    {
        SDL_Rect splace = { int(_xBuffer - MBB_WIDTH), int(_yBuffer + MBB_HEIGHT), 30, 30 };
        splace.x += rand()%int(MBB_WIDTH*2);
        splace.y -= rand()%int(MBB_PARTICLEH*2);

        Particle* foo;
        IF_PARTICLE_GET( foo )
        {
            foo->start( splace, 0.7 );
            foo->effect = Particle::flags( Particle::FADE_OUT | Particle::SPIN );
            foo->myTexture = assets::texture::MAKIN_BACON_BOT;
            foo->subTex = 4;
        }
    }

        /*NEW ATTACK*/
    if( _timeSinceAttack > _attack_timer() )
    {
        int buffer = rand()%TOTAL;
        if( buffer == _lastAttack )
        {
            buffer += 1;
            buffer %= TOTAL;
        }
        _lastAttack = attacks( buffer );

        _timeSinceAttack = MBB_ATTACK_DELAY;
        _attackProcs = 0;
        _firstProc = false;
    }

        /*PAIN*/
    if( _body != nullptr )
    {
        *_body->area = { _xBuffer, _yBuffer, 400 };
    }
}

void BaconBot::draw( void )
{
        /*LASER DRAWING*/
    if( BIG_LAZOR == _lastAttack && _timeSinceAttack > 0 )
    {
        SDL_Color c = color::WHITE;
        SDL_Rect dspace = { (int)_xBuffer, int(_yBuffer - MBB_LAZOR_RADIUS/2), 9999, int(MBB_LAZOR_RADIUS*2) };
        if( _attackProcs <= 1 )
        {
            dspace.h /= 4;
            c.a = 140;
        }
        else
        {
            if( int(_timeSinceAttack*100)%20 > 10 )
            {
                c.a = 140;
            }
        }
        dspace.y = _yBuffer - dspace.h/2;
        SDL_Point rotm = { 0, dspace.h/2 };
        gTextures[ assets::texture::MAKIN_BACON_BOT ].render( dspace, c, 3, angles::get_angle( get_syncPoint(), *_pointTarget ), true, SDL_FLIP_NONE, &rotm );
    }

    SDL_Color c = color::WHITE;
    if( _iTime < MBB_INVICIBLE_TIME )
    {
        c.g = _iTime/MBB_INVICIBLE_TIME*254;
        c.b = c.g;
    }
    SDL_Rect splace = { int(_xBuffer - MBB_WIDTH), int(_yBuffer - MBB_HEIGHT), int(MBB_WIDTH*2), int(MBB_HEIGHT*2) };

        /*SPINNING BUFFER*/
    if( SPIN_CHAMBER == _lastAttack && _timeSinceAttack > 0 && _timeSinceAttack < MBB_ORBIT_PREP )
    {
        c.a = _timeSinceAttack/MBB_ORBIT_PREP*254;
    }

    gTextures[ assets::texture::MAKIN_BACON_BOT ].render( splace, c, 1 );

        /*PULSE EXPANDY*/
    if( PULSE == _lastAttack && _timeSinceAttack > 0 )
    {
        float radi = 0;
        if( _attackProcs == 0 )
        {
            radi = _timeSinceAttack/(_attack_timer())*MBB_PULSE_DIST;
        }
        else
        {
            radi = (_attack_timer() - _timeSinceAttack)/(_attack_timer())*MBB_PULSE_DIST;
        }
        radi *= 1.12;
        Circle area = { _xBuffer, _yBuffer, radi };
        gTextures[ assets::texture::MAKIN_BACON_BOT ].render( area.ct_Rect(), color::WHITE, 2 );
    }
}

void BaconBot::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _iTime = MBB_INVICIBLE_TIME;
    _timeSinceAttack = 0;
    _attackProcs = 0;
    _lastAttack = TOTAL;
    _firstProc = 0;

    IF_HURT_CIRC( _body )
    {
        _healthPoints = MBB_BASE_HEALTH;
    }
}

void BaconBot::get_hurt( double damage, HurtCircle* from )
{
    if( _iTime > MBB_INVICIBLE_TIME )
    {
        if( from == _body )
        {
            _healthPoints -= damage;
            _iTime = 0;
        }
    }
}

void BaconBot::clean_up( void )
{
    if( _body != nullptr )
    {
        _body->target = nullptr;
        _body = nullptr;
    }
}

void BaconBot::cut_scene_update( void )
{
    if( !get_dead() )
    {
        SDL_Rect splace = { int(_xBuffer - MBB_WIDTH), int(_yBuffer + MBB_HEIGHT), 30, 30 };
        splace.x += rand()%int(MBB_WIDTH*2);
        splace.y -= rand()%int(MBB_PARTICLEH*2);

        Particle* foo;
        IF_PARTICLE_GET( foo )
        {
            foo->start( splace, 0.7 );
            foo->effect = Particle::flags( Particle::FADE_OUT | Particle::SPIN );
            foo->myTexture = assets::texture::MAKIN_BACON_BOT;
            foo->subTex = 4;
        }
    }
    else
    {
        _iTime += gFrameTime;
        if( _iTime > MBB_INVICIBLE_TIME )
        {
            Particle* foo;
            IF_PARTICLE_GET( foo )
            {
                foo->start( { int( _xBuffer - MBB_WIDTH ), int( _yBuffer - MBB_HEIGHT ), int(MBB_WIDTH*2), int(MBB_HEIGHT*2) }, MBB_INVICIBLE_TIME );
                foo->myTexture = assets::texture::MAKIN_BACON_BOT;
                foo->subTex = 2;
                foo->effect = Particle::flags( Particle::SPIN | Particle::FADE_IN | Particle::FADE_OUT );
            }
            _iTime = 0;
        }
    }
}

float BaconBot::_attack_timer( attacks t )
{
    if( t == TOTAL )
        t = _lastAttack;

    switch( t )
    {
    case SHOOTY:
        return 5.5;
    case PULSE:
        return 5.8;
    case BIG_LAZOR:
        return 1.69;
    case SPIN_CHAMBER:
        return 5;
    case TOTAL:
        return 2.6;
    }
    return 0;
}

uint8_t BaconBot::_total_procs( attacks t )
{
    if( t == TOTAL )
        t = _lastAttack;
    switch( t )
    {
    case SHOOTY:
        return 6;
    case PULSE:
        return 2;
    case BIG_LAZOR:
        return 3;
    case SPIN_CHAMBER:
        return 8;
    case TOTAL:
        return 1;
    }
    return 1;
}
