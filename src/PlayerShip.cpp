#include "PlayerShip.h"
#include <SDL2/SDL_shape.h>

#include <gert_SoundWrapper.h>
#include <gert_CameraMath.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <HurtCircle.h>
#include <TexHolder.h>
#include <Bullet.h>
#include <cmath>

#include <particle.h>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define TURN_RATE ( 280 - ( 100 * _inputs[ ACCELERATE ] ) )
#define MAX_HEALTH_POINTS 100
#define SHIP_MOVEMENT_SPEED 900
#define INVICIBLE_TIME 1.2
#define SHIP_RADIUS 70
#define SHOT_TIME ( 0.4 )

/*SPECIAL CONSTANTS*/

float* const PlayerShip::powerMaxTime = new float[ PlayerShip::NONE ] { 2, 5.5, 10, SHOT_TIME*3, 9, 2, 5 };
/// BRAKE AUTO_RETICLE SHEILD BACK_SHOT MASSIVE_SHOT TELEPORT HEAL

PlayerShip::PlayerShip()
{
    //ctor
    _curretPower = powerup::BRAKE;
    for( int i = 0; i < TOTAL; i++ )
    {
        _inputs[ i ] = false;
    }
    _lastKey = TOTAL;

    /*In game stuff*/
    _iTime = 0;
    _angle = 0;
    _myHurtCirc = nullptr;
    _xBuffer = 0;
    _yBuffer = 0;
    _xDelta = 0;
    _yDelta = 0;

    _active = false;
    _lastPowerUsed = 0;
    _lastShot = 0;
    _enemy = false;

    _xJoyStick = 0;
    _yJoyStick = 0;
}

PlayerShip::~PlayerShip()
{
    //dtor
}

void PlayerShip::manage_event( const SDL_Event& event )
{
    switch( event.type )
    {
    case SDL_JOYAXISMOTION:
        {
            if( event.jaxis.axis == SDL_CONTROLLER_AXIS_LEFTX )
            {
                _xJoyStick = event.jaxis.value;
            }
            else if( event.jaxis.axis == SDL_CONTROLLER_AXIS_LEFTY )
            {
                _yJoyStick = event.jaxis.value;
            }
        }
        break;
    case SDL_JOYBUTTONDOWN:
        if( event.jbutton.button == JOY_CTR( gpcACCELERATE ) )
        {
            _inputs[ ACCELERATE ] = true;
        }
        else if( event.jbutton.button == JOY_CTR( gpcSHOOT ) )
        {
            _inputs[ SHOOT ] = true;
        }
        else if( event.jbutton.button == JOY_CTR( gpcSPECIAL ) )
        {
            _inputs[ SPECIAL ] = true;
        }
        break;
    case SDL_JOYBUTTONUP:
        if( event.jbutton.button == JOY_CTR( gpcACCELERATE ) )
        {
            _inputs[ ACCELERATE ] = false;
        }
        else if( event.jbutton.button == JOY_CTR( gpcSHOOT ) )
        {
            _inputs[ SHOOT ] = false;
        }
        else if( event.jbutton.button == JOY_CTR( gpcSPECIAL ) )
        {
            _inputs[ SPECIAL ] = false;
        }
        break;
    case SDL_KEYDOWN:
        switch( event.key.keysym.sym )
        {
        case SDLK_LEFT:
            _inputs[ TURN_RIGHT ] = true;
            _lastKey = TURN_RIGHT;
            break;
        case SDLK_RIGHT:
            _inputs[ TURN_LEFT ] = true;
            _lastKey = TURN_LEFT;
            break;
        case SDLK_UP:
            _inputs[ ACCELERATE ] = true;
            _lastKey = ACCELERATE;
            break;
        case SDLK_z:
            _inputs[ SHOOT ] = true;
            _lastKey = SHOOT;
            break;
        case SDLK_x:
            _inputs[ SPECIAL ] = true;
            _lastKey = SPECIAL;
            break;
        }
        break;//SDL_KEYDOWN
    case SDL_KEYUP:
        switch( event.key.keysym.sym )
        {
        case SDLK_LEFT:
            _inputs[ TURN_RIGHT ] = false;
            break;
        case SDLK_RIGHT:
            _inputs[ TURN_LEFT ] = false;
            break;
        case SDLK_UP:
            _inputs[ ACCELERATE ] = false;
            break;
        case SDLK_z:
            _inputs[ SHOOT ] = false;
            break;
        case SDLK_x:
            _inputs[ SPECIAL ] = false;
            break;
        }
        break;//SDL_KEYUP
    }
}

void PlayerShip::update( void )
{
    _pushedFrame = false;
    if( _active )
    {
        _lastPowerUsed += gFrameTime;
        if( _lastPowerUsed > powerMaxTime[ _curretPower ] )
        {
            _lastPowerUsed = powerMaxTime[ _curretPower ];
        }

        _lastShot += gFrameTime;
        _iTime += gFrameTime;

        ///TURNING
        if( _inputs[ TURN_LEFT ] && _lastKey != TURN_RIGHT )
        {
            _angle += TURN_RATE * gFrameTime;
        }
        else if( _inputs[ TURN_RIGHT ] && _lastKey != TURN_LEFT )
        {
            _angle -= TURN_RATE * gFrameTime;
        }

        if( _xJoyStick > JOY_DEADZONE || _xJoyStick < -JOY_DEADZONE || _yJoyStick > JOY_DEADZONE || _yJoyStick < -JOY_DEADZONE )
        {
            float _testAngle = atan2( double( _yJoyStick ), double( _xJoyStick ) ) * 180 / M_PI;
            if( _testAngle < 0 )
                _testAngle += 360;

            _testAngle = _testAngle - _angle;
            if( std::abs( _testAngle ) > 180 )
            {
                _testAngle *= -1;
            }

            double realTurn = TURN_RATE * gFrameTime;
            if( _testAngle > realTurn )
            {
                _angle += realTurn;
            }
            else if( _testAngle < -realTurn )
            {
                _angle -= realTurn;
            }
            else
            {
                _angle = _testAngle + _angle;
            }
        }

        if( _angle < 0 )
        {
            _angle = 360 + _angle;
        }
        else if( _angle > 360 )
        {
            _angle = _angle - 360;
        }

        ///FRICTION
        _xDelta *= 0.991;
        _yDelta *= 0.991;

        ///ALTER DELTA POSITION
        if( _inputs[ ACCELERATE ] )
        {
            _xDelta = cos( DT_PI( _angle ) ) * SHIP_MOVEMENT_SPEED;
            _yDelta = sin( DT_PI( _angle ) ) * SHIP_MOVEMENT_SPEED;

            /*trail effect*/
            Particle* foo;
            IF_PARTICLE_GET( foo )
            {
                SDL_Rect splace = { int(_xBuffer - SHIP_RADIUS), int(_yBuffer - SHIP_RADIUS), 20, 20 };
                splace.x += rand()%(SHIP_RADIUS*2);
                splace.y += rand()%(SHIP_RADIUS*2);
                foo->start( splace, 0.3 );
                foo->myTexture = assets::texture::EMBER;
                foo->effect = Particle::flags( Particle::FADE_OUT | Particle::SPIN );
            }
        }
        else
        {
            if( _curretPower == TELEPORT )
            {
                if( _lastPowerUsed > gFrameTime*2 )
                {
                    _lastPowerUsed -= gFrameTime*1.5;
                }
            }
        }

        ///SPECIAL
        if( _inputs[ SPECIAL ] && _lastPowerUsed >= powerMaxTime[ _curretPower ])
        {
            switch( _curretPower )
            {
            case powerup::MASSIVE_SHOT:
                {
                    Bullet* biggin;
                    IF_BULLET_GET( biggin )
                    {
                        biggin->start( get_syncPoint(), this, _angle, 2000, 1.5 );
                        biggin->ability = bulletFunction::layDoTs;
                        biggin->damage = 1.5;
                        biggin->totalAbilityCount = 5;
                        biggin->radius = 200;
                        _lastPowerUsed = 0;
                    }
                }
                break;
            case powerup::TELEPORT:
                {
                    Particle* foo;
                    IF_PARTICLE_GET( foo )
                    {
                        SDL_Rect splace = { int( _xBuffer - SHIP_RADIUS ), int( _yBuffer - SHIP_RADIUS ), SHIP_RADIUS*2, SHIP_RADIUS*2 };
                        foo->start( splace, 0.5 );
                        foo->myTexture = assets::texture::MAKIN_BACON_BOT;
                        foo->subTex = 2;
                        foo->effect = Particle::flags( Particle::SPIN | Particle::FADE_OUT | Particle::BLUE_MOD );
                    }
                }
                gSound[ assets::sfx::TELEPORT ].play();
                _xBuffer += cos( DT_PI( _angle ) ) * SHIP_MOVEMENT_SPEED;
                _yBuffer += sin( DT_PI( _angle ) ) * SHIP_MOVEMENT_SPEED;
                _lastPowerUsed = 0;
                break;
            case powerup::BACK_SHOT:
                {
                    Bullet* foo;
                    IF_BULLET_GET( foo )
                    {
                        float cangle = _angle - 180;
                        if( cangle < 0 ) cangle += 360;
                        foo->start( get_syncPoint(), this, cangle, 2200, 2 );
                        foo->damage = 1;
                    }
                    _lastPowerUsed = 0;
                }
                break;
            case powerup::AUTO_RETICLE:
                {
                    _angle = angles::get_angle( get_syncPoint(), gGameManager->get_enemy_point() );
                    Particle* foo;
                    IF_PARTICLE_GET( foo )
                    {
                        int r = 80;
                        SDL_Rect splace = { gGameManager->get_enemy_point().x - r, gGameManager->get_enemy_point().y - r, r*2, r*2 };
                        foo->start( splace, 0.6 );
                        foo->angle = 45;
                        foo->myTexture = assets::texture::DOWN_UNDER;
                        foo->subTex = 2;
                        foo->effect = Particle::flags( Particle::FADE_OUT | Particle::SPIN );
                    }
                    _lastPowerUsed = 0;
                }
                break;
            case powerup::BRAKE:
                _lastPowerUsed = 0;
                _xDelta = 0;
                _yDelta = 0;
                break;
            case powerup::SHIELD:
                break;
            case powerup::HEAL:
                break;
            case powerup::NONE:
                break;
            }
        }

        if( _curretPower == HEAL && _lastPowerUsed >= powerMaxTime[ _curretPower ] && _healthPoints <= MAX_HEALTH_POINTS - 1 )
        {
            _healthPoints += 1;
            _lastPowerUsed = 0;
            Particle* hpFluff;
            IF_PARTICLE_GET( hpFluff )
            {
                SDL_Rect splace = { int( _xBuffer - SHIP_RADIUS ), int( _yBuffer - SHIP_RADIUS ), SHIP_RADIUS*2, SHIP_RADIUS*2 };
                hpFluff->start( splace, 1 );
                hpFluff->myTexture = assets::texture::POWER_UPS;
                hpFluff->subTex = 3;
                hpFluff->effect = Particle::flags( Particle::RISING | Particle::SHRINK );
            }
        }

        ///SHOOTING
        if( _inputs[ SHOOT ] )
        {
            if( _lastShot > SHOT_TIME )
            {
                Bullet* foo;
                IF_BULLET_GET( foo )
                {
                    foo->start( get_syncPoint(), this, _angle, 2200, 2 );
                    foo->damage = 1;
                }
                _lastShot = 0;
            }
        }

        ///UPDATE POSITION
        _xBuffer += _xDelta * gFrameTime;
        _yBuffer += _yDelta * gFrameTime;

        if( _myHurtCirc != nullptr )
        {
            *_myHurtCirc->area = { _xBuffer, _yBuffer, SHIP_RADIUS };
        }
    }
}

void PlayerShip::draw( void )
{
    if( _active )
    {
        SDL_Color c = color::WHITE;
        if( _iTime < INVICIBLE_TIME )
        {
            c = { 255, Uint8( _iTime/INVICIBLE_TIME*254 ), Uint8( _iTime/INVICIBLE_TIME*254 ), 255 };
            if( int(_iTime * 100)%20 > 10 )
            {
                c.a = 0;
            }
        }
        SDL_Rect drawinspot = { int( _xBuffer ) - SHIP_RADIUS, int( _yBuffer ) - SHIP_RADIUS, SHIP_RADIUS*2, SHIP_RADIUS*2 };
        gTextures[ assets::texture::SHIP ].render( drawinspot, c, 0, _angle );

        switch( _curretPower )
        {
        case SHIELD:
            {
                SDL_Color shieldC = { 0, 255, 255, 127 };
                if( _lastPowerUsed < powerMaxTime[ _curretPower ] )
                {
                    shieldC = { 255, 255, 255, uint8_t( _lastPowerUsed / powerMaxTime[ _curretPower ] * 127 ) };
                }
                gTextures[ assets::texture::POWER_UPS ].render( drawinspot, shieldC, 0 );
            }
            break;
        case HEAL:
            {
                SDL_Color hColour = color::WHITE;
                hColour.a = 0;
                if( _lastPowerUsed < powerMaxTime[ _curretPower ] )
                {
                    hColour.a = (powerMaxTime[ _curretPower ] - _lastPowerUsed)/powerMaxTime[ _curretPower ]*150;
                }

                gTextures[ assets::texture::POWER_UPS ].render( drawinspot, hColour, 3 );
            }
            break;
        default:
            {
                SDL_Rect pos = { int(_xBuffer - SHIP_RADIUS), int(_yBuffer + SHIP_RADIUS + 15), SHIP_RADIUS*2, 30 };
                if( _lastPowerUsed < powerMaxTime[ _curretPower ] )
                {
                    pos.w = _lastPowerUsed/powerMaxTime[ _curretPower ]*SHIP_RADIUS*2;
                }
                gTextures[ assets::texture::TEXT_BACKGROUND ].render( pos, color::CYAN );
            }
            break;
        }///switch
    }
}

void PlayerShip::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _xBuffer = pos.x;
    _yBuffer = pos.y;
    _xDelta = 0;
    _yDelta = 0;
    _angle = 0;
    _healthPoints = MAX_HEALTH_POINTS;

    _lastPowerUsed = 0;
    _lastShot = 0;

    if( _myHurtCirc == nullptr )
    {
        IF_HURT_CIRC( _myHurtCirc )
        {
            _active = true;
        }
    }
}

void PlayerShip::get_hurt( double damage, HurtCircle* from )
{
    if( _iTime > INVICIBLE_TIME && damage > 0 )
    {
        if( _curretPower == powerup::SHIELD && _lastPowerUsed >= powerMaxTime[ _curretPower ] )
        {
            _lastPowerUsed = 0;
            _iTime = 0;
        }
        else
        {
            _healthPoints -= damage;
            if( _curretPower == powerup::SHIELD || _curretPower == powerup::HEAL )
            {
                _lastPowerUsed = 0;
            }
            _iTime = 0;
        }
    }
}

void PlayerShip::push_me( float xd, float yd, HurtCircle* from )
{
    if( !_pushedFrame )
    {
        HealthyActor::push_me( xd, yd, from );
        _pushedFrame = true;
    }
}

void PlayerShip::set_powerup( powerup p )
{
    _curretPower = p;
}

SDL_Point PlayerShip::get_syncPoint( void )
{
    return { int( _xBuffer ), int( _yBuffer ) };
}
