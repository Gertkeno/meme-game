#include "EquipMenu.h"

#include <SDL2/SDL_shape.h>
#include <SDL2/SDL_events.h>
#include <sstream>

#include <BossSheet.h>
#include <PlayerShip.h>
#include <GameManager.h>

#include <gert_Camera.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>
#include <gert_CameraMath.h>
#include <gert_FontWrapper.h>

#define EQUIP_MENU_OPTIONS_MAX ( int(bosses::NONE) + int(PlayerShip::powerup::NONE) )

#define EM_MOD_POWERUP( p ) ( bosses::NONE + int( p ) )

EquipMenu::EquipMenu()
{
    //ctor
    _clickBoxs = new SDL_Rect[ EQUIP_MENU_OPTIONS_MAX ];
    _activeSelect = 0;
    _joyStickProc = false;

    re_adjust();
}

EquipMenu::~EquipMenu()
{
    //dtor
    delete[] _clickBoxs;
}

int EquipMenu::manage_input( const SDL_Event& event )
{
    int8_t xincrement = 0, yincrement = 0;
    switch( event.type )
    {
    case SDL_JOYBUTTONDOWN:
        if( event.jbutton.button == JOY_CTR( gpcSHOOT ) )
        {
            return _activeSelect;
        }
        break;
    case SDL_JOYAXISMOTION:
        if( event.jaxis.axis == SDL_CONTROLLER_AXIS_LEFTX )
        {
            if( event.jaxis.value > JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                xincrement = 1;
                _joyStickProc = true;
            }
            else if( event.jaxis.value < -JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                xincrement = -1;
                _joyStickProc = true;
            }
            else if( event.jaxis.value < JOY_HEAVY_DEADZONE && event.jaxis.value > -JOY_HEAVY_DEADZONE )
            {
                _joyStickProc = false;
            }
        }
        else if( event.jaxis.axis == SDL_CONTROLLER_AXIS_LEFTY )
        {
            if( event.jaxis.value > JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                yincrement = 1;
                _joyStickProc = true;
            }
            else if( event.jaxis.value < -JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                yincrement = -1;
                _joyStickProc = true;
            }
            else if( event.jaxis.value < JOY_HEAVY_DEADZONE && event.jaxis.value > -JOY_HEAVY_DEADZONE )
            {
                _joyStickProc = false;
            }
        }
        break;
    case SDL_KEYDOWN:
        switch( event.key.keysym.sym )
        {
        case SDLK_UP:
            yincrement = -1;
            break;
        case SDLK_DOWN:
            yincrement = 1;
            break;
        case SDLK_RIGHT:
            xincrement = 1;
            break;
        case SDLK_LEFT:
            xincrement = -1;
            break;
        case SDLK_RETURN:
        case SDLK_z:
            return _activeSelect;
        }
        break;
    }

    if( xincrement != 0 )
    {
        if( ( _activeSelect > 0 && xincrement < 0 ) || ( _activeSelect < EQUIP_MENU_OPTIONS_MAX - 1 && xincrement > 0 ) )
        {
            _activeSelect += xincrement;
        }
        else if( _activeSelect == 0 && xincrement < 0 )
        {
            _activeSelect = EM_MOD_POWERUP(PlayerShip::NONE) - 1;
        }
        else if( _activeSelect == EQUIP_MENU_OPTIONS_MAX - 1 && xincrement > 0 )
        {
            _activeSelect = 0;
        }
    }
    if( yincrement != 0 )
    {
        if( ( _activeSelect > bosses::NONE - 1 && yincrement < 0 ) || ( _activeSelect < PlayerShip::powerup::NONE && yincrement > 0 ) )
        {
            _activeSelect += yincrement*bosses::NONE;
        }
    }
    return -1;
}

void EquipMenu::draw( void )
{
    for( int i = 0; i < EQUIP_MENU_OPTIONS_MAX; i++ )
    {
        SDL_Color f = color::BLUE;
        if( _activeSelect == i )
        {
            f = color::BLACK;
        }
        else if( i - bosses::NONE == int( gGameManager->_player->get_powerup() ) )
        {
            f = color::GREEN;
        }
        gTextures[ assets::texture::CIRCLE ].render( _clickBoxs[ i ], f, 0, 0, false );
        uint16_t tex = 0;
        uint8_t subTex = 0;
        if( i < bosses::NONE )
        {
            switch( i )
            {
            case bosses::FRANKERZ:
                tex = assets::texture::FRANKER_Z;
                break;
            case bosses::LONNY_LINDA:
                tex = assets::texture::LONNY_LINDA;
                break;
            case bosses::MR_ELECTRIC:
                tex = assets::texture::MR_ELECTRIC;
                break;
            case bosses::DUD:
                tex = assets::texture::DOWN_UNDER;
                break;
            case bosses::TABE_MONSTER:
                tex = assets::texture::TABE_MONSTER;
                break;
            case bosses::BACON_BOT:
                tex = assets::texture::MAKIN_BACON_BOT;
                break;
            case bosses::CHAO_HIBBY:
                tex = assets::texture::CHAO_HIBBY;
                break;
            case bosses::MONA:
                tex = assets::texture::MONA;
                break;
            }
        }
        else
        {
            tex = assets::texture::POWER_UPS;
            switch( i - bosses::NONE )
            {
            case PlayerShip::BRAKE:
                subTex = 1;
                break;
            case PlayerShip::BACK_SHOT:
                subTex = 2;
                break;
            case PlayerShip::SHIELD:
                subTex = 0;
                break;
            case PlayerShip::HEAL:
                subTex = 3;
                break;
            case PlayerShip::MASSIVE_SHOT:
                subTex = 4;
                break;
            case PlayerShip::TELEPORT:
                tex = assets::texture::MAKIN_BACON_BOT;
                subTex = 2;
                break;
            case PlayerShip::AUTO_RETICLE:
                tex = assets::texture::DOWN_UNDER;
                subTex = 2;
                break;
            }
        }

        if( tex != 0 )
        {
            SDL_Color c = color::WHITE;
            if( i > bosses::NONE && (i-bosses::NONE) * EM_POWERUP_INC > int( gGameManager->total_boss_score() ) )
            {
                c = color::BLACK;
            }
            gTextures[ tex ].render( _clickBoxs[ i ], c, subTex, 0, false );
        }

        if( _activeSelect == i )
        {
            SDL_RendererFlip f = SDL_FLIP_NONE;
            if( _activeSelect < bosses::NONE )
            {
                f = SDL_FLIP_VERTICAL;
            }
            gTextures[ assets::texture::PICKER ].render( _clickBoxs[ i ], { 0, 255, 255, 190 }, 0, 0, false, f );
        }
    }//for( int i = 0; i < EQUIP_MENU_OPTIONS_MAX; i++ )

    std::stringstream tscore;
    tscore << "Total score: " << gGameManager->total_boss_score();
    SDL_Rect textSpace = rectAlign::middle( { 0, gCamera.h - FONT_HEIGHT*2, gFont->string_width( tscore.str() ), FONT_HEIGHT }, { 0, 0, gCamera.w, gCamera.h } );
    gFont->render( textSpace, tscore.str(), color::BLACK, false );

    if( _activeSelect < bosses::NONE )
    {
        std::stringstream score;
        score << "Score for this enemy: " << gGameManager->_bossScore[ bosses::guide( _activeSelect ) ];
        SDL_Rect textSpace = rectAlign::middle( { 0, gCamera.h - FONT_HEIGHT, gFont->string_width( score.str() ), FONT_HEIGHT }, { 0, 0, gCamera.w, gCamera.h } );
        gFont->render( textSpace, score.str(), color::BLACK, false );
    }
    else if( _activeSelect >= bosses::NONE )
    {
        SDL_Rect textSpace = { 0, gCamera.h - FONT_HEIGHT, 400, FONT_HEIGHT };
        if( _activeSelect - bosses::NONE == int( gGameManager->_player->get_powerup() ) )
        {
            const char* t = "This power is equiped.";
            textSpace.w = gFont->string_width( t );
            textSpace.x = gCamera.w/2 - textSpace.w/2;

            gFont->render( textSpace, t, color::BLACK, false );
        }
        else if( ( _activeSelect - bosses::NONE ) * EM_POWERUP_INC > (int)gGameManager->total_boss_score() )
        {
            std::stringstream needScore;
            needScore << "You need " << ( _activeSelect - bosses::NONE ) * EM_POWERUP_INC << " score to use this.";
            textSpace.w = gFont->string_width( needScore.str() );
            textSpace.x = gCamera.w/2 - textSpace.w/2;
            gFont->render( textSpace, needScore.str(), color::BLACK, false );
        }
    }
}

#define OPTION_SIZE 100
void EquipMenu::re_adjust( void )
{
    int fullLength = 0;
    for( int i = 0; i < bosses::NONE; i++ )
    {
        _clickBoxs[ i ] = { i*OPTION_SIZE, gCamera.h/2, OPTION_SIZE, OPTION_SIZE };
        fullLength += _clickBoxs[ i ].w;
    }
    for( int i = bosses::NONE; i < EQUIP_MENU_OPTIONS_MAX; i++ )
    {
        _clickBoxs[ i ] = { ( i - bosses::NONE )*OPTION_SIZE, gCamera.h/2 + OPTION_SIZE, OPTION_SIZE, OPTION_SIZE };
    }

    int offset = gCamera.w/2 - fullLength/2;
    for( uint8_t i = 0; i < EQUIP_MENU_OPTIONS_MAX; i++ )
    {
        _clickBoxs[ i ].x += offset;
    }
}
