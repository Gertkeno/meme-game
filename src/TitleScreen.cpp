#include "TitleScreen.h"
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_shape.h>

#include <gert_FontWrapper.h>
#include <gert_CameraMath.h>
#include <gert_Wrappers.h>
#include <gert_Camera.h>
#include <GameManager.h>
#include <TexHolder.h>
#include <particle.h>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define TS_CONTINUE_STR "Continue"
#define TS_NEW_GAME_STR "New Game"
#define TS_CREDITS_STR "Credits"
#define TS_QUIT_STR "Exit Game"

#define TS_ANIMATION_TIME 10

TitleScreen::TitleScreen()
{
    //ctor
    _mstr = new std::string[ TOTAL ];
    _activeSelect = tsOptions(0);

    _mstr[ CONTINUE ] = TS_CONTINUE_STR;
    _mstr[ NEW_GAME ] = TS_NEW_GAME_STR;
    _mstr[ CREDITS ] = TS_CREDITS_STR;
    _mstr[ QUIT ] = TS_QUIT_STR;
}

TitleScreen::~TitleScreen()
{
    //dtor
    delete[] _mstr;
}

void TitleScreen::update( void )
{
    SDL_Rect logoImg = { 0, 10, 700, 300 };
    logoImg.x = gCamera.w/2 - logoImg.w/2;

    if( rand()%20 == 0 )
    {
        Particle* fire;
        IF_PARTICLE_GET( fire )
        {
            SDL_Rect splace = { 0, 0, 70, 110 };
            splace.x = logoImg.x + rand()%(logoImg.w - splace.w);
            splace.y = logoImg.y + rand()%(logoImg.h - splace.h);

            fire->start( splace, 0.6 );
            uint8_t trand = rand()%3;
            if( trand == 0 )
            {
                fire->myTexture = assets::texture::EXPLOSION_0;
                fire->angle = rand()%360;
            }
            else
                fire->myTexture = assets::texture::FLAME;
            fire->effect = Particle::flags( Particle::FIXED | Particle::SHRINK | Particle::RISING );
        }
    }
}

void TitleScreen::draw( void )
{
    ///BIG OLD LOGO THING
    SDL_Rect logoImg = { 0, 10, 700, 300 };
    logoImg.x = gCamera.w/2 - logoImg.w/2;

    gTextures[ assets::texture::TITLE_IMG ].render( logoImg, color::WHITE, 0, 0, false );

    SDL_Rect splaces[ TOTAL ];
    for( uint8_t i = 0; i < TOTAL; i++ )
    {
        splaces[ i ] = { 0, int( i * FONT_HEIGHT * 1.2 ) + ( logoImg.y + logoImg.h + 10 ), gFont->string_width( _mstr[ i ] ), FONT_HEIGHT };
        splaces[ i ].x = gCamera.w/2 - splaces[ i ].w/2;

        SDL_Color c = color::BLACK;
        if( _activeSelect == i )
        {
            c = color::BLUE;
        }
        gFont->render( splaces[ i ], _mstr[ i ], c, false );
    }

    SDL_Rect vplace = { 0, gCamera.h - FONT_HEIGHT, 300, FONT_HEIGHT };
    gFont->render( vplace, "r3", color::BLACK, false );
}

TitleScreen::tsOptions TitleScreen::manage_input( const SDL_Event& event )
{
    int yinc{0};
    switch( event.type )
    {
    case SDL_JOYAXISMOTION:
        if( event.jaxis.axis == SDL_CONTROLLER_AXIS_LEFTY )
        {
            if( event.jaxis.value > JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                yinc = 1;
                _joyStickProc = true;
            }
            else if( event.jaxis.value < -JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                yinc = -1;
                _joyStickProc = true;
            }
            else if( event.jaxis.value < JOY_HEAVY_DEADZONE && event.jaxis.value > -JOY_HEAVY_DEADZONE )
            {
                _joyStickProc = false;
            }
        }
        break;
    case SDL_JOYBUTTONDOWN:
        if( event.jbutton.button == JOY_CTR( gpcSHOOT ) )
        {
            return _activeSelect;
        }
        else if( event.jbutton.button == JOY_CTR( gpcSTART ) )
        {
            return _activeSelect;
        }
        break;
    case SDL_KEYDOWN:
        switch( event.key.keysym.sym )
        {
        case SDLK_UP:
            yinc = -1;
            break;
        case SDLK_DOWN:
            yinc = 1;
            break;
        case SDLK_RETURN:
        case SDLK_z:
            return _activeSelect;
        }
        break;
    }

    if( yinc != 0 )
    {
        if( ( _activeSelect > 0 && yinc < 0 ) || ( _activeSelect < TOTAL - 1 && yinc > 0 ) )
        {
            _activeSelect = tsOptions( _activeSelect + yinc );
        }
        else if( _activeSelect == 0 && yinc < 0 )
        {
            _activeSelect = tsOptions( TOTAL - 1 );
        }
        else if( _activeSelect == TOTAL - 1 && yinc > 0 )
        {
            _activeSelect = tsOptions( 0 );
        }
    }

    return TOTAL;
}
