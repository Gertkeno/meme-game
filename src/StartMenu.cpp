#include "StartMenu.h"
#include <SDL2/SDL_shape.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_mixer.h>
#include <sstream>

#include <TexHolder.h>
#include <gert_Camera.h>
#include <gert_Wrappers.h>
#include <gert_FontWrapper.h>
#include <gert_SoundWrapper.h>
#include <GameManager.h>

#define SM_RESUME_STR "Resume"
#define SM_QUIT_STR "Quit"
#define SM_VOLUME_STR "Music:"
#define SM_SOUND_STR "Sound:"
#define SM_MAIN_MENU_STR "Main Menu"
#define SM_REBIND_STR "Rebind Controller"
#define SM_FULLSCREEN "Toggle FullScreen"

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

uint8_t StartMenu::Music_Volume = SM_MAX_MUSIC;
uint8_t StartMenu::Sound_Volume = SM_MAX_MUSIC;

StartMenu::StartMenu()
{
    //ctor
    _boxText = new std::string[ oTOTAL ];
    _clickBoxes = new SDL_Rect[ oTOTAL ];
    _activeSelect = options(0);

    re_adjust();
    _joyStickProc = false;
}

StartMenu::~StartMenu()
{
    //dtor
    delete[] _boxText;
    delete[] _clickBoxes;
}

void StartMenu::draw( void )
{
    for( uint16_t i = 0; i < oTOTAL; i++ )
    {
        SDL_Color c = { 80, 80, 80, 255 };
        if( _activeSelect == i )
        {
            c = color::BLUE;
        }
        gTextures[ assets::texture::TEXT_BACKGROUND ].render( _clickBoxes[ i ], c, 0, 0, false );
        gFont->render( _clickBoxes[ i ], _boxText[ i ], color::WHITE, false );
    }
}

StartMenu::options StartMenu::manage_inputs( const SDL_Event& even )
{
    int8_t increment = 0;
    int8_t ASinc = 0;
    switch( even.type )
    {
    case SDL_JOYAXISMOTION:
        if( even.jaxis.axis == SDL_CONTROLLER_AXIS_LEFTX )
        {
            if( even.jaxis.value > JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                ASinc = 1;
                _joyStickProc = true;
            }
            else if( even.jaxis.value < -JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                ASinc = -1;
                _joyStickProc = true;
            }
            else if( even.jaxis.value < JOY_HEAVY_DEADZONE && even.jaxis.value > -JOY_HEAVY_DEADZONE )
            {
                _joyStickProc = false;
            }
        }
        else if( even.jaxis.axis == SDL_CONTROLLER_AXIS_LEFTY )
        {
            if( even.jaxis.value > JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                increment = -1;
                _joyStickProc = true;
            }
            else if( even.jaxis.value < -JOY_HEAVY_DEADZONE && !_joyStickProc )
            {
                increment = 1;
                _joyStickProc = true;
            }
            else if( even.jaxis.value < JOY_HEAVY_DEADZONE && even.jaxis.value > -JOY_HEAVY_DEADZONE )
            {
                _joyStickProc = false;
            }
        }
        break;
    case SDL_JOYBUTTONDOWN:
        if( even.jbutton.button == JOY_CTR( gpcSHOOT ) )
        {
            return _activeSelect;
        }
        break;
    case SDL_KEYDOWN:
        switch( even.key.keysym.sym )
        {
        case SDLK_RETURN:
        case SDLK_z:
            Mix_HaltChannel( -1 );
            return _activeSelect;
        case SDLK_LEFT:
            ASinc = -1;
            break;
        case SDLK_RIGHT:
            ASinc = 1;
            break;
        case SDLK_UP:
            increment = 1;
            break;
        case SDLK_DOWN:
            increment = -1;
            break;
        }
        break;
    }
    if( ASinc != 0 )
    {
        if( (_activeSelect > 0 && ASinc < 0) || (_activeSelect < oTOTAL - 1 && ASinc > 0) )
        {
            _activeSelect = options( _activeSelect + ASinc );
        }
        else if( _activeSelect == 0 && ASinc < 0 )
        {
            _activeSelect = options( oTOTAL - 1 );
        }
        else if( _activeSelect == oTOTAL - 1 && ASinc > 0 )
        {
            _activeSelect = options(0);
        }
    }
    if( increment != 0 )
    {
        switch( _activeSelect )
        {
        case oMUSIC_VOLUME:
            if( ( Music_Volume > 0 && increment < 0 ) || ( Music_Volume < SM_MAX_MUSIC && increment > 0 ) )
            {
                Music_Volume += increment;
                Mix_VolumeMusic( float(Music_Volume)/SM_MAX_MUSIC * MIX_MAX_VOLUME );
                re_adjust();
            }
            break;
        case oSOUND_VOLUME:
            if( ( Sound_Volume > 0 && increment < 0 ) || ( Sound_Volume < SM_MAX_MUSIC && increment > 0 ) )
            {
                Sound_Volume += increment;
                Mix_Volume( -1, float(Sound_Volume)/SM_MAX_MUSIC * MIX_MAX_VOLUME );
                Mix_HaltChannel( -1 );
                int sf = rand()%assets::sfx::TOTAL;
                gSound[ sf ].play();
                re_adjust();
            }
        default:
            break;
        }
    }

    return oTOTAL;
}

void StartMenu::re_adjust( void )
{
    _boxText[ oFS_TOGGLE ] = SM_FULLSCREEN;
    _boxText[ oRESUME ] = SM_RESUME_STR;
    _boxText[ oQUIT ] = SM_QUIT_STR;
    {
        std::stringstream ss; ss << SM_VOLUME_STR << int(Music_Volume);
        _boxText[ oMUSIC_VOLUME ] = ss.str();
    }
    _boxText[ oMAIN_MENU ] = SM_MAIN_MENU_STR;
    {
        std::stringstream ss; ss << SM_SOUND_STR << int( Sound_Volume );
        _boxText[ oSOUND_VOLUME ] = ss.str();
    }
    _boxText[ oREBIND_CONTROLLER ] = SM_REBIND_STR;
    for( uint8_t i = 0; i < oTOTAL; i++ )
    {
        _clickBoxes[ i ] = { 0, gCamera.h/2 - FONT_HEIGHT/2, gFont->string_width( _boxText[ i ] ), FONT_HEIGHT };
        if( i > 0 )
        {
            _clickBoxes[ i ].x = _clickBoxes[ i-1 ].x + _clickBoxes[ i-1 ].w + 5;
        }
    }
    int totalWidth = _clickBoxes[ oTOTAL-1 ].x + _clickBoxes[ oTOTAL-1 ].w;
    int xOffset = gCamera.w/2 - totalWidth/2;
    for( uint8_t i = 0; i < oTOTAL; i++ )
    {
        _clickBoxes[ i ].x += xOffset;
    }
}
