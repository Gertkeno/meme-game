#include "HitCircle.h"

#include <gert_Collision.h>

HitCircle::HitCircle()
{
    //ctor
    _lifeTime = 1;
    totalLifeTime = 0;
    damage = 0;
    area = new Circle( { 0, 0, 0 } );
    enemy = true;
}

HitCircle::~HitCircle()
{
    //dtor
    delete area;
}

void HitCircle::update( void )
{
    if( _lifeTime <= totalLifeTime )
    {
        _lifeTime += gFrameTime;
    }
    else
    {
        totalLifeTime = 0;
        damage = 0;
    }
}

void HitCircle::start( Circle area, float damage, bool enemyFlag )
{
    *this->area = area;
    this->damage = damage;
    this->enemy = enemyFlag;
    _lifeTime = 0;
    totalLifeTime = 0;
}
