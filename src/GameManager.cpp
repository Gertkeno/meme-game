#include "GameManager.h"
#include <SDL2/SDL_shape.h>
#include <SDL2/SDL_mixer.h>
#include <sstream>

#include <gert_SoundWrapper.h>
#include <gert_FontWrapper.h>
#include <gert_CameraMath.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <gert_Camera.h>
#include <Background.h>
#include <TexHolder.h>

#include <HurtCircle.h>
#include <PushCircle.h>
#include <HitCircle.h>

#include <TitleScreen.h>
#include <PlayerShip.h>
#include <StartMenu.h>
#include <EquipMenu.h>
#include <particle.h>
#include <Bullet.h>

#include <DownUnderDan.h>
#include <TabeMonster.h>
#include <LonnyLinda.h>
#include <MrElectric.h>
#include <ChaoHibby.h>
#include <FrankerZ.h>
#include <BaconBot.h>
#include <Mona.h>

#ifdef _GERT_DEBUG
#include <iostream>
#include <sstream>
#endif // _GERT_DEBUG

#define MAX_CREDITS_TIME 40

#define SCENE_ICON_SIZE ( gCamera.h * 2/7 )
#define MAX_HIT_CIRCS 100
#define MAX_PUSH_CIRCS 50
#define MAX_HURT_CIRCS 20
#define MAX_BULLETS 100
#define MAX_PARTICLE 80

GameManager::GameManager()
{
    //ctor
    gameState = gmsTITLE_SCREEN;
    _event = new SDL_Event;

    /*Cutscene*/
    _credStr = nullptr;
    _sceneFile = nullptr;
    _sceneIcon = nullptr;
    SDL_StartTextInput();

    /*Under the hood game*/
    _hitC = new HitCircle[ MAX_HIT_CIRCS ];
    _hurtC = new HurtCircle[ MAX_HURT_CIRCS ];
    _pushC = new PushCircle[ MAX_PUSH_CIRCS ];

    _bossScore = new uint16_t[ bosses::NONE ];
    for( int i = 0; i < bosses::NONE; i++ )
    {
        _bossScore[ i ] = 0;
    }

    gamePadControls = new uint8_t[ gpcTOTAL ];
    gamePadControls[ gpcSHOOT ] = 10;
    gamePadControls[ gpcSPECIAL ] = 11;
    gamePadControls[ gpcACCELERATE ] = 9;
    gamePadControls[ gpcSTART ] = 4;

    _player = new PlayerShip;
    _currentBoss = bosses::NONE;
    _bossStartingHealth = 0;

    /*Visible game*/
    _theBoss = nullptr;
    _equips = new EquipMenu;
    _bg = new Background;
    _projectiles = new Bullet[ MAX_BULLETS ];
    _effect = new Particle[ MAX_PARTICLE ];
    _pmenu = new StartMenu;
    _titleScreen = new TitleScreen;

    open_music( "assets/audio/RuneScape Login Music.mp3" );
}

GameManager::~GameManager()
{
    //dtor
    delete _event;
    if( _sceneFile != nullptr )
    {
        SDL_RWclose( _sceneFile );
    }

    delete[] _hitC;
    delete[] _hurtC;
    delete[] _pushC;

    delete _player;
    delete[] _bossScore;
    delete _equips;
    delete _bg;
    delete[] _projectiles;
    delete[] _effect;
    delete[] gamePadControls;

    if( _theBoss != nullptr )
    {
        delete _theBoss;
    }

    delete _pmenu;
    delete _titleScreen;
}

void GameManager::update( void )
{
    while( SDL_PollEvent( _event ) )
    {
        switch( gameState ) ///GAMESATE ACTIONS
        {
        case gmsPLAYING:
            switch( _event->type )
            {
            case SDL_JOYBUTTONDOWN:
                if( _event->jbutton.button == gamePadControls[ gpcSTART ] )
                {
                    _pmenu->re_adjust();
                    gameState = gmsPAUSE;
                    Mix_PauseMusic();
                }
                break;
            case SDL_KEYDOWN:
                switch( _event->key.keysym.sym )
                {
                case SDLK_ESCAPE:
                    _pmenu->re_adjust();
                    gameState = gmsPAUSE;
                    Mix_PauseMusic();
                    break;
                }
                break;//SDL_KEYDOWN
            }
            break;//gmsPLAYING
        case gmsCUT_SCENE:
            switch( _event->type )
            {
            case SDL_JOYBUTTONDOWN:
                if( _event->jbutton.button == gamePadControls[ gpcSHOOT ] )
                {
                    if( !_next_scene() )
                    {
                        _what_now();
                    }
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                switch( _event->button.button )
                {
                case SDL_BUTTON_LEFT:
                    if( !_next_scene() )
                    {
                        _what_now();
                    }
                    break;//SDL_BUTTON_LEFT
                }
                break;//SDL_MOUSEBUTTONDOWN
            case SDL_KEYDOWN:
                switch( _event->key.keysym.sym )
                {
                case SDLK_z:
                case SDLK_x:
                case SDLK_RETURN:
                case SDLK_SPACE:
                    if( !_next_scene() )
                    {
                        _what_now();
                    }
                    break;//SDLK_SPACE;
                }
                break;//SDL_KEYDOWN
            }
            break;//gmsCUT_SCENE
            /* ALL EQUIPMENT RELATED THINGS: BOSS SELECT EQUIPMENT EQUIP MENU START */
        case gmsEQUIPMENT:
            {
                int activesel = _equips->manage_input( *_event );
                if( activesel >= 0 )
                {
                    if( activesel < bosses::NONE )
                    {
                        #define BOSS_START( bo, audio, story )\
                        open_music( audio );\
                        clean_bullets();\
                        if( _theBoss != nullptr ) { _theBoss->clean_up(); delete _theBoss; _theBoss = nullptr; }\
                        _theBoss = new bo;\
                        _player->start( { -1400, -1400 } );\
                        _theBoss->start( { 0, 0 } );\
                        _bossStartingHealth = _theBoss->get_healthPoints();\
                        open_scene( story );

                        _currentBoss = bosses::guide( activesel );
                        switch( _currentBoss )
                        {
                        case bosses::MONA:
                            BOSS_START( Mona, "assets/audio/PEEPO.mp3", "assets/story/mona.txt" )
                            break;
                        case bosses::CHAO_HIBBY:
                            BOSS_START( ChaoHibby, "assets/audio/retrobution.mp3", "assets/story/hibby.txt" )
                            break;
                        case bosses::BACON_BOT:
                            BOSS_START( BaconBot, "assets/audio/Unfounded Revenge.mp3", "assets/story/baconbot.txt" )
                            break;
                        case bosses::TABE_MONSTER:
                            BOSS_START( TabeMonster, "assets/audio/Sushi Go Round.mp3", "assets/story/tabemonster.txt" )
                            break;
                        case bosses::DUD:
                            BOSS_START( DownUnderDan, "assets/audio/Down Under Dan Log Jumping Theme.mp3", "assets/story/dan.txt" )
                            break;
                        case bosses::FRANKERZ:
                            BOSS_START( FrankerZ, "assets/audio/frankerz.mp3", "assets/story/dogface.txt" )
                            break;
                        case bosses::LONNY_LINDA:
                            BOSS_START( LonnyLinda, "assets/audio/Smiles and Tears.mp3", "assets/story/lonny.txt" )
                            break;
                        case bosses::MR_ELECTRIC:
                            BOSS_START( MrElectric, "assets/audio/retrobution.mp3", "assets/story/electric.txt" )
                            break;
                        case bosses::NONE:
                            break;
                        }
                    }
                    else if( activesel >= bosses::NONE )
                    {
                        PlayerShip::powerup ctActive = PlayerShip::powerup( activesel - bosses::NONE );

                        if( total_boss_score() > uint32_t( ctActive * EM_POWERUP_INC ) )
                        {
                            _player->set_powerup( ctActive );
                            if( !_save_game() )
                            {
                                #ifdef _GERT_DEBUG
                                std::cout << "Couldn't save Game\n";
                                #endif // _GERT_DEBUG
                            }
                        }
                    }
                }// _event->type == SDL_KEYDOWN && _event->key.keysym.sym == SDLK_z
                else if( ( _event->type == SDL_KEYDOWN && _event->key.keysym.sym == SDLK_ESCAPE ) || ( _event->type == SDL_JOYBUTTONDOWN && _event->jbutton.button == gamePadControls[ gpcSTART ] ) )
                {
                    gameState = gmsPAUSE;
                    _pmenu->re_adjust();
                }
            }
            break;//gmsEQUIPMENT
            /*NAME SELECTION INTRO */
        case gmsNAME_SELECT:
            switch( _event->type )
            {
            case SDL_TEXTINPUT:
                _characterName += _event->text.text;
                break;
            case SDL_KEYDOWN:
                switch( _event->key.keysym.sym )
                {
                case SDLK_BACKSPACE:
                    if( _characterName.length() > 0 )
                    {
                        _characterName.pop_back();
                    }
                    break;
                case SDLK_RETURN:
                    if( _characterName.length() > 0 )
                    {
                        open_scene( "assets/story/intro.txt" );
                        SDL_StopTextInput();
                    }
                    break;
                }
                break;
            }
            break;
        case gmsPAUSE:
            if( ( _event->type == SDL_KEYDOWN && ( _event->key.keysym.sym == SDLK_ESCAPE || _event->key.keysym.sym == SDLK_x ) ) ||
                ( _event->type == SDL_JOYBUTTONDOWN && ( _event->jbutton.button == gamePadControls[ gpcSTART ] || _event->jbutton.button == gamePadControls[ gpcSPECIAL ] ) ) )
            {
                _what_now();
                Mix_HaltChannel( -1 );
                if( gmsPLAYING == gameState )
                {
                    Mix_ResumeMusic();
                }
            }
            else
            {
                ///PAUSE MENU START
                switch( _pmenu->manage_inputs( *_event ) )
                {
                case StartMenu::oFS_TOGGLE:
                    if( ( SDL_GetWindowFlags( gWindow ) & SDL_WINDOW_FULLSCREEN_DESKTOP ) == SDL_WINDOW_FULLSCREEN_DESKTOP )
                    {
                        SDL_SetWindowFullscreen( gWindow, 0 );
                    }
                    else
                    {
                        SDL_SetWindowFullscreen( gWindow, SDL_WINDOW_FULLSCREEN_DESKTOP );
                    }
                    break;
                case StartMenu::oQUIT:
                    gameState = gmsTITLE_SCREEN;
                    clean_bullets();
                    if( _theBoss != nullptr ) { _theBoss->clean_up(); delete _theBoss; _theBoss = nullptr; }
                    open_music( "assets/audio/RuneScape Login Music.mp3" );
                    break;
                case StartMenu::oMAIN_MENU:
                    if( _theBoss != nullptr )
                    {
                        _theBoss->clean_up();
                    }
                    clean_bullets();
                    gameState = gmsEQUIPMENT;
                    break;
                case StartMenu::oRESUME:
                    _what_now();
                    if( gmsPLAYING == gameState )
                        Mix_ResumeMusic();
                    break;
                case StartMenu::oREBIND_CONTROLLER:
                    _configRole = 0;
                    gameState = gmsCONTROLLER_CONFIG;
                    break;
                case StartMenu::oMUSIC_VOLUME:
                    break;
                case StartMenu::oSOUND_VOLUME:
                    break;
                case StartMenu::oTOTAL:
                    break;
                }
            }
            break;
        case gmsCONTROLLER_CONFIG:
            switch( _event->type )
            {
            case SDL_JOYBUTTONDOWN:
                gamePadControls[ _configRole ] = _event->jbutton.button;
                _configRole += 1;
                break;
            case SDL_KEYDOWN:
                if( _event->key.keysym.sym == SDLK_ESCAPE )
                {
                    gameState = gmsPAUSE;
                    _configRole = 0;
                }
                break;
            }
            break;
        case gmsCREDITS_ROLL:
            if( ( _event->type == SDL_KEYDOWN && _event->key.keysym.sym == SDLK_ESCAPE ) || ( _event->type == SDL_JOYBUTTONDOWN && _event->jbutton.button == gamePadControls[ gpcSTART ] ) )
            {
                _end_credits();
                _what_now();
            }
            break;
        case gmsTITLE_SCREEN:
            switch( _titleScreen->manage_input( *_event ) )
            {
            case TitleScreen::CONTINUE:
                if( _load_game() )
                {
                    gameState = gmsEQUIPMENT;
                    _equips->re_adjust();
                    Mix_PauseMusic();
                    if( gMusic != nullptr )
                    {
                        Mix_FreeMusic( gMusic );
                        gMusic = nullptr;
                    }
                    break;
                }
            case TitleScreen::NEW_GAME:
                Mix_PauseMusic();
                if( gMusic != nullptr )
                {
                    Mix_FreeMusic( gMusic );
                    gMusic = nullptr;
                }
                gameState = gmsNAME_SELECT;
                _characterName = "Doofus";
                for( int i = 0; i < bosses::NONE; i++ )
                {
                    _bossScore[ i ] = 0;
                }
                break;
            case TitleScreen::CREDITS:
                _start_credits();
                gameState = gmsCREDITS_ROLL;
                break;
            case TitleScreen::QUIT:
                gameState = gmsQUITTING;
                break;
            case TitleScreen::TOTAL:
                break;
            }
            break;
        case gmsQUITTING:
            break;
        }//switch( gameState )

        ///GLOBAL ACTIONS
        _player->manage_event( *_event );
        switch( _event->type )
        {
        case SDL_WINDOWEVENT:
            if( _event->window.event == SDL_WINDOWEVENT_RESIZED )
            {
                SDL_GetWindowSize( gWindow, &gCamera.w, &gCamera.h );
                _equips->re_adjust();
            }
            break;
        case SDL_QUIT:
            gameState = gmsQUITTING;
            break;//SDL_QUIT
        case SDL_KEYDOWN:
            switch( _event->key.keysym.sym )
            {
            case SDLK_F4:
                gameState = gmsQUITTING;
                break;
            }
            break;//SDL_KEYDOWN
        }//_event->type
    }//SDL_PollEvent( _event )

    switch( gameState )///GAME STATE UPDATES
    {
    case gmsPLAYING:
        {
            for( uint16_t i = 0; i < MAX_HIT_CIRCS; i++ ) /*Update hit/hurt circles first so they can be drawn before the frame ends*/
            {
                if( _hitC[ i ].get_active() )
                {
                    for( unsigned int u = 0; u < MAX_HURT_CIRCS; u++ )
                    {
                        _hurtC[ u ].check_collide( &_hitC[ i ] );
                    }
                }
                _hitC[ i ].update();
            }
            for( uint16_t i = 0; i < MAX_PUSH_CIRCS; i++ )/*UPDATE PUSH CIRCLES*/
            {
                if( _pushC[ i ].get_active() )
                {
                    for( uint16_t u = 0; u < MAX_HURT_CIRCS; u++ )
                    {
                        _hurtC[ u ].check_push( &_pushC[ i ] );
                    }
                }
                _pushC[ i ].update();
            }

            _player->update();
            if( _theBoss != nullptr )
            {
                _theBoss->update();
            }
            for( unsigned int i = 0; i < MAX_BULLETS; i++ )
            {
                _projectiles[ i ].update();
            }
            for( unsigned int i = 0; i < MAX_PARTICLE; i++ )
            {
                _effect[ i ].update();
            }

            /*CAMERA UPDATING*/
            int widthDiff = gCamera.w/gCamera.z;
            int heightDiff = gCamera.h/gCamera.z;
            if( _theBoss != nullptr )
            {
                gCamera.z = gCamera.h / ( collision::distance( _theBoss->get_syncPoint(), _player->get_syncPoint() ) + 1100 );
                if( gCamera.z > 1 )
                {
                    gCamera.z = 1;
                }
                if( gCamera.z < 0.085 )
                {
                    gCamera.z = 0.085;
                }
                widthDiff = gCamera.w/gCamera.z - widthDiff;
                heightDiff = gCamera.h/gCamera.z - heightDiff;
                gCamera.x -= widthDiff/2;
                gCamera.y -= heightDiff/2;
            }

            {
                //boss tracking
                int camMargin = 600;
                if( _theBoss->get_syncPoint().x + camMargin > gCamera.x + gCamera.w/gCamera.z )
                {
                    gCamera.x = ( _theBoss->get_syncPoint().x + camMargin ) - gCamera.w/gCamera.z;
                }
                else if( _theBoss->get_syncPoint().x - camMargin < gCamera.x )
                {
                    gCamera.x = _theBoss->get_syncPoint().x - camMargin;
                }

                if( _theBoss->get_syncPoint().y + camMargin > gCamera.y + gCamera.h/gCamera.z )
                {
                    gCamera.y = ( _theBoss->get_syncPoint().y + camMargin ) - gCamera.h/gCamera.z;
                }
                else if( _theBoss->get_syncPoint().y - camMargin < gCamera.y )
                {
                    gCamera.y = _theBoss->get_syncPoint().y - camMargin;
                }

                //player tracking
                if( _player->get_syncPoint().x + camMargin > gCamera.x + gCamera.w/gCamera.z )
                {
                    gCamera.x = ( _player->get_syncPoint().x + camMargin ) - gCamera.w/gCamera.z;
                }
                else if( _player->get_syncPoint().x - camMargin < gCamera.x )
                {
                    gCamera.x = _player->get_syncPoint().x - camMargin;
                }

                if( _player->get_syncPoint().y + camMargin > gCamera.y + gCamera.h/gCamera.z )
                {
                    gCamera.y = ( _player->get_syncPoint().y + camMargin ) - gCamera.h/gCamera.z;
                }
                else if( _player->get_syncPoint().y - camMargin < gCamera.y )
                {
                    gCamera.y = _player->get_syncPoint().y - camMargin;
                }
            }

            /* BOSS DEFEAT PLAYER LOSE KILL WIN */
            if( ( _theBoss != nullptr && _theBoss->get_dead() ) || _player->get_dead() )
            {
                /*GENERAL CLEAN UP*/
                _theBoss->clean_up();
                clean_bullets();

                /* DEATH CUTSCENE */
                if( _theBoss->get_dead() )
                {
                    if( _bossScore[ _currentBoss ] < _player->get_healthPoints() )
                    {
                        _bossScore[ _currentBoss ] = _player->get_healthPoints();
                        if( !_save_game() )
                        {
                            #ifdef _GERT_DEBUG
                            std::cout << "Couldn't save game\n";
                            #endif // _GERT_DEBUG
                        }
                    }

                    switch( _currentBoss )
                    {
                    case bosses::BACON_BOT:
                        open_scene( "assets/story/baconbotdown.txt" );
                        break;
                    case bosses::DUD:
                        open_scene( "assets/story/dandown.txt" );
                        break;
                    case bosses::FRANKERZ:
                        open_scene( "assets/story/dogfacedown.txt" );
                        break;
                    case bosses::LONNY_LINDA:
                        open_scene( "assets/story/lonnydown.txt" );
                        break;
                    case bosses::MR_ELECTRIC:
                        open_scene( "assets/story/electricdown.txt" );
                        break;
                    case bosses::TABE_MONSTER:
                        open_scene( "assets/story/tabemonsterdown.txt" );
                        break;
                    case bosses::CHAO_HIBBY:
                        open_scene( "assets/story/hibbydown.txt" );
                        break;
                    case bosses::MONA:
                        open_scene( "assets/story/monadown.txt" );
                        break;
                    default:
                        _what_now();
                        break;
                    }
                }
                else
                {
                    gSound[ assets::sfx::SCREAM ].play();
                    open_scene( "assets/story/die.txt" );
                }

                _player->clean_up();
            }//( _theBoss != nullptr && _theBoss->get_dead() ) || _player->get_dead()
        }
        break;
    case gmsCUT_SCENE:
        {
            if( _theBoss != nullptr )
            {
                _theBoss->cut_scene_update();
                _player->cut_scene_update();
            }
            for( unsigned int i = 0; i < MAX_PARTICLE; i++ )
            {
                _effect[ i ].update();
            }
        }
        break;
    case gmsCONTROLLER_CONFIG:
        if( _configRole >= gpcTOTAL )
        {
            _configRole = 0;
            _what_now();
        }
        break;
    case gmsCREDITS_ROLL:
        _timeInCredits += gFrameTime;
        if( _timeInCredits >= MAX_CREDITS_TIME )
        {
            _end_credits();
            _what_now();
        }
        break;
    case gmsPAUSE:
        break;
    case gmsNAME_SELECT:
        break;
    case gmsTITLE_SCREEN:
        _titleScreen->update();
        for( uint16_t i = 0; i < MAX_PARTICLE; i++ )
        {
            _effect[ i ].update();
        }
        break;
    case gmsEQUIPMENT:
        if( _theBoss != nullptr )
        {
            delete _theBoss;
            _theBoss = nullptr;
        }
        break;
    case gmsQUITTING:
        break;
    }

    //GLOBAL UPDATES
}

void GameManager::draw( void )
{
    SDL_SetRenderDrawColor( gRenderer, 255, 255, 255, 255 );
    SDL_RenderClear( gRenderer );

    if( gameState == gmsPLAYING || gameState == gmsPAUSE || gameState == gmsQUITTING || gameState == gmsCUT_SCENE )
    {
        _bg->draw();
        for( unsigned int i = 0; i < MAX_BULLETS; i++ )
        {
            _projectiles[ i ].draw();
        }
        _player->draw();
        if( _theBoss != nullptr )
        {
            _theBoss->draw();
        }
        for( unsigned int i = 0; i < MAX_PARTICLE; i++ )
        {
            _effect[ i ].draw();
        }

        if( gameState == gmsPLAYING )
        {
            if( _theBoss != nullptr ) /*THIS INCLUDES UI*/
            {

                SDL_Rect bossHPBar = { 0, 0, int( float( _theBoss->get_healthPoints() )/float( _bossStartingHealth ) * gCamera.w ), 20 };
                gTextures[ assets::texture::HEALTH_BAR ].render( bossHPBar, color::WHITE, 0, 0, false );

                std::stringstream percHP; percHP << _player->get_healthPoints() << '%';
                SDL_Rect percentHP = rectAlign::middle( { 0, gCamera.h - FONT_HEIGHT, gFont->string_width( percHP.str() ), FONT_HEIGHT }, { 0, 0, gCamera.w, gCamera.h } );
                gFont->render( percentHP, percHP.str(), color::RED, false );
            }
        }
        #ifdef _GERT_DEBUG
        for( int i = 0; i < MAX_HURT_CIRCS; i++ )
        {
            if( _hurtC[ i ].target != nullptr )
            {
                gTextures[ assets::texture::CIRCLE ].render( _hurtC[ i ].area->ct_Rect(), { 255, 255, 0, 80 } );
            }
        }
        for( uint16_t i = 0; i < MAX_HIT_CIRCS; i++ )
        {
            if( _hitC[ i ].get_active() )
            {
                gTextures[ assets::texture::CIRCLE ].render( _hitC[ i ].area->ct_Rect(), { 255, 0, 0, 80 } );
            }
        }
        for( uint16_t i = 0; i < MAX_PUSH_CIRCS; i++ )
        {
            if( _pushC[ i ].get_active() )
            {
                gTextures[ assets::texture::CIRCLE ].render( _pushC[ i ].area->ct_Rect(), { 0, 255, 255, 80 } );
            }
        }
        #endif // _GERT_DEBUG
    }
    if( gameState == gmsEQUIPMENT )
    {
        _equips->draw();
    }
    else if( gameState == gmsPAUSE )
    {
        _pmenu->draw();
    }
    else if( gameState == gmsNAME_SELECT )
    {
        gFont->render( rectAlign::middle( { 0, 0, gFont->string_width( "Please enter a name:" ), FONT_HEIGHT }, { 0, 0, gCamera.w, gCamera.h } ), "Please enter a name:", color::BLACK, false );
        gFont->render( rectAlign::middle( { 0, FONT_HEIGHT, gFont->string_width( _characterName ), FONT_HEIGHT }, { 0, 0, gCamera.w, gCamera.h } ), _characterName, color::BLUE, false );
    }
    else if( gameState == gmsCUT_SCENE )
    {
        SDL_Rect textbg = { 0, gCamera.h - SCENE_ICON_SIZE, gCamera.w, SCENE_ICON_SIZE };
        gTextures[ assets::texture::TEXT_BACKGROUND ].render( textbg, color::WHITE, 0, 0, false );

        SDL_Rect iconSpace = { 0, gCamera.h - SCENE_ICON_SIZE, SCENE_ICON_SIZE, SCENE_ICON_SIZE };
        SDL_Rect textSpace = { iconSpace.x + iconSpace.w, gCamera.h - SCENE_ICON_SIZE, gCamera.w - ( iconSpace.w + iconSpace.x ) - 10, SCENE_ICON_SIZE };
        gFont->render( textSpace, _sceneText, color::BLACK, false );
        if( _sceneIcon != nullptr )
        {
            _sceneIcon->render( iconSpace, color::WHITE, 0, 0, false );
        }
    }
    else if( gameState == gmsCONTROLLER_CONFIG )
    {
        std::string butpress;
        switch( _configRole )
        {
        case gpcSHOOT:
            butpress = "SHOOT";
            break;
        case gpcACCELERATE:
            butpress = "ACCELERATE";
            break;
        case gpcSPECIAL:
            butpress = "SPECIAL";
            break;
        case gpcSTART:
            butpress = "START";
            break;
        }

        SDL_Rect splace = { 0, gCamera.h/2 - FONT_HEIGHT/2, gFont->string_width( butpress ), FONT_HEIGHT };
        splace = rectAlign::middle( splace, { 0, 0, gCamera.w, gCamera.h } );
        gFont->render( splace, butpress, color::BLACK, false );
    }
    else if( gameState == gmsCREDITS_ROLL )
    {
        SDL_Rect credPlace[ _totalCredits ];
        int lastCredit = _totalCredits * FONT_HEIGHT * 1.5;
        for( uint8_t i = 0; i < _totalCredits; i++ )
        {
            credPlace[ i ] = { 0, int( gCamera.h + i * FONT_HEIGHT*1.5 ), gFont->string_width( _credStr[ i ] ), FONT_HEIGHT };
            credPlace[ i ].x = gCamera.w/2 - credPlace[ i ].w/2;

            float percent = _timeInCredits/MAX_CREDITS_TIME;
            credPlace[ i ].y -= ( gCamera.h + lastCredit ) * percent;

            gFont->render( credPlace[ i ], _credStr[ i ], color::BLACK, false );
        }
    }
    else if( gameState == gmsTITLE_SCREEN )
    {
        _titleScreen->draw();
        for( uint16_t i = 0; i < MAX_PARTICLE; i++ )
        {
            _effect[ i ].draw();
        }
    }


    #ifdef _GERT_DEBUG
    char foo[] = { char( gameState + '0' ), '\0' };
    gFont->render( { 0, 0, 200, 200 }, foo, color::MAGENTA, false );

    #define TOTAL_READ( p, max, name )\
    {\
        uint16_t positives = 0;\
        for( uint16_t i = 0; i < max; i++ )\
        {\
            if( p )\
            {\
                positives += 1;\
            }\
    }\
    std::stringstream posRead; posRead << #name << ":" << positives << "/" << max;\
    gFont->render({ 0, FONT_HEIGHT*iteration, 200, 200}, posRead.str(), color::MAGENTA, false );\
    }\
    iteration++;

    {
        int iteration = 1;
        TOTAL_READ( _projectiles[ i ].is_active(), MAX_BULLETS, Bullets )
        TOTAL_READ( _hitC[ i ].get_active(), MAX_HIT_CIRCS, HitCircles )
        TOTAL_READ( _effect[ i ].get_active(), MAX_PARTICLE, Particles )
    }
    #endif // _GERT_DEBUG

    SDL_RenderPresent( gRenderer );
}

#define SCENE_COMMAND( a, b ) ( buffer[ 0 ] == *#a && buffer[ 1 ] == *#b )

bool GameManager::_next_scene( void )
{
    if( _sceneFile != nullptr )
    {
        _sceneText.clear();
        char buffer[ 2 ] = { '\0', '\0' };
        while( SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 ) > 0 && buffer[ 1 ] != '\n' )
        {
            if( SCENE_COMMAND( $, n ) )
            {
                _sceneText += _characterName;
                SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 );
            }
            else if( buffer[ 0 ] == '[' && buffer[ 1 ] >= '0' && buffer[ 1 ] <= '9' )
            {
                std::string read;
                read += buffer[ 1 ];
                while( SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 ) > 0 && buffer[ 1 ] != ']' )
                {
                    read += buffer[ 1 ];
                }
                int totalNum = strtol( read.c_str(), (char**)NULL, 10 );
                if( totalNum < assets::texture::TOTAL )
                {
                    _sceneIcon = &gTextures[ totalNum ];
                }
                else
                {
                    _sceneIcon = nullptr;
                }
                SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 );
            }
            else if( SCENE_COMMAND( <, x ) )
            {
                std::string read;
                while( SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 ) > 0 && buffer[ 1 ] != '>' )
                {
                    read += buffer[ 1 ];
                }
                gCamera.x = strtol( read.c_str(), (char**)NULL, 10 );
                /** strol: a simple guide
                    input a c string and it will output the numerical value, I've been making this function all too often.
                    second input isn't useful to me so set it to char**NULL
                    third input is what base to read the number in
                */
                SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 );
            }
            else if( SCENE_COMMAND( <, y ) )
            {
                std::string read;
                while( SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 ) > 0 && buffer[ 1 ] != '>' )
                {
                    read += buffer[ 1 ];
                }
                gCamera.y = strtol( read.c_str(), (char**)NULL, 10 );
                SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 );
            }
            else if( SCENE_COMMAND( <, z ) )
            {
                std::string read;
                while( SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 ) > 0 && buffer[ 1 ] != '>' )
                {
                    read += ( buffer[ 1 ] );
                }
                gCamera.z = strtod( read.c_str(), (char**)NULL );
                SDL_RWread( _sceneFile, &buffer[ 1 ], sizeof( char ), 1 );
            }
            else
            {
                if( buffer[ 0 ] != '\0' && buffer[ 0 ] != '\n' )
                {
                    _sceneText += buffer[ 0 ];
                }
            }
            buffer[ 0 ] = buffer[ 1 ];
        }
        if( buffer[ 1 ] != '\n' )
        {
            SDL_RWclose( _sceneFile );
            _sceneFile = nullptr;
            _sceneText += buffer[ 0 ];//gets last character in buffer[ 0 ] when line ends
        }
        #ifdef _LINUX
        else
        {
            _sceneText += buffer[ 0 ];
        }
        #endif // _LINUX
        if( _sceneText.empty() )
        {
            _next_scene();
        }
        return true;
    }
    else
    {
        return false;
    }
}

bool GameManager::open_scene( std::string fname )
{
    if( _sceneFile == nullptr )
    {
        _sceneText.clear();
        try
        {
            _sceneFile = SDL_RWFromFile( fname.c_str(), "r" );
            if( _sceneFile != NULL )
            {
                gameState = gmsCUT_SCENE;
                _next_scene();
                return true;
            }
            else
            {
                std::string foo = "Couldn't open file :>";
                foo += fname;
                throw foo.c_str();
            }
        }
        catch( const char* ex )
        {
            #ifdef _GERT_DEBUG
            std::cout << ex << std::endl;
            #endif // _GERT_DEBUG
            return false;
        }
    }
    else
    {
        return false;
    }
}

HurtCircle* GameManager::get_hurt_circle( HealthyActor* t )
{
    for( unsigned int i = 0; i < MAX_HURT_CIRCS; i++ )
    {
        if( _hurtC[ i ].target == nullptr )
        {
            _hurtC[ i ].target = t;
            return &_hurtC[ i ];
        }
    }
    return nullptr;
}

HitCircle* GameManager::get_hit_circle( void )
{
    for( unsigned int i = 0; i < MAX_HIT_CIRCS; i++ )
    {
        if( !_hitC[ i ].get_active() )
        {
            return &_hitC[ i ];
        }
    }
    return nullptr;
}

PushCircle* GameManager::get_push_circle( void )
{
    for( uint16_t i = 0; i < MAX_PUSH_CIRCS; i++ )
    {
        if( !_pushC[ i ].get_active() )
        {
            return &_pushC[ i ];
        }
    }
    return nullptr;
}

void GameManager::_what_now( void )
{
    if( _theBoss != nullptr )
    {
        if( _theBoss->get_dead() || _player->get_dead() )
        {
            Mix_PauseMusic();
            if( gMusic != nullptr )
            {
                Mix_FreeMusic( gMusic );
                gMusic = nullptr;
            }
            if( _theBoss != nullptr )
            {
                delete _theBoss;
                _theBoss = nullptr;
            }
            gameState = gmsEQUIPMENT;
            _equips->re_adjust();
        }
        else
        {
            Mix_ResumeMusic();
            gameState = gmsPLAYING;
        }
    }
    else if( gameState == gmsCREDITS_ROLL )
    {
        gameState = gmsTITLE_SCREEN;
    }
    else
    {
        gameState = gmsEQUIPMENT;
        _equips->re_adjust();
    }
}

uint32_t GameManager::total_boss_score( void )
{
    uint32_t t = 0;
    for( int i = 0; i < bosses::NONE; i++ )
    {
        t += _bossScore[ i ];
    }
    return t;
}

Bullet* GameManager::get_bullet( void )
{
    for( unsigned int i = 0; i < MAX_BULLETS; i++ )
    {
        if( !_projectiles[ i ].is_active() )
        {
            return &_projectiles[ i ];
        }
    }
    return nullptr;
}

SDL_Point GameManager::get_player_point( void )
{
    return _player->get_syncPoint();
}

SDL_Point GameManager::get_enemy_point( void )
{
    if( _theBoss != nullptr )
    {
        return _theBoss->get_syncPoint();
    }
    return { 0, 0 };
}

Particle* GameManager::get_particle( void )
{
    for( unsigned int i = 0; i < MAX_PARTICLE; i++ )
    {
        if( !_effect[ i ].get_active() )
        {
            return &_effect[ i ];
        }
    }
    return nullptr;
}

bool GameManager::_save_game( void )
{
    SDL_RWops* sFile = SDL_RWFromFile( "save.dat", "w" );
    if( sFile != NULL )
    {
        uint8_t spowerup = uint8_t(_player->get_powerup());
        SDL_RWwrite( sFile, &spowerup, sizeof( uint8_t ), 1 );

        for( unsigned int i = 0; i < bosses::NONE; i++ )
        {
            SDL_RWwrite( sFile, &_bossScore[ i ], sizeof( uint16_t ), 1 );
        }
        SDL_RWwrite( sFile, _characterName.c_str(), sizeof( char ), _characterName.length() );
        SDL_RWclose( sFile );
        return true;
    }
    return false;
}

bool GameManager::_load_game( void )
{
    SDL_RWops* sFile = SDL_RWFromFile( "save.dat", "r" );
    if( sFile != NULL )
    {
        uint8_t pupBuffer = 0;
        SDL_RWread( sFile, &pupBuffer, sizeof( uint8_t ), 1 );
        _player->set_powerup( PlayerShip::powerup(pupBuffer) );

        for( unsigned int i = 0; i < bosses::NONE; i++ )
        {
            SDL_RWread( sFile, &_bossScore[ i ], sizeof( uint16_t ), 1 );
        }
        _characterName.clear();
        char buffer = '0';
        while( SDL_RWread( sFile, &buffer, sizeof( char ), 1 ) > 0 && buffer != '\0' )
        {
            _characterName += buffer;
        }
        SDL_RWclose( sFile );
        return true;
    }
    return false;
}

void GameManager::clean_bullets( void )
{
    for( uint16_t i = 0; i < MAX_BULLETS; i++ )
    {
        _projectiles[ i ].force_kill();
    }
    for( uint16_t i = 0; i < MAX_PARTICLE; i++ )
    {
        _effect[ i ].force_kill();
    }
    for( uint16_t i = 0; i < MAX_HIT_CIRCS; i++ )
    {
        _hitC[ i ].force_kill();
    }
    for( uint16_t i = 0; i < MAX_PUSH_CIRCS; i++ )
    {
        _pushC[ i ].force_kill();
    }
}

void GameManager::open_music( const char* fname )
{
    if( gMusic != nullptr )
    {
        Mix_FreeMusic( gMusic );
        gMusic = nullptr;
    }
    gMusic = Mix_LoadMUS( fname );
    Mix_PlayMusic( gMusic, -1 );
}

bool GameManager::_all_bosses_done( void )
{
    bool lastit = true;
    for( uint8_t i = 0; i < bosses::NONE; i++ )
    {
        if( _bossScore[ i ] <= 0 )
        {
            lastit = false;
            break;
        }
    }
    return lastit;
}

void GameManager::_end_credits( void )
{
    if( _credStr != nullptr )
    {
        delete[] _credStr;
    }
}

bool GameManager::_start_credits( void )
{
    SDL_RWops* credFile = SDL_RWFromFile( "assets/credits.txt", "r" );
    if( credFile != NULL )
    {
        _timeInCredits = 0;
        _totalCredits = 0;
        char buffer;
        while( SDL_RWread( credFile, &buffer, sizeof( char ), 1 ) > 0 )
        {
            if( buffer == '\n' )
            {
                _totalCredits += 1;
            }
        }
        _credStr = new std::string[ _totalCredits ];
        for( uint8_t i = 0; i < _totalCredits; i++ )
        {
            _credStr[ i ].clear();
        }

        ///Actual reading
        uint8_t activeName = 0;
        SDL_RWseek( credFile, 0, RW_SEEK_SET );
        while( SDL_RWread( credFile, &buffer, sizeof( char ), 1 ) > 0 )
        {
            if( buffer != '\n' )
            {
                if( activeName < _totalCredits )
                {
                    _credStr[ activeName ] += buffer;
                }
            }
            else
            {
                #ifndef _LINUX
                _credStr[ activeName ].pop_back();
                #endif // _LINUX
                activeName += 1;
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}
