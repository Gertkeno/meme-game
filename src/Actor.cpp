#include "Actor.h"
#include <SDL2/SDL.h>

Actor::Actor()
{
    //ctor
    _xBuffer = 0;
    _yBuffer = 0;
}

Actor::~Actor()
{
    //dtor
}

SDL_Point Actor::get_syncPoint( void )
{
    return { int( _xBuffer ), int( _yBuffer ) };
}

void Actor::start( const SDL_Point& pos )
{
    _xBuffer = pos.x;
    _yBuffer = pos.y;
}
