#include "HurtCircle.h"
#include <HitCircle.h>
#include <PushCircle.h>
#include <gert_Collision.h>
#include <HealthyActor.h>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

HurtCircle::HurtCircle()
{
    //ctor
    target = nullptr;
    area = new Circle( { 0, 0, 0 } );
}

HurtCircle::~HurtCircle()
{
    //dtor
    delete area;
}

void HurtCircle::check_collide( HitCircle* hc )
{
    if( target != nullptr && hc->get_active() && hc->enemy != target->get_enemy() && collision::get_collide( *hc->area, *this->area ) )
    {
        target->get_hurt( hc->damage, this );
    }
}

void HurtCircle::check_push( PushCircle* pc )
{
    if( target != nullptr && pc->get_active() && pc->enemy != target->get_enemy() && collision::get_collide( *this->area, *pc->area ) )
    {
        target->push_me( pc->get_xForce(), pc->get_yForce(), this );
    }
}
