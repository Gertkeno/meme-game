#include "Bullet.h"
#include <cmath>
#include <SDL2/SDL_shape.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>

#ifndef M_PI
#define M_PI 3.141592653589793238462643383279502884197169399375105
#endif // M_PI

#define BULLET_FADE_POINT ( totalTime * 0.85 )

Bullet::Bullet()
{
    //ctor
    _xDelta = 0;
    _yDelta = 0;
    ability = bulletFunction::basicShot;
    _lifeTime = 0;
    totalTime = 0;

    myTexture = assets::texture::LASOR_SHOT;
    radius = 0;
    _abilityCount = 0;
    totalAbilityCount = 0;
    damage = 0;
}

Bullet::~Bullet()
{
    //dtor
}

void Bullet::update( void )
{
    _lifeTime += gFrameTime;
    if( is_active() )
    {
        _xBuffer += _xDelta * gFrameTime;
        _yBuffer += _yDelta * gFrameTime;

        double ctTime = ( totalTime / totalAbilityCount );
        if( ability != nullptr && ( _lifeTime - ( _abilityCount * ctTime ) > ctTime || totalAbilityCount == 0 ) )
        {
            ability( this );
            _abilityCount += 1;
        }
        bulletFunction::basicShot( this );
    }
}

void Bullet::draw( void )
{
    if( is_active() )
    {
        SDL_Rect foo = { int( _xBuffer  - radius ), int( _yBuffer - radius ), int( radius*2 ), int( radius*2 ) };
        SDL_Color c = color::WHITE;
        if( _lifeTime > BULLET_FADE_POINT )
        {
            c = { 255, 255, 255, Uint8( (BULLET_FADE_POINT - _lifeTime)/(totalTime - BULLET_FADE_POINT) * 254 ) };
        }
        gTextures[ myTexture ].render( foo, c, subTex, _angle );
    }
}

void Bullet::start( const SDL_Point& pos, HealthyActor* o, float angle, float speed, float life )
{
    Actor::start( pos );
    _angle = angle;
    _xDelta = cos( angle * M_PI/180 ) * speed;
    _yDelta = sin( angle * M_PI/180 ) * speed;
    totalTime = life;
    _lifeTime = 0;

    radius = 40;
    owner = o;
    damage = 1;

    _abilityCount = 0;
    totalAbilityCount = 0;
    ability = nullptr;
    myTexture = assets::texture::LASOR_SHOT;
    subTex = 0;
}

void Bullet::alter_direction( float angle )
{
    if( angle > 360 )
    {
        angle -= 360;
    }
    else if( angle < 0 )
    {
        angle += 360;
    }
    _angle = angle;
    float speed = std::abs( _xDelta ) + std::abs( _yDelta );
    _xDelta = cos( angle * M_PI/180 ) * speed;
    _yDelta = sin( angle * M_PI/180 ) * speed;
}

void Bullet::force_kill( void )
{
    _lifeTime = 0;
    totalTime = 0;
    damage = 0;
    _abilityCount = 0;
    totalAbilityCount = 0;
    ability = bulletFunction::basicShot;
}
