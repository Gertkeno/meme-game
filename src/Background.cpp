#include "Background.h"

#include <SDL2/SDL_shape.h>
#include <gert_Camera.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>

#define BGW ( 1600 * 10 )
#define BGH ( 1200 * 10 )

Background::Background()
{
    //ctor
    _area = new SDL_Rect[TOTAL];
    _area[ MM ] = { 0 - BGW/2, 0 - BGH/2, BGW, BGH };
    _area[ TL ] = { _area[ MM ].x - _area[ MM ].w, _area[ MM ].y - _area[ MM ].h, BGW, BGH };
    _area[ TM ] = { _area[ MM ].x, _area[ MM ].y - _area[ MM ].h, BGW, BGH };
    _area[ TR ] = { _area[ MM ].x + _area[ MM ].w, _area[ MM ].y - _area[ MM ].h, BGW, BGH };
    _area[ ML ] = { _area[ MM ].x - _area[ MM ].w, _area[ MM ].y, BGW, BGH };
    _area[ MR ] = { _area[ MM ].x + _area[ MM ].w, _area[ MM ].y, BGW, BGH };
    _area[ BL ] = { _area[ MM ].x - _area[ MM ].w, _area[ MM ].y + _area[ MM ].h, BGW, BGH };
    _area[ BM ] = { _area[ MM ].x, _area[ MM ].y + _area[ MM ].h, BGW, BGH };
    _area[ BR ] = { _area[ MM ].x + _area[ MM ].w, _area[ MM ].y + _area[ MM ].h, BGW, BGH };
}

Background::~Background()
{
    //dtor
    delete[] _area;
}

void Background::draw( void )
{
    bool changePlace = false;
    if( _area[ MM ].x > gCamera.x )
    {
        _area[ MM ].x -= BGW;
        changePlace = true;
    }
    else if( _area[ MM ].x + _area[ MM ].w < gCamera.x + gCamera.w/gCamera.z )
    {
        _area[ MM ].x += BGW;
        changePlace = true;
    }
    if( _area[ MM ].y > gCamera.y )
    {
        _area[ MM ].y -= BGH;
        changePlace = true;
    }
    else if( _area[ MM ].y + _area[ MM ].h < gCamera.y + gCamera.h/gCamera.z  )
    {
        _area[ MM ].y += BGH;
        changePlace = true;
    }

    if( changePlace )
    {
        _area[ TL ] = { _area[ MM ].x - _area[ MM ].w, _area[ MM ].y - _area[ MM ].h, BGW, BGH };
        _area[ TM ] = { _area[ MM ].x, _area[ MM ].y - _area[ MM ].h, BGW, BGH };
        _area[ TR ] = { _area[ MM ].x + _area[ MM ].w, _area[ MM ].y - _area[ MM ].h, BGW, BGH };
        _area[ ML ] = { _area[ MM ].x - _area[ MM ].w, _area[ MM ].y, BGW, BGH };
        _area[ MR ] = { _area[ MM ].x + _area[ MM ].w, _area[ MM ].y, BGW, BGH };
        _area[ BL ] = { _area[ MM ].x - _area[ MM ].w, _area[ MM ].y + _area[ MM ].h, BGW, BGH };
        _area[ BM ] = { _area[ MM ].x, _area[ MM ].y + _area[ MM ].h, BGW, BGH };
        _area[ BR ] = { _area[ MM ].x + _area[ MM ].w, _area[ MM ].y + _area[ MM ].h, BGW, BGH };
    }

    for( int i = 0; i < TOTAL; i++ )
    {
        gTextures[ assets::texture::BACKGROUND ].render( _area[ i ], color::WHITE );
    }
}
