mkdir -p .obj

for file in src/*.cpp src/boss/*.cpp gertTech/*.cpp; do
	echo "building $file ..."
	TRUNC="${file##*/}"
	if [ $file -nt ".obj/${TRUNC%.*}.o" ]; then
		g++ -I include -I include/boss -I gertTech $file -c -o ".obj/${TRUNC%.*}.o"
	fi
done
g++ -I include -I include/boss -I gertTech main.cpp -c -o ".obj/main.o"

echo "linking!!!"
g++ .obj/*.o -lSDL2 -lSDL2_mixer -lSDL2_image -lSDL2_ttf -o memegame
