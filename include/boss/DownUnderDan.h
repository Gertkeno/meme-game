#ifndef DOWNUNDERDAN_H
#define DOWNUNDERDAN_H

#include <HealthyActor.h>

class GameManager;
extern GameManager* gGameManager;

class SDL_Point;
class HurtCircle;

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;

class DownUnderDan: public HealthyActor
{
    public:
        DownUnderDan();
        virtual ~DownUnderDan();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void cut_scene_update( void );

        void get_hurt( double damage, HurtCircle* from );
        void clean_up( void );
    protected:
        /*ATTACK BASE*/
        enum attacks: uint8_t
        {
            JUMP_TO,
            SHOOTING_SPREE,
            HONEY_ANT,
            BOOMERANG,
            TOTAL
        };
        float _timeSinceAttack;
        attacks _lastAttack;
        float _attack_timer( attacks t = TOTAL );
        uint8_t _attackProcs;
        uint8_t _total_procs( attacks t = TOTAL );

        /*SPECIFIC ATTACKS*/
        float _xAnt;
        float _yAnt;
        bool _antHit;
        bool _firstProc;
        bool _oddProc;
        HurtCircle* _antHC;
        SDL_Point* _shotPoints;

        float _spinning;

        /*PAIN*/
        float _iTime;
        HurtCircle* _bodyHC;
        float _xDelta;
        float _yDelta;
    private:
};

#endif // DOWNUNDERDAN_H
