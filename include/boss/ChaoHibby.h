#ifndef CHAOHIBBY_H
#define CHAOHIBBY_H

#include <HealthyActor.h>

typedef unsigned char uint8_t;
class GameManager;
extern GameManager* gGameManager;

class ChaoHibby : public HealthyActor
{
    public:
        ChaoHibby();
        virtual ~ChaoHibby();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void cut_scene_update( void );

        void get_hurt( double damage, HurtCircle* from );
        void clean_up( void );

    protected:
        /* BASIC ATTACKS */
        enum attacks: uint8_t
        {
            BASTION,
            BALL_MOVE,
            PURIFY,
            EVOLVE,
            TOTAL
        };
        float _timeSinceAttack;
        attacks _lastAttack;
        float _attack_timer( attacks t = TOTAL );
        uint8_t _attackProcs;
        uint8_t _total_procs( attacks t = TOTAL );

        /*SPECIFICS*/
        bool _firstProc;
        bool _evil;
        SDL_Point* _ballPoint;
        SDL_Point* _ballTo;

        /*PAIN*/
        float _iTime;
        HurtCircle** _bodyHC;
    private:
};

#endif // CHAOHIBBY_H
