#ifndef TABESMEAT_H
#define TABESMEAT_H

#include <HealthyActor.h>

typedef unsigned char uint8_t;

class GameManager;
extern GameManager* gGameManager;
class Circle;

class TabesMeat : public HealthyActor
{
    public:
        TabesMeat();
        virtual ~TabesMeat();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );

        void get_hurt( double damage, HurtCircle* from );
        void force_kill( void );

        bool get_dead( void );
        Circle get_area( void );
        void move_to( const SDL_Point& pos, float speed );
        void set_syncPoint( const SDL_Point& pos );
        void set_hitbox( float dam );

        uint8_t subtex;
    protected:
        HurtCircle* _body;
        float _iTime;
        bool _starting;
        float _angle;
    private:
};

#endif // TABESMEAT_H
