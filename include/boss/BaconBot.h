#ifndef BACONBOT_H
#define BACONBOT_H

#include <HealthyActor.h>

class GameManager;
extern GameManager* gGameManager;
class HurtCircle;
extern double gFrameTime;

typedef unsigned char uint8_t;

class BaconBot : public HealthyActor
{
    public:
        BaconBot();
        virtual ~BaconBot();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void cut_scene_update( void );

        void get_hurt( double damage, HurtCircle* from );
        void clean_up( void );

    protected:
        /*BASE ATTACK*/
        enum attacks: uint8_t
        {
            SHOOTY,
            BIG_LAZOR,
            SPIN_CHAMBER,
            PULSE,
            TOTAL
        };
        float _timeSinceAttack;
        attacks _lastAttack;
        float _attack_timer( attacks t = TOTAL );
        uint8_t _attackProcs;
        uint8_t _total_procs( attacks t = TOTAL );

        /*SPECIFIC ATTACK DATA*/
        SDL_Point* _pointTarget;
        bool _firstProc;
        float _weirdAngle;

        /*PAIN*/
        HurtCircle* _body;
        float _iTime;

    private:
};

#endif // BACONBOT_H
