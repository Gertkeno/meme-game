#ifndef MRELECTRIC_H
#define MRELECTRIC_H

#include <HealthyActor.h>

typedef unsigned short int uint16_t;
typedef unsigned char uint8_t;

class GameManager;
extern GameManager* gGameManager;

class HurtCircle;
class SDL_Point;

class MrElectric: public HealthyActor
{
    public:
        MrElectric();
        virtual ~MrElectric();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void cut_scene_update( void );

        void get_hurt( double damage, HurtCircle* from );
        void push_me( float xd, float yd, HurtCircle* from );
        void clean_up( void );
    protected:
        /*ATTACKING*/
        enum attacks: uint8_t
        {
            DIRECT_SHOT,
            RESET_ARMS,
            GRAB_PLAYER,
            MOVE_TO,
            ROT_ARMS,
            TOTAL
        };

        float _timeSinceAttack;
        attacks _lastAttack;
        float _attack_timer( attacks t );
        uint8_t _attackProcs;
        uint8_t _total_procs( attacks t );

        /*SUPER SPECIFIC VARIABLES*/
        float* _handDelta; //blocked like [ [ x, y ], [ x, y ] ]
        float _xDelta;
        float _yDelta;
        bool _firstProc;
        SDL_Point* _appTo;

        /*HEALTH DAMAGE*/
        float _iTime;
        HurtCircle* _bodyHurtC;
        HurtCircle** _armHurC;

        SDL_Point* _appendage;
    private:
};

#endif // MRELECTRIC_H
