#ifndef TABEMONSTER_H
#define TABEMONSTER_H

#include <HealthyActor.h>

typedef unsigned char uint8_t;
class TabesMeat;

class TabeMonster : public HealthyActor
{
    public:
        TabeMonster();
        virtual ~TabeMonster();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );

        void get_hurt( double damage, HurtCircle* from );
        void cut_scene_update( void );
        void clean_up( void );
    protected:
        float _xDelta, _yDelta;

        /*GENERAL ATTACK STUFF*/
        enum attacks: uint8_t
        {
            RAM_MEAT,
            MEAT_SPIN,
            MIX_UP_SHOT,
            SUCKING_IN,
            TOTAL
        };

        attacks _lastAttack;
        float _timeSinceAttack;
        float _attack_timer( attacks t = TOTAL );
        uint8_t _attackProcs;
        uint8_t _total_procs( attacks t = TOTAL );
        bool _firstProc;

        /*SPECIFIC ATTACK STUFF*/
        TabesMeat* _meats;
        float _lastMeat;
        uint8_t _get_closest_meat( const SDL_Point& pos );
        uint8_t _storedClose;
        float _angleOffset;

        /*PAIN*/
        HurtCircle* _bodyHC;
        float _iTime;
    private:
};

#endif // TABEMONSTER_H
