#ifndef LONNYLINDA_H
#define LONNYLINDA_H

#include <HealthyActor.h>

typedef unsigned short int uint16_t;
typedef unsigned char uint8_t;

class GameManager;
extern GameManager* gGameManager;

class LonnyLinda : public HealthyActor
{
    public:
        LonnyLinda();
        virtual ~LonnyLinda();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void cut_scene_update( void );

        void get_hurt( double damage, HurtCircle* from );
        void push_me( float xd, float yd, HurtCircle* from );
        void clean_up( void );
    private:

        /*ATTACK GOOFS*/
        enum attacks: uint8_t
        {
            BARRAGE,
            HUUUU,
            CHANGE_ANGLE,
            COAT_FRONT,
            TOTAL
        };

        attacks _lastAttack;
        float _attackTime;
        float _attack_timer( attacks t );
        uint8_t _attackProcs;
        uint8_t _total_procs( attacks t );

        /*SPECIFIC VARIABLES*/
        uint16_t _angle;
        bool _firstProc;

        /*TAIL AND BODY STUFF*/
        HurtCircle** _myHurtcs;
        SDL_Point* _myPoints;
        float* _pointITime;
};

#endif // LONNYLINDA_H
