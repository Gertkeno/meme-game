#ifndef MONA_H
#define MONA_H

#include <HealthyActor.h>

class GameManager;
extern GameManager* gGameManager;

class SDL_Point;
class HurtCircle;

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;

class Mona : public HealthyActor
{
    public:
        Mona();
        virtual ~Mona();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void cut_scene_update( void );

        void get_hurt( double damage, HurtCircle* from );
        void clean_up( void );
    protected:
        /*ATTACK BASE*/
        enum attacks: uint8_t
        {
            RUSSER,
            WALL,
            SHOOTY,
            TETHER,
            TOTAL
        };
        float _timeSinceAttack;
        attacks _lastAttack;
        float _attack_timer( attacks t = TOTAL );
        uint8_t _attackProcs;
        uint8_t _total_procs( attacks t = TOTAL );

        /*SPECIFiC ATTACKS*/
        SDL_Point* _grapplePoint;
        uint8_t _lastWall;
        SDL_Point* _wallpoint;
        bool _firstProc;

        /*PAIN*/
        HurtCircle* _bodyHC;
        float _iTime;
    private:
};

#endif // MONA_H
