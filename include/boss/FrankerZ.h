#ifndef FRANKERZ_H
#define FRANKERZ_H

#include <HealthyActor.h>

class GameManager;
extern GameManager* gGameManager;

class FrankerZ : public HealthyActor
{
    public:
        FrankerZ();
        virtual ~FrankerZ();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void cut_scene_update( void );

        void get_hurt( double damage, HurtCircle* from );
        void clean_up( void );
    private:

        /*NON-SPECIFIC ACTIONS*/
        enum attacks: unsigned char
        {
            SPLITS,
            SOFT_LOCK,
            CHANGE_ROUTE,
            BOMB,
            TOTAL
        };
        attacks _lastAttack;
        float _timeSinceLastAttack;
        float _attack_timer( attacks t );
        unsigned char _attackProcs;
        unsigned char _total_attack_procs( attacks t );

        /*MORE SPECIFIC ACTION VARS*/
        char _xMove, _yMove;
        SDL_Point* _bombPoint;
        bool _firstProc;

        /*HURTY STUFF*/
        HurtCircle* _bodyHC;
        float _iTime;
};

#endif // FRANKERZ_H
