#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <string>
#include <BossSheet.h>

union SDL_Event;

class Background;
typedef struct _Mix_Music Mix_Music;
extern Mix_Music* gMusic;

class SDL_RWops;
class SDL_Point;
class SDL_Window;
extern SDL_Window* gWindow;

class Font_Wrapper;
extern Font_Wrapper* gFont;

class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;

class PlayerShip;
class HurtCircle;
class HitCircle;
class PushCircle;

class HealthyActor;
class EquipMenu;
class Bullet;
class Particle;
class StartMenu;
class TitleScreen;

#ifndef gRenderer
    #define gRenderer SDL_GetRenderer( gWindow )
#endif // gRenderer

#ifdef _GERT_DEBUG
    #define IF_HIT_CIRC( p ) p = gGameManager->get_hit_circle(); if( p == nullptr ) { std::cout << "Could not get hit circle, please raise cap" << std::endl; } else
    #define IF_HURT_CIRC( p ) p = gGameManager->get_hurt_circle( this ); if( p == nullptr ) { std::cout << "Could not get hurt circle, please raise cap" << std::endl; } else
    #define IF_PUSH_CIRC( p ) p = gGameManager->get_push_circle(); if( p == nullptr ) { std::cout << "Could not get push circle, please raise cap" << std::endl; } else

    #define IF_BULLET_GET( p ) p = gGameManager->get_bullet(); if( p == nullptr ) { std::cout << "Could not get bullet, please raise cap" << std::endl; } else
    #define IF_PARTICLE_GET( p ) p = gGameManager->get_particle(); if( p == nullptr ) { std::cout << "Could not get particle, please raise cap" << std::endl; } else
#else
    #define IF_HIT_CIRC( p ) p = gGameManager->get_hit_circle(); if( p != nullptr )
    #define IF_HURT_CIRC( p ) p = gGameManager->get_hurt_circle( this ); if( p != nullptr )
    #define IF_PUSH_CIRC( p ) p = gGameManager->get_push_circle(); if( p != nullptr )

    #define IF_BULLET_GET( p ) p = gGameManager->get_bullet(); if( p != nullptr )
    #define IF_PARTICLE_GET( p ) p = gGameManager->get_particle(); if( p != nullptr )
#endif // _GERT_DEBUG

#define JOY_DEADZONE 2000
#define JOY_HEAVY_DEADZONE 32000

#define JOY_CTR( x ) gGameManager->gamePadControls[ GameManager::x ]

class GameManager
{
    public:
        friend class EquipMenu;
        GameManager();
        virtual ~GameManager();

        enum gmstates: uint8_t
        {
            gmsPLAYING,
            gmsCUT_SCENE,
            gmsNAME_SELECT,
            gmsPAUSE,
            gmsEQUIPMENT,
            gmsCONTROLLER_CONFIG,
            gmsCREDITS_ROLL,
            gmsTITLE_SCREEN,
            gmsQUITTING
        };

        gmstates gameState;

        void update( void );
        void draw( void );

        /*Game Under the hood*/
        HitCircle* get_hit_circle( void );
        HurtCircle* get_hurt_circle( HealthyActor* t );
        PushCircle* get_push_circle( void );
        Bullet* get_bullet( void );

        Particle* get_particle( void );
        SDL_Point get_player_point( void );
        SDL_Point get_enemy_point( void );

        enum padControl: uint8_t
        {
            gpcSHOOT,
            gpcSPECIAL,
            gpcACCELERATE,
            gpcSTART,
            gpcTOTAL
        };
        uint8_t* gamePadControls;
        uint32_t total_boss_score( void );
        void clean_bullets( void );

        /*Cutscene*/
        bool open_scene( std::string fname );
        void open_music( const char* fname );
    private:
        SDL_Event* _event;

        /*over the hood game stuff*/
        PlayerShip* _player;
        EquipMenu* _equips;
        HealthyActor* _theBoss;
        Background* _bg;
        Bullet* _projectiles;
        Particle* _effect;

        StartMenu* _pmenu;
        TitleScreen* _titleScreen;

        /*Game Under the hood*/
        HitCircle* _hitC;
        HurtCircle* _hurtC;
        PushCircle* _pushC;
        uint8_t _configRole;

        /*Saving / Scoring*/
        uint16_t* _bossScore;
        bosses::guide _currentBoss;
        int _bossStartingHealth;
        bool _all_bosses_done( void );

        bool _save_game( void );
        bool _load_game( void );

        /*Cutscene*/
        std::string _sceneText;
        std::string _characterName;
        SDL_RWops* _sceneFile;
        MultiFrame_Wrap* _sceneIcon;
        bool _next_scene( void );
        void _what_now( void );

        /*Credits*/
        std::string* _credStr;
        uint8_t _totalCredits;
        float _timeInCredits;
        bool _start_credits( void );
        void _end_credits( void );
};

#endif // GAMEMANAGER_H
