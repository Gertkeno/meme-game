#ifndef PARTICLE_H
#define PARTICLE_H

class SDL_Rect;
extern double gFrameTime;

class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;

typedef unsigned short int uint16_t;
typedef short int int16_t;

class Particle
{
    public:
        Particle();
        virtual ~Particle();

        enum flags: uint16_t
        {
            FADE_IN = 1 << 0,
            FADE_OUT = 1 << 1,
            RED_MOD = 1 << 2,
            BLUE_MOD = 1 << 3,
            GREEN_MOD = 1 << 4,
            SPIN = 1 << 5,
            SHRINK = 1 << 6,
            RISING = 1 << 7,
            SINKING = 1 << 8,
            FIXED = 1 << 9
        };

        void update( void );
        void draw( void );
        void start( const SDL_Rect& pos, float life );
        bool get_active( void ) { return _lifeTime < _lifeSpan; }
        void force_kill( void );

        uint16_t myTexture;
        int16_t subTex;
        float angle;

        flags effect;
    protected:
        SDL_Rect* _pos;
        float _lifeTime;
        float _lifeSpan;
    private:
};

#endif // PARTICLE_H
