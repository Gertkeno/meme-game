#ifndef BACKGROUND_H
#define BACKGROUND_H

class SDL_Rect;
class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;

class Camera;
extern Camera gCamera;

class Background
{
    public:
        Background();
        virtual ~Background();

        void draw( void );
    protected:
        SDL_Rect* _area;
        enum: unsigned char
        {
            TL,
            TM,
            TR,
            ML,
            MM,
            MR,
            BL,
            BM,
            BR,
            TOTAL
        };
    private:
};

#endif // BACKGROUND_H
