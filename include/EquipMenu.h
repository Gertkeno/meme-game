#ifndef EQUIPMENU_H
#define EQUIPMENU_H

union SDL_Event;
class SDL_Rect;

class Camera;
extern Camera gCamera;

class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;

#define EM_POWERUP_INC 70

class EquipMenu
{
    public:
        EquipMenu();
        virtual ~EquipMenu();

        int manage_input( const SDL_Event& event );
        void draw( void );
        int get_activeSelect( void ) { return _activeSelect; }

        void re_adjust( void );
    protected:
        SDL_Rect* _clickBoxs;
        int _activeSelect;
        bool _joyStickProc;
    private:
};

#endif // EQUIPMENU_H
