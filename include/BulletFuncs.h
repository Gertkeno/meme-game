#ifndef BULLET_FUNCS_H
#define BULLET_FUNCS_H

class Bullet;
class GameManager;
extern GameManager* gGameManager;

namespace bulletFunction
{
    void basicShot( Bullet* p );
    void recursiveShot( Bullet* p );
    void layDoTs( Bullet* p );
    void comeBack( Bullet*p );
}

#endif // BULLET_FUNCS_H
