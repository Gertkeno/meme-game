#ifndef BOSSSHEET_H
#define BOSSSHEET_H

namespace bosses
{
    enum guide: unsigned char
    {
        DUD,
        BACON_BOT,
        FRANKERZ,
        LONNY_LINDA,
        MR_ELECTRIC,
        TABE_MONSTER,
        CHAO_HIBBY,
        MONA,
        NONE
    };
}

#endif // BOSSSHEET_H
