#ifndef HEALTHYACTOR_H
#define HEALTHYACTOR_H

#include <Actor.h>

class HurtCircle;

extern double gFrameTime;

class HealthyActor : public Actor
{
    public:
        HealthyActor();
        virtual ~HealthyActor();
        float get_healthPoints( void ) { return _healthPoints; }
        virtual bool get_dead( void ) { return _healthPoints <= 0; }

        virtual void get_hurt( double damage, HurtCircle* from ) = 0;
        virtual void push_me( float xd, float yd, HurtCircle* from );
        virtual void clean_up( void );
        bool get_enemy( void ) { return _enemy; }

        virtual void cut_scene_update( void ) {}
    protected:
        float _healthPoints;
        bool _enemy;
};

#endif // HEALTHYACTOR_H
