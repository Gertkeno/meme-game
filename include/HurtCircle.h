#ifndef HURTCIRCLE_H
#define HURTCIRCLE_H

class Circle;
class HitCircle;
class PushCircle;
class HealthyActor;

class HurtCircle
{
    public:
        HurtCircle();
        virtual ~HurtCircle();

        Circle* area;
        HealthyActor* target;

        void check_collide( HitCircle* hc );
        void check_push( PushCircle* pc );
};

#endif // HURTCIRCLE_H
