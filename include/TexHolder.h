#ifndef TEXHOLDER_H
#define TEXHOLDER_H

class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;
class Font_Wrapper;
extern Font_Wrapper* gFont;
class Sound_Wrapper;
extern Sound_Wrapper* gSound;

#define FONT_HEIGHT gFont->string_height()

namespace assets
{
    namespace texture
    {
        enum texNames: unsigned short int
        {
            iPC0,/// 0
            iCAPTIN0,/// 1
            iAI0,/// 2

            FRANKER_Z,/// 3
            LONNY_LINDA,/// 4
            MR_ELECTRIC,/// 5
            DOWN_UNDER,/// 6
            TABE_MONSTER,/// 7
            MAKIN_BACON_BOT,/// 8
            CHAO_HIBBY,/// 9
            MONA, /// 10

            EXPLOSION_0,
            LASOR_SHOT,
            POWER_UPS,
            ORB_SHOT,
            OOT_BOMB,
            CIRCLE,
            PICKER,
            TRACK,
            EMBER,
            FLAME,
            SHIP,

            TEXT_BACKGROUND,
            BACKGROUND,
            HEALTH_BAR,
            TITLE_IMG,

            TOTAL
        };
    }

    namespace sfx
    {
        enum sfxNames: unsigned short int
        {
            DAN_SHORT_UH,
            TABE_SUCC,
            OVER_HERE,
            TELEPORT,
            SCREAM,
            TOTAL
        };
    }
};

#endif // TEXHOLDER_H
