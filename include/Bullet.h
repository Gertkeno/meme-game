#ifndef BULLET_H
#define BULLET_H

#include <Actor.h>
#include <BulletFuncs.h>

class HealthyActor;
extern double gFrameTime;
class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;

class Bullet : public Actor
{
    public:
        Bullet();
        virtual ~Bullet();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos, HealthyActor* o, float angle, float speed, float life );
        void alter_direction( float angle );

        bool is_active( void ) { return _lifeTime < totalTime; }
        void force_kill( void );

        float totalTime;
        uint16_t myTexture;
        uint8_t subTex;
        uint8_t totalAbilityCount;
        float radius;
        short int damage;

        void (*ability)( Bullet* b );
        HealthyActor* owner;

        uint8_t get_abilityCount( void ) { return _abilityCount; }
        float get_angle( void ) { return _angle; }
    protected:
        float _lifeTime;
        float _xDelta;
        float _yDelta;
        uint8_t _abilityCount;
        float _angle;
    private:
};

#endif // BULLET_H
