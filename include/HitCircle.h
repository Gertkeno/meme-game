#ifndef HITCIRCLE_H
#define HITCIRCLE_H

class Circle;
extern double gFrameTime;

class HitCircle
{
    public:
        HitCircle();
        virtual ~HitCircle();

        virtual void update( void );
        virtual void start( Circle area, float damage, bool enemyFlag );
        void force_kill( void ) { _lifeTime = totalLifeTime + 1; }

        Circle* area;
        float damage;

        bool get_active( void ) { return _lifeTime <= totalLifeTime; }
        bool enemy;
        float totalLifeTime;
    protected:
        float _lifeTime;
};

#endif // HITCIRCLE_H
