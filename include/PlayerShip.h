#ifndef PLAYERSHIP_H
#define PLAYERSHIP_H

#include <HealthyActor.h>

union SDL_Event;
class HurtCircle;
extern double gFrameTime;

class GameManager;
extern GameManager* gGameManager;

typedef short int int16_t;
typedef unsigned char uint8_t;

class PlayerShip : public HealthyActor
{
    public:
        PlayerShip();
        virtual ~PlayerShip();

        void manage_event( const SDL_Event& event );
        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
        void get_hurt( double damage, HurtCircle* from );
        void push_me( float xd, float yd, HurtCircle* from );

        enum powerup: unsigned char
        {
            BRAKE,
            AUTO_RETICLE,
            SHIELD,
            BACK_SHOT,
            MASSIVE_SHOT,
            TELEPORT,
            HEAL,
            NONE
        };

        static float* const powerMaxTime;

        void set_powerup( powerup p );
        powerup get_powerup( void ) { return _curretPower; }
        SDL_Point get_syncPoint( void );
    protected:
        /*MOVEMENT*/
        enum inputTypes: uint8_t
        {
            ACCELERATE,
            TURN_RIGHT,
            TURN_LEFT,
            SHOOT,
            SPECIAL,
            TOTAL
        };
        bool _inputs[ inputTypes::TOTAL ];
        inputTypes _lastKey;
        double _angle;
        float _xDelta, _yDelta;

        int16_t _xJoyStick;
        int16_t _yJoyStick;

        /*PAIN HURTING*/
        HurtCircle* _myHurtCirc;
        float _iTime;
        bool _pushedFrame; ///can't be pushed twice in a frame;


        /*ATTACKING*/
        powerup _curretPower;
        float _lastPowerUsed;
        float _lastShot;

        bool _active;
};

#endif // PLAYERSHIP_H
