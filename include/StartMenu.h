#ifndef STARTMENU_H
#define STARTMENU_H

#include <string>
union SDL_Event;
struct SDL_Rect;

#define SM_MAX_MUSIC 20
typedef unsigned char uint8_t;

class GameManager;
extern GameManager* gGameManager;
extern double gFrameTime;

class StartMenu
{
    public:
        StartMenu();
        virtual ~StartMenu();

        static uint8_t Music_Volume;
        static uint8_t Sound_Volume;

        enum options: unsigned char
        {
            oRESUME,
            oMAIN_MENU,
            oSOUND_VOLUME,
            oMUSIC_VOLUME,
            oFS_TOGGLE,
            oREBIND_CONTROLLER,
            oQUIT,
            oTOTAL
        };

        options manage_inputs( const SDL_Event& evnt );
        void draw( void );
        void re_adjust( void );
    protected:
        std::string* _boxText;
        SDL_Rect* _clickBoxes;
        options _activeSelect;
        bool _joyStickProc;
    private:
};

#endif // STARTMENU_H
