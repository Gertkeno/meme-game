#ifndef PUSHCIRCLE_H
#define PUSHCIRCLE_H

#include <HitCircle.h>
class GameManager;
extern GameManager* gGameManager;

class PushCircle: public HitCircle
{
    public:
        PushCircle();
        virtual ~PushCircle();

        void start( Circle area, float angle, float totalForce, bool enemyFlag, float damage = 0 );

        float get_xForce( void ) { return _xForce; }
        float get_yForce( void ) { return _yForce; }
    protected:
        float _xForce, _yForce;
    private:
};

#endif // PUSHCIRCLE_H
