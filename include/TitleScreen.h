#ifndef TITLESCREEN_H
#define TITLESCREEN_H

typedef unsigned char uint8_t;
union SDL_Event;
class Camera;
extern Camera gCamera;

extern double gFrameTime;

class GameManager;
extern GameManager* gGameManager;
#include <string>

class TitleScreen
{
    public:
        TitleScreen();
        virtual ~TitleScreen();

        enum tsOptions: uint8_t
        {
            CONTINUE,
            NEW_GAME,
            CREDITS,
            QUIT,
            TOTAL
        };

        void draw( void );
        void update( void );
        tsOptions manage_input( const SDL_Event& event );

    private:
        std::string* _mstr;
        tsOptions _activeSelect;
        bool _joyStickProc;
};

#endif // TITLESCREEN_H
