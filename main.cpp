#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include <TexHolder.h>
#include <gert_Wrappers.h>
#include <gert_FontWrapper.h>
#include <gert_SoundWrapper.h>
#include <GameManager.h>

/*SAVE STUFF*/
#include <StartMenu.h>

#include <gert_Camera.h>
#include <random>
#include <ctime>
#include <cstring>

Camera gCamera;
GameManager* gGameManager;
SDL_Window* gWindow;

MultiFrame_Wrap* gTextures;
Font_Wrapper* gFont;
Sound_Wrapper* gSound;

double gFrameTime;
Mix_Music* gMusic;

typedef unsigned char uint8_t;

void loadAssets( void )
{
    using namespace assets;
    gTextures = new MultiFrame_Wrap[ texture::TOTAL ];

    gTextures[ texture::iPC0 ].load_texture( "assets/storyicon/mainchar0.png" );
    gTextures[ texture::iCAPTIN0 ].load_texture( "assets/storyicon/capin0.png" );
    gTextures[ texture::iAI0 ].load_texture( "assets/storyicon/ai0.png" );
    gTextures[ texture::SHIP ].load_texture( "assets/ship.png" );

    gTextures[ texture::CIRCLE ].load_texture( "assets/ui/Circle.png" );
    gTextures[ texture::TRACK ].load_texture( "assets/gradtrack.png" );
    gTextures[ texture::PICKER ].load_texture( "assets/ui/picker.png" );
    gTextures[ texture::EMBER ].load_texture( "assets/ember.png", 4, 1 );
    gTextures[ texture::BACKGROUND ].load_texture( "assets/bg.jpg" );
    gTextures[ texture::TEXT_BACKGROUND ].load_texture( "assets/ui/textbg.jpg" );
    gTextures[ texture::LASOR_SHOT ].load_texture( "assets/laserShot.png" );
    gTextures[ texture::ORB_SHOT ].load_texture( "assets/orby.png" );

    gTextures[ texture::POWER_UPS ].load_texture( "assets/ui/powerups.png", "xml" );
    gTextures[ texture::EXPLOSION_0 ].load_texture( "assets/explode0.png", 5, 5 );
    gTextures[ texture::HEALTH_BAR ].load_texture( "assets/ui/healthfragment.png" );
    gTextures[ texture::TITLE_IMG ].load_texture( "assets/ui/title.png" );
    gTextures[ texture::OOT_BOMB ].load_texture( "assets/OOT_bomb.png" );
    gTextures[ texture::FLAME ].load_texture( "assets/flame.png", "xml" );

    gTextures[ texture::FRANKER_Z ].load_texture( "assets/dogface.png" );
    gTextures[ texture::LONNY_LINDA ].load_texture( "assets/wailord.png", "xml" );
    gTextures[ texture::MR_ELECTRIC ].load_texture( "assets/mrelectric.png", "xml" );
    gTextures[ texture::DOWN_UNDER ].load_texture( "assets/fullDUD.png", "xml" );
    gTextures[ texture::TABE_MONSTER ].load_texture( "assets/tabemonster.png", "xml" );
    gTextures[ texture::MAKIN_BACON_BOT ].load_texture( "assets/baconbot.png", "xml" );
    gTextures[ texture::CHAO_HIBBY ].load_texture( "assets/chaos.png", "xml" );
    gTextures[ texture::MONA ].load_texture( "assets/mona.png", "xml" );

    gFont = new Font_Wrapper( "assets/BebasNeue.otf", 22 );

    gSound = new Sound_Wrapper[ sfx::TOTAL ];
    gSound[ sfx::DAN_SHORT_UH ].load_sound( "assets/audio/shortUH.wav" );
    gSound[ sfx::TABE_SUCC ].load_sound( "assets/audio/succ.wav" );
    gSound[ sfx::SCREAM ].load_sound( "assets/audio/die.wav" );
    gSound[ sfx::TELEPORT ].load_sound( "assets/audio/Teleport.wav" );
    gSound[ sfx::OVER_HERE ].load_sound( "assets/audio/scorpion-get_over_here.wav" );
}

void closeAssets( void )
{
    delete[] gTextures;
    delete[] gSound;

    if( gFont != nullptr )
    {
        delete gFont;
    }

    if( gMusic != nullptr )
    {
        Mix_PauseMusic();
        Mix_FreeMusic( gMusic );
    }
}

void save_settings( void )
{
    SDL_RWops* sFile = SDL_RWFromFile( "Settings.dat", "w" );
    if( sFile != nullptr )
    {
        SDL_RWwrite( sFile, &gCamera.w, sizeof( int ), 1 );
        SDL_RWwrite( sFile, &gCamera.h, sizeof( int ), 1 );

        SDL_RWwrite( sFile, &StartMenu::Music_Volume, sizeof( uint8_t ), 1 );
        SDL_RWwrite( sFile, &StartMenu::Sound_Volume, sizeof( uint8_t ), 1 );

        for( uint8_t i = 0; i < GameManager::gpcTOTAL; i++ )
        {
            SDL_RWwrite( sFile, &gGameManager->gamePadControls[ i ], sizeof( uint8_t ), 1 );
        }

        bool fs = (SDL_GetWindowFlags( gWindow ) & SDL_WINDOW_FULLSCREEN_DESKTOP) == SDL_WINDOW_FULLSCREEN_DESKTOP;
        SDL_RWwrite( sFile, &fs, sizeof( bool ), 1 );

        SDL_RWclose( sFile );
    }
    else
    {
        std::cout << "Couldn't write to save file\n";
    }
}

bool load_settings( void )
{
    SDL_RWops* sFile = SDL_RWFromFile( "Settings.dat", "r" );
    if( sFile != nullptr )
    {
        SDL_RWread( sFile, &gCamera.w, sizeof( int ), 1 );
        SDL_RWread( sFile, &gCamera.h, sizeof( int ), 1 );

        SDL_RWread( sFile, &StartMenu::Music_Volume, sizeof( uint8_t ), 1 );
        SDL_RWread( sFile, &StartMenu::Sound_Volume, sizeof( uint8_t ), 1 );

        for( uint8_t i = 0; i < GameManager::gpcTOTAL; i++ )
        {
            SDL_RWread( sFile, &gGameManager->gamePadControls[ i ], sizeof( uint8_t ), 1 );
        }

        bool fs = false;
        SDL_RWread( sFile, &fs, sizeof( bool ), 1 );

        if( fs )
        {
            SDL_SetWindowFullscreen( gWindow, SDL_WINDOW_FULLSCREEN_DESKTOP );
        }

        SDL_RWclose( sFile );
        return true;
    }
    else
    {
        return false;
    }
}

int main( int argc, char* argv[] )
{
    srand( time( 0 ) );
    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    {
        std::cout << SDL_GetError() << std::endl;
        return 1;
    }
    if( IMG_Init( IMG_INIT_JPG | IMG_INIT_PNG ) < 0 )
    {
        std::cout << IMG_GetError() << std::endl;
        return 2;
    }
    if( TTF_Init() < 0 )
    {
        std::cout << TTF_GetError() << std::endl;
        return 3;
    }
    if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )/* remember to resample to 22050*/
    {
        std::cout << Mix_GetError() << std::endl;
        return 4;
    }

    gMusic = nullptr;
    gCamera = { 0, 0, 1280, 720, 0.4 };

    gWindow = SDL_CreateWindow( "Meme Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gCamera.w, gCamera.h, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE );
    SDL_Renderer* rendera = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );

    SDL_ShowCursor( false );

    if( SDL_NumJoysticks() > 0 )
    {
        SDL_Joystick* gameCon = SDL_JoystickOpen( 0 );
        if( gameCon == NULL )
        {
            std::cout << "Error opening joystick\n>" << SDL_GetError() << std::endl;
        }
    }

    loadAssets();
    gGameManager = new GameManager;

    if( !load_settings() )
    {

    }

    ///Command line arguments
    if( argc > 1 )
    {
        for( int i = 1; i < argc; i++ )
        {
            if( strcmp( argv[ i ], "-window" ) == 0 )
            {
                SDL_SetWindowFullscreen( gWindow, 0 );
            }
            else if( strcmp( argv[ i ], "-lowres" ) == 0 )
            {
                gCamera.w = 800;
                gCamera.h = 600;
            }
            else if( strcmp( argv[ i ], "-mute" ) == 0 )
            {
                StartMenu::Music_Volume = 0;
                StartMenu::Sound_Volume = 0;
            }
        }
    }

    SDL_SetWindowSize( gWindow, gCamera.w, gCamera.h );

    Mix_VolumeMusic( float(StartMenu::Music_Volume)/SM_MAX_MUSIC * MIX_MAX_VOLUME );
    Mix_Volume( -1, float(StartMenu::Sound_Volume)/SM_MAX_MUSIC * MIX_MAX_VOLUME );

    Uint32 frameTime = 0;
    while( gGameManager->gameState != GameManager::gmsQUITTING )
    {
        gFrameTime = ( SDL_GetTicks() - frameTime ) * 0.001;
        frameTime = SDL_GetTicks();

        gGameManager->update();
        gGameManager->draw();

        #define MAX_FPS ( 1000.0/150 )
        if( SDL_GetTicks() - frameTime < MAX_FPS )
        {
            SDL_Delay( MAX_FPS - ( SDL_GetTicks() - frameTime ) );
        }
    }

    closeAssets();
    save_settings();

    SDL_DestroyRenderer( rendera );
    SDL_DestroyWindow( gWindow );

    SDL_Quit();
    IMG_Quit();
    TTF_Quit();
    Mix_Quit();

    delete gGameManager;
    return 0;
}
